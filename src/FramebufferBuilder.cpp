#include "Textures/DepthTexture.hpp"
#include "FramebufferBuilder.hpp"

#include <iostream>

namespace plp
{

void FramebufferBuilder::addColorAttachment(std::shared_ptr<Texture> texture)
{
	colorAttachments.push_back(texture);
}

void FramebufferBuilder::setDepthAttachment(std::shared_ptr<Texture> depthTexture)
{
	depthAttachment = depthTexture;
}

void FramebufferBuilder::setStencilAttachment(std::shared_ptr<Texture> stencilTexture)
{
	stencilAttachment = stencilTexture;
}

static constexpr std::string_view getFramebufferStatusStr(GLenum status)
{
	switch (status)
	{
		CASE_VALUE(GL_FRAMEBUFFER_UNDEFINED);
		CASE_VALUE(GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT);
		CASE_VALUE(GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT);
		CASE_VALUE(GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER);
		CASE_VALUE(GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER);
		CASE_VALUE(GL_FRAMEBUFFER_UNSUPPORTED);
		CASE_VALUE(GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE);
		CASE_VALUE(GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS);
	}
	std::unreachable();
}

std::optional<Framebuffer> FramebufferBuilder::create(bool noWriteNoRead) {
	if(!noWriteNoRead && colorAttachments.empty())
		return std::nullopt;
	Framebuffer fb;
	for(unsigned i = 0; i < colorAttachments.size(); i++) {
		glNamedFramebufferTexture(fb.id, GL_COLOR_ATTACHMENT0 + i, colorAttachments[i]->getID(), 0);
	}
	if(stencilAttachment != nullptr) {
		if(stencilAttachment == depthAttachment)
			glNamedFramebufferTexture(fb.id, GL_DEPTH_STENCIL_ATTACHMENT, stencilAttachment->getID(), 0);
		else
			glNamedFramebufferTexture(fb.id, GL_STENCIL_ATTACHMENT, stencilAttachment->getID(), 0);
	}
	if(depthAttachment != nullptr)
		glNamedFramebufferTexture(fb.id, GL_DEPTH_ATTACHMENT, depthAttachment->getID(), 0);
	if(noWriteNoRead) {
		glNamedFramebufferReadBuffer(fb.id, GL_NONE);
		glNamedFramebufferDrawBuffer(fb.id, GL_NONE);
	}
	GLenum status;
	status = glCheckNamedFramebufferStatus(fb.id, GL_DRAW_FRAMEBUFFER);
	if(status != GL_FRAMEBUFFER_COMPLETE) {
		std::cerr << "Error: framebuffer incomplete for drawing ( " << getFramebufferStatusStr(status) << " )" << std::endl;
		return std::nullopt;
	}
	status = glCheckNamedFramebufferStatus(fb.id, GL_READ_FRAMEBUFFER);
	if(status != GL_FRAMEBUFFER_COMPLETE) {
		std::cerr << "Error: framebuffer incomplete for reading ( " << getFramebufferStatusStr(status) << " )" << std::endl;
		return std::nullopt;
	}
	fb.colorAttachments = std::move(colorAttachments);
	fb.depthAttachment = std::move(depthAttachment);
	fb.stencilAttachment = std::move(stencilAttachment);
	return fb;
}

std::optional<Framebuffer>
createDepthMap(glm::uvec2 resolution)
{
	FramebufferBuilder builder;
	auto depth = std::make_shared<DepthTexture>(DepthTexture::createEmpty(resolution).value());
	builder.setDepthAttachment(depth);
	return builder.create(true);
}

} // namespace plp
