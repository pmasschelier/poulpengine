#include "Model.hpp"

namespace plp {

Model::Model(RawModel &&model, std::shared_ptr<AbstractShaderProgram> program) :
	Model(std::make_shared<RawModel>(std::forward<RawModel>(model)), program)
{
}

Model::Model(std::shared_ptr<const RawModel> model,
			std::shared_ptr<AbstractShaderProgram> program) :
	Renderable(model, program)

{
	texturesId.fill(0);
}

constexpr GLbitfield 
	mapping_flags = GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT,
	storage_flags = GL_DYNAMIC_STORAGE_BIT | mapping_flags;

void Model::setProgram(std::shared_ptr<AbstractShaderProgram> program)
{
	this->program = program;
}

void Model::setMaterial(Material const &material)
{
	if(!m_materialUBO) {
		glCreateBuffers(1, &m_materialUBO);
		glNamedBufferStorage(m_materialUBO, sizeof(Material), &material, storage_flags);
	}

	this->material = material;
	glNamedBufferSubData(m_materialUBO, 0, sizeof(Material), &material);
}

void Model::checkTextureUnit(std::size_t unit) {
	if(unit > MAX_TEXTURE_UNITS)
		throw std::runtime_error("Texture unit should be a number between 0 and 15 (included).");
}

void Model::addTexture(std::shared_ptr<Texture> const& texture, std::string const& samplerName, std::size_t unit)
{
#ifndef NDEBUG
	checkTextureUnit(unit);
#endif
	textures[unit] = std::make_optional(std::make_pair(samplerName, texture));
	texturesId[unit] = texture->getID();
}

void Model::render(StaticBufferStorage &command) const
{
	for(unsigned i = 0; i < MAX_TEXTURE_UNITS; i++) {
		if(textures[i])
			program->setUniform(textures[i].value().first, (int)i);
	}

	[[unlikely]] if(!cullFace)
		glDisable(GL_CULL_FACE);

	glBindTextures(0, texturesId.size(), texturesId.data());

	if(m_materialUBO) {
		glBindBufferBase(GL_UNIFORM_BUFFER, ShaderProgram::MATERIAL, m_materialUBO);
	}

	Renderable::render(command);

	if(m_materialUBO) {
		glBindBufferBase(GL_UNIFORM_BUFFER, ShaderProgram::MATERIAL, 0);
	}

	[[unlikely]] if(!cullFace)
		glEnable(GL_CULL_FACE);
}

void Model::render(const std::uint32_t queries, QueryData *data) const
{
	for(unsigned i = 0; i < MAX_TEXTURE_UNITS; i++) {
		if(textures[i])
			program->setUniform(textures[i].value().first, (int)i);
	}

	[[unlikely]] if(!cullFace)
		glDisable(GL_CULL_FACE);

	glBindTextures(0, texturesId.size(), texturesId.data());

	if(m_materialUBO) {
		glBindBufferBase(GL_UNIFORM_BUFFER, ShaderProgram::MATERIAL, m_materialUBO);
	}

	Renderable::render(queries, data);

	if(m_materialUBO) {
		glBindBufferBase(GL_UNIFORM_BUFFER, ShaderProgram::MATERIAL, 0);
	}

	[[unlikely]] if(!cullFace)
		glEnable(GL_CULL_FACE);
}

void Model::render(glm::mat4 modelMatrix, glm::mat3 inversedTransposedModelMatrix, const std::uint32_t queries, QueryData* data) const {	
	program->setUniform("model", modelMatrix);
	program->setUniform("inversedTransposedModel", inversedTransposedModelMatrix);
	
	render(queries, data);
}

void Model::setCullFace(bool activate)
{
	this->cullFace = activate;
}

bool Model::getCullFace() const
{
	return cullFace;
}

}
