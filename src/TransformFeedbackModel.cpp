#include "TransformFeedbackModel.hpp"
#include "Enabler.hpp"

namespace plp
{

TransformFeedbackModel::TransformFeedbackModel(
	RawModel&& rawmodel,
	std::shared_ptr<TransformFeedbackShaderProgram> program,
	std::shared_ptr<StaticBufferStorage> writeBuffer
) :
	Renderable(std::move(rawmodel), program),
	program(program.get()),
	writeBuffer(writeBuffer)
{
}

const TransformFeedbackObject&TransformFeedbackModel::getTransformFeedbackObject() const
{
	return tf;
}

void TransformFeedbackModel::setWriteBuffer(std::shared_ptr<StaticBufferStorage> writeBuffer)
{
	this->writeBuffer = writeBuffer;
}

TransformFeedbackObject TransformFeedbackModel::swapTransformFeedbackObject()
{
	TransformFeedbackObject nextTf;
	std::swap(tf, nextTf);
	return nextTf;
}

void TransformFeedbackModel::render(const std::uint32_t queries, QueryData *data) const
{
	Enabler enabler(GL_RASTERIZER_DISCARD);

	program->getTransformFeedBackObject().setBuffer(writeBuffer);

	const auto call_render = [this, queries, data]() {
		Renderable::render(queries, data);
	};

	tf.record(POINTS, call_render);
}

} // namespace plp
