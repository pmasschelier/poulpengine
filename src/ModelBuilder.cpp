#include "ModelBuilder.hpp"
#include "Textures/Texture.hpp"
#include "Shaders/ShaderBuilder.hpp"

namespace plp
{

bool ModelBuilder::checkAttribute(Attribute const& attr, unsigned int relativeOffset) {
	if(attr.index == Attribute::DUMMY)
		return true;

	if(attr.index >= RawModel::getMaxVertexAttribs()) {
		std::cerr << "Error: Array attrib index can't be greater than " << RawModel::getMaxVertexAttribs() - 1 << std::endl;
		goto add_attribute_error;
	}
	if(relativeOffset > RawModel::getMaxVertexAttribRelativeOffset()) {
		std::cerr << "Error: Array attrib relative offset can't be greater than " << RawModel::getMaxVertexAttribRelativeOffset() << std::endl;
		goto add_attribute_error;
	}
	if(usedAttributeIndices.contains(attr.index)) {
		std::cerr << "Attribute index " << attr.index << "(Width: " << attr.width << ", Type: " << attr.typeStr() << ")" << " is already used in this VAO." << std::endl;
		goto add_attribute_error;
	}
	return true;
add_attribute_error:
	std::cerr << "Error: Invalid VAO attribute (width = " << attr.width << ", type = " << attr.typeStr() << ", index = " << attr.index << ")" << std::endl;
	return false;
}

bool ModelBuilder::checkBuffer(SharedBuffer const& buffer, GLuint bindingIndex, unsigned int stride)
{
	if(!buffer) {
		std::cerr << "Error: Can't bind a SharedBuffer == nullptr" << std::endl;
		return false;
	}
	if(bindingIndex >= RawModel::getMaxVertexAttribBindings()) {
		std::cerr << "Error: Array attrib binding can't be greater than " << RawModel::getMaxVertexAttribBindings() - 1 << std::endl;
		return false;
	}
	if(stride > RawModel::getMaxVertexAttribStride()) {
		std::cerr << "Error: Array attrib stride can't be greater than " << RawModel::getMaxVertexAttribStride() << std::endl;
		return false;
	}
	return true;
	std::cerr << "Error: Invalid VAO buffer binding (" << "(id = )" << buffer->getID() << ", binding index = " << bindingIndex << ", stride = " << stride << ")" << std::endl;
	return false;
}

ModelBuilder::ModelBuilder(std::size_t nbVerticesToDraw, PrimitiveType type, std::size_t patchSize): 
	nbVerticesToDraw(nbVerticesToDraw),
	type(type),
	patchSize(patchSize)
{
}

bool ModelBuilder::addBuffer(StaticBufferStorage &&buffer, Attribute const &attr, std::size_t offset, unsigned int attribDivisor)
{
	return addBuffer(std::make_shared<StaticBufferStorage>(std::forward<StaticBufferStorage>(buffer)), attr, offset, attribDivisor);
}

bool ModelBuilder::addBuffer(SharedBuffer const &buffer, Attribute const &attr, std::size_t offset, unsigned int attribDivisor)
{
	GLuint bindIndex = boundBuffers.size();
	if(!checkBuffer(buffer, bindIndex, attr.getByteSize()))
		return false;
	if(!checkAttribute(attr, bindIndex))
		return false;
	boundBuffers.push_back({ buffer, { attr }, offset, attribDivisor });
	return true;
}

bool ModelBuilder::addBuffer(StaticBufferStorage&& buffer, std::vector<Attribute> const &attribs, std::size_t offset, unsigned int attribDivisor)
{
	return addBuffer(std::make_shared<StaticBufferStorage>(std::forward<StaticBufferStorage>(buffer)), attribs, offset, attribDivisor);
}

bool ModelBuilder::addBuffer(SharedBuffer const& buffer, std::vector<Attribute> const& attribs, std::size_t offset, unsigned int attribDivisor)
{
	GLuint bindIndex = boundBuffers.size();
	GLuint relativeOffset{};
	bool invalidAttribute = false;
	for(auto const& attr : attribs) {
		if(!checkAttribute(attr, relativeOffset))
			invalidAttribute = true;
		relativeOffset += attr.getByteSize();
	}
	if(invalidAttribute)
		return false;
	
	if(!checkBuffer(buffer, bindIndex, relativeOffset))
		return false;

	boundBuffers.push_back({ buffer, attribs, offset, attribDivisor });
	return true;
}

using ElementBuffer = StaticBufferStorage;
void ModelBuilder::setElementBuffer(ElementBuffer&& buffer)
{
	setElementBuffer(std::make_shared<ElementBuffer>(std::forward<ElementBuffer>(buffer)));
}

void ModelBuilder::setElementBuffer(SharedBuffer buffer)
{
	elements = buffer;
}

void ModelBuilder::setBoundingBox(glm::vec3 center, glm::vec3 extent)
{
	this->center = center;
	this->extent = extent;
}

void ModelBuilder::setName(std::string const &name)
{
	this->name = name;
}

std::string ModelBuilder::getName() const
{
	return name;
}

bool ModelBuilder::addTexture(std::shared_ptr<Texture> const &texture)
{
	textures.push_back(texture);
	return true;
}

std::optional<RawModel> ModelBuilder::create() const
{
	RawModel mesh(nbVerticesToDraw, type, patchSize);

	if(elements) {
		glVertexArrayElementBuffer(mesh.id, elements->getID());
		mesh.elementBuffer = true;
	}
	else {
		for(unsigned int bindingIndex = 0; bindingIndex < boundBuffers.size(); bindingIndex++) {
			const RawModel::BindingPoint& b = boundBuffers[bindingIndex];
			std::size_t stride = Attribute::computeStride(b.attributes.cbegin(), b.attributes.cend());
			std::size_t minSize = stride * nbVerticesToDraw;
			if(b.attribDivisor == 0 && minSize > b.buffer->getSizeBytes()) {
				std::cerr << "Error: Vertex buffer (" << b.buffer->getSizeBytes() << " bytes)"
					<< " can't be smaller than : buffer stride (" << stride << " bytes)"
					<< " * nb vertices to draw (" << nbVerticesToDraw << ") = " << minSize << " bytes" << std::endl;
				return std::nullopt;
			}
		}
	}
	for(unsigned int bindingIndex = 0; bindingIndex < boundBuffers.size(); bindingIndex++) {
		const RawModel::BindingPoint& b = boundBuffers[bindingIndex];

		unsigned int relativeOffset{};
		for(auto const& attr: b.attributes) {
			if(Attribute::DUMMY != attr.index) {
				glEnableVertexArrayAttrib(mesh.id, attr.index);
				if(Attribute::isInteger(attr.type))
					glVertexArrayAttribIFormat(mesh.id, attr.index, attr.width, attr.type, relativeOffset);
				else if(attr.type == Attribute::DOUBLE)
					glVertexArrayAttribLFormat(mesh.id, attr.index, attr.width, attr.type, relativeOffset);
				else
					glVertexArrayAttribFormat(mesh.id, attr.index, attr.width, attr.type, GL_FALSE, relativeOffset);
				glVertexArrayAttribBinding(mesh.id, attr.index, bindingIndex);
			}
			relativeOffset += attr.getByteSize();
		}
		
		constexpr auto OFFSET = 0;
		glVertexArrayVertexBuffer(mesh.id, bindingIndex, b.buffer->getID(), OFFSET, relativeOffset);
		glVertexArrayBindingDivisor(mesh.id, bindingIndex, b.attribDivisor);
	}
	mesh.center = center;
	mesh.extent = extent;
	return mesh;
}

std::optional<Model> ModelBuilder::create(std::shared_ptr<ShaderProgram> const &program, std::uint32_t instanceCount) const
{
	std::optional<RawModel> rawmodel = create();
	if(!rawmodel)
		return std::nullopt;
	Model model(std::move(rawmodel.value()), program);
	for(unsigned i = 0; i < textures.size(); i++) {
		model.addTexture(textures[i], "tex", i);
	}
	model.setInstanceCount(instanceCount);
	return model;
}

/* std::optional<InstancedModel>
ModelBuilder::create(std::shared_ptr<ShaderProgram> const& program, unsigned int instanceCount, unsigned swapBufferCount, bool enableFrustumCulling)
{
	const std::size_t instancedBufferSize = instanceCount * (swapBufferCount + 1) * sizeof(InstancedModel::TransformData);
	const auto instancedBuffer = std::make_shared<MappedBufferStorage<InstancedModel::TransformData[]>>(instancedBufferSize);

	const std::vector<Attribute> attributes = {
		{ Attribute::FLOAT, Attribute::VEC4, Attribute::MODEL},
		{ Attribute::FLOAT, Attribute::VEC4, Attribute::MODEL + 1},
		{ Attribute::FLOAT, Attribute::VEC4, Attribute::MODEL + 2},
		{ Attribute::FLOAT, Attribute::VEC4, Attribute::MODEL + 3},
		{ Attribute::FLOAT, Attribute::VEC3, Attribute::INVERSED_TRANSPOSED_MODEL},
		{ Attribute::FLOAT, Attribute::VEC3, Attribute::INVERSED_TRANSPOSED_MODEL + 1},
		{ Attribute::FLOAT, Attribute::VEC3, Attribute::INVERSED_TRANSPOSED_MODEL + 2},
	};

	if(!enableFrustumCulling) {
		if(!addBuffer(instancedBuffer,attributes, 0, 1))
			return std::nullopt;
		std::optional<Model> model = create(program);
		if(!model)
			return std::nullopt;
		return InstancedModel(std::move(model.value()), instancedBuffer, instanceCount, swapBufferCount);
	}

	const auto culledBuffer = std::make_shared<StaticBufferStorage>(instancedBufferSize);

	if(!addBuffer(culledBuffer,attributes, 0, 1))
		return std::nullopt;
	std::optional<Model> model = create(program);
	if(!model)
		return std::nullopt;
	clear();
	
	addBuffer(instancedBuffer, attributes);
	auto rawCullingModel = create();
	if(!rawCullingModel)
		return std::nullopt;

	TransformFeedbackModel cullingModel(std::move(rawCullingModel.value()), ShaderBuilder::getCommonTransformFeedbackProgram(CULLING_PROGRAM), culledBuffer);
	return InstancedModel(std::move(model.value()), instancedBuffer, std::move(cullingModel), instanceCount, swapBufferCount);
} */

/* std::optional<RawModel> RawModelBuilder::create(
	std::size_t nbVerticesToDraw,
	std::vector<RawModel::BindingPoint> bindingPoints,
	SharedBuffer elements,
	PrimitiveType type,
	std::size_t patchSize)
{
	RawModelBuilder builder(nbVerticesToDraw, type, patchSize);
	for(auto const& bindingPoint: bindingPoints) {
		if(!builder.addBuffer(bindingPoint.buffer, bindingPoint.attributes, bindingPoint.attribDivisor))
			return std::nullopt;
	}
	builder.setElementBuffer(elements);
	return builder.create();
} */

std::optional<RawModel> ModelBuilder::create(
	std::size_t nbVerticesToDraw,
	RawModel::BindingPoint bindingPoint,
	SharedBuffer elements,
	PrimitiveType type,
	std::size_t patchSize)
{
	ModelBuilder builder(nbVerticesToDraw, type, patchSize);
	if(!builder.addBuffer(bindingPoint.buffer, bindingPoint.attributes, bindingPoint.attribDivisor))
		return std::nullopt;
	builder.setElementBuffer(elements);
	return builder.create();
}

std::optional<Model> ModelBuilder::create(std::shared_ptr<ShaderProgram> program, std::size_t nbVerticesToDraw, RawModel::BindingPoint bindingPoint, SharedBuffer elements, PrimitiveType type, std::size_t patchSize)
{
	ModelBuilder builder(nbVerticesToDraw, type, patchSize);
	if(!builder.addBuffer(bindingPoint.buffer, bindingPoint.attributes, bindingPoint.attribDivisor))
		return std::nullopt;
	builder.setElementBuffer(elements);
	return builder.create(program);
}

void ModelBuilder::clear()
{
	boundBuffers.clear();
	usedAttributeIndices.clear();
	elements.reset();
}

} // namespace plp
