#include "FrustumShape.hpp"

namespace plp
{

namespace FrustumShape
{

ModelBuilder create(float FoV, float ratio, float nearClipping, float farClipping)
{
	ModelBuilder builder(24, LINES);

	const float halfVSide = glm::tan(glm::radians(FoV) * .5f);
    const float halfHSide = halfVSide * ratio;

	const glm::vec3 direction(0.f, 0.f, 1.f), left(halfHSide, 0., 0.f), up(0.f, halfVSide, 0.f);

	std::vector<glm::vec3> vertices = {
		nearClipping * (direction + left + up),
		nearClipping * (direction - left + up),
		nearClipping * (direction + left - up),
		nearClipping * (direction - left - up),
		farClipping * (direction + left + up),
		farClipping * (direction - left + up),
		farClipping * (direction + left - up),
		farClipping * (direction - left - up),
	};

	std::vector<unsigned int> elements = {
		0, 1, 2, 3, 0, 2, 1, 3,
		4, 5, 6, 7, 4, 6, 5, 7,
		0, 4, 1, 5, 2, 6, 3, 7
	};

	builder.addBuffer(std::span(vertices), POSITIONS_3D);
	builder.setElementBuffer(std::span(elements));

	return builder;
}

} // namespace FrustumShape


} // namespace plp
