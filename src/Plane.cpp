#include "Plane.hpp"

namespace plp
{

namespace Plane
{

SharedBuffer verticesBO;
SharedBuffer normalsBO;
SharedBuffer elementsBO;
SharedBuffer texcoordsBO;

SharedBuffer getVerticesBO()
{
	if(!normalsBO)
		normalsBO = std::make_shared<StaticBufferStorage>(std::span(VERTICES));
	return normalsBO;
}

SharedBuffer getNormalsBO()
{
	if(!verticesBO)
		verticesBO = std::make_shared<StaticBufferStorage>(std::span(NORMALS));
	return verticesBO;
}

SharedBuffer getElementsBO()
{
	if(!elementsBO)
		elementsBO = std::make_shared<StaticBufferStorage>(std::span(INDICES));
	return elementsBO;
}

SharedBuffer getTexcoordsBO()
{
	if(!texcoordsBO)
		texcoordsBO = std::make_shared<StaticBufferStorage>(std::span(UVS));
	return texcoordsBO;
}

SharedRawModel sharedPlane;

SharedRawModel getTexturedPlane()
{
	if(!sharedPlane) {
		ModelBuilder builder(INDICES.size());
		builder.addBuffer(getVerticesBO(), { Attribute::FLOAT, Attribute::VEC3, Attribute::POSITIONS });
		builder.addBuffer(getNormalsBO(), { Attribute::FLOAT, Attribute::VEC3, Attribute::NORMALS });
		builder.addBuffer(getTexcoordsBO(), { Attribute::FLOAT, Attribute::VEC2, Attribute::UVS });
		builder.setElementBuffer(getElementsBO());
		sharedPlane = std::make_shared<RawModel>(builder.create().value());
	}
	return sharedPlane;
}

} // namespace Plane

} // namespace plp