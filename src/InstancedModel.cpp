#include "InstancedModel.hpp"
#include "TransformData.hpp"
#include "ModelBuilder.hpp"

namespace plp {

InstancedModel::InstancedModel(
	Model &&model,
	std::shared_ptr<TranformationBufferType> const& instancedBuffer,
	unsigned int instanceCount,
	unsigned int swapBufferCount
) :
		Model(std::forward<Model>(model)),
		// drawCommand(DrawElementsIndirectCommand { rawmodel->getNbVerticesToDraw(), instanceCount }),
		// drawBuffer(std::span(&drawCommand, sizeof(DrawElementsIndirectCommand))),
		instancedBuffer(instancedBuffer),
		transformations(instancedBuffer->map()),
		swapBufferCount(swapBufferCount),
		tfBuffer(std::make_shared<StaticBufferStorage>(instanceCount * sizeof(GlobalTransformData)))
{
	setInstanceCount(instanceCount);

	
	// cullingModel = std::make_unique<TransformFeedbackModel>(ModelBuilder::create(cullingShader, instanceCount, { , attributes}, nullptr, POINTS));
}

InstancedModel::InstancedModel(
	Model &&model,
	std::shared_ptr<TranformationBufferType> const &instancedBuffer,
	TransformFeedbackModel &&cullingModel,
	unsigned int instanceCount,
	unsigned int swapBufferCount) :
		Model(std::forward<Model>(model)),
		cullingModel(std::make_unique<TransformFeedbackModel>(std::forward<TransformFeedbackModel>(cullingModel))),
		instancedBuffer(instancedBuffer),
		transformations(instancedBuffer->map()),
		swapBufferCount(swapBufferCount)
{
	setInstanceCount(instanceCount);
}


void InstancedModel::update()
{
	swapCount++;
	// std::cout << "Update: write buffer = " << getWriteBufferIndex() << ", read buffer = " << getReadBufferIndex() << std::endl; 
	if(swapCount > swapBufferCount) {
		setBaseInstance(getInstanceCount() * getReadBufferIndex());
	}
}

void InstancedModel::render(const std::uint32_t queries, QueryData* data) const
{
	Model::render(queries, data);
}

unsigned int InstancedModel::getWriteBufferIndex() const
{
	return swapCount % (swapBufferCount + 1);
}

unsigned int InstancedModel::getReadBufferIndex() const
{
	return (swapCount - 1) % (swapBufferCount + 1);
}

}