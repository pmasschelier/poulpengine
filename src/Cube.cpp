#include "Cube.hpp"

namespace plp {

namespace Cube {

SharedBuffer cubeVBO;
SharedBuffer verticesBO;
SharedBuffer normalsBO;
SharedBuffer elementsBO;

SharedBuffer getCubeVBO()
{
    if(!cubeVBO)
        cubeVBO = std::make_shared<StaticBufferStorage>(std::span(CUBE));
    return cubeVBO;
}

SharedBuffer getVerticesBO()
{
    if(!verticesBO)
        verticesBO = std::make_shared<StaticBufferStorage>(std::span(VERTICES));
    return verticesBO;
}

SharedBuffer getNormalsBO()
{
    if(!normalsBO)
        normalsBO = std::make_shared<StaticBufferStorage>(std::span(NORMALS));
    return normalsBO;
}

SharedBuffer getElementsBO()
{
    if(!elementsBO)
        elementsBO = std::make_shared<StaticBufferStorage>(std::span(INDICES));
    return elementsBO;
}



std::array<float, 72> computeColorsByFace(std::array<glm::vec3, 6> const& colors)
{
    std::array<float, 72> mapped_colors;

    for(unsigned i = 0; i < 6; i++) {
        for(unsigned j = 0; j < 4; j++) {
            mapped_colors[i*12 + j*3 + 0] = colors[i].r;
            mapped_colors[i*12 + j*3 + 1] = colors[i].g;
            mapped_colors[i*12 + j*3 + 2] = colors[i].b;
        }
    }

    return mapped_colors;
}

std::array<float, 48> computeTexCoords(unsigned widthTex, unsigned heightTex, std::array<unsigned, 6> const& index)
{
    std::array<float, 48> texCoords;

    float uX(1/(float)widthTex), uY(1/(float)heightTex);

    for(unsigned i=0; i<6; i++) {
        unsigned posXonTex = index[i] % widthTex;
        unsigned posYonTex = index[i] / widthTex;

        texCoords[8*i + 0] = (posXonTex) * uX;
        texCoords[8*i + 1] = (posYonTex) * uY;
        
        texCoords[8*i + 2] = (posXonTex + 1) * uX;
        texCoords[8*i + 3] = (posYonTex) * uY;
        
        texCoords[8*i + 4] = (posXonTex + 1) * uX;
        texCoords[8*i + 5] = (posYonTex + 1) * uY;
        
        texCoords[8*i + 6] = (posXonTex) * uX;
        texCoords[8*i + 7] = (posYonTex + 1) * uY;
    }

    return texCoords;
}

ModelBuilder createColoredCube(std::array<glm::vec3, 6> const& colors)
{
	ModelBuilder builder(INDICES.size());
	builder.addBuffer(getVerticesBO(), { Attribute::FLOAT, Attribute::VEC3, Attribute::POSITIONS });

	auto colorVertices = computeColorsByFace(colors);

	builder.addBuffer(std::span(colorVertices), { Attribute::FLOAT, Attribute::VEC3, Attribute::COLORS });
	builder.setElementBuffer(getElementsBO());
	builder.setBoundingBox(glm::vec3(0.f), glm::vec3(1.f));
	return builder;
}

ModelBuilder createTexturedCube(unsigned widthTex, unsigned heightTex, std::array<unsigned, 6> const& index) {
	ModelBuilder builder(INDICES.size());
	builder.addBuffer(getVerticesBO(), POSITIONS_3D);

	auto uvs = computeTexCoords(widthTex, heightTex, index);
	builder.addBuffer(std::span(uvs), TEXCOORDS);
	builder.setElementBuffer(getElementsBO());
	builder.setBoundingBox(glm::vec3(0.f), glm::vec3(1.f));
	return builder;
}

std::shared_ptr<RawModel> createNudeCube()
{
	static std::shared_ptr<RawModel> cube;
	if(cube) return cube;
	ModelBuilder builder(CUBE.size() / 3);
	builder.addBuffer(getCubeVBO(), POSITIONS_3D);
	builder.setBoundingBox(glm::vec3(0.f), glm::vec3(1.f));
	cube = std::make_shared<RawModel>(builder.create().value());
	return cube;
}

}

}

