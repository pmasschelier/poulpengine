#include "ShadowMap.hpp"

#include "FramebufferBuilder.hpp"

namespace plp
{

ShadowMap::ShadowMap(glm::uvec2 resolution)
{
	fb = FramebufferBuilder::createDepthMap(resolution);
}

} // namespace plp
