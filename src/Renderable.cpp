#include "Renderable.hpp"

namespace plp
{

static constexpr GLenum queryBitToTarget(int bit) {
	if(bit == Renderable::PRIMITIVES_GENERATED) return GL_PRIMITIVES_GENERATED;
	if(bit == Renderable::TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN) return GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN;
	if(bit == Renderable::TRANSFORM_FEEDBACK_OVERFLOW) return GL_TRANSFORM_FEEDBACK_OVERFLOW;
	if(bit == Renderable::TRANSFORM_FEEDBACK_STREAM_OVERFLOW) return GL_TRANSFORM_FEEDBACK_STREAM_OVERFLOW;
	if(bit == Renderable::SAMPLES_PASSED) return GL_SAMPLES_PASSED;
	if(bit == Renderable::ANY_SAMPLES_PASSED) return GL_ANY_SAMPLES_PASSED;
	if(bit == Renderable::ANY_SAMPLES_PASSED_CONSERVATIVE) return GL_ANY_SAMPLES_PASSED_CONSERVATIVE;
	if(bit == Renderable::TIME_ELAPSED) return GL_TIME_ELAPSED;
	if(bit == Renderable::VERTICES_SUBMITTED) return GL_VERTICES_SUBMITTED;
	if(bit == Renderable::PRIMITIVES_SUBMITTED) return GL_PRIMITIVES_SUBMITTED;
	if(bit == Renderable::VERTEX_SHADER_INVOCATIONS) return GL_VERTEX_SHADER_INVOCATIONS;
	if(bit == Renderable::TESS_CONTROL_SHADER_PATCHES) return GL_TESS_CONTROL_SHADER_PATCHES;
	if(bit == Renderable::TESS_EVALUATION_SHADER_INVOCATIONS) return GL_TESS_EVALUATION_SHADER_INVOCATIONS;
	if(bit == Renderable::GEOMETRY_SHADER_INVOCATIONS) return GL_GEOMETRY_SHADER_INVOCATIONS;
	if(bit == Renderable::GEOMETRY_SHADER_PRIMITIVES_EMITTED) return GL_GEOMETRY_SHADER_PRIMITIVES_EMITTED;
	if(bit == Renderable::CLIPPING_INPUT_PRIMITIVES) return GL_CLIPPING_INPUT_PRIMITIVES;
	if(bit == Renderable::CLIPPING_OUTPUT_PRIMITIVES) return GL_CLIPPING_OUTPUT_PRIMITIVES;
	if(bit == Renderable::FRAGMENT_SHADER_INVOCATIONS) return GL_FRAGMENT_SHADER_INVOCATIONS;
	if(bit == Renderable::COMPUTE_SHADER_INVOCATIONS) return GL_COMPUTE_SHADER_INVOCATIONS;
	return 0;
}

static void writeQueryResult(std::pair<GLenum, GLuint> query, Renderable::QueryData* data) {
	std::uint32_t value;
	glGetQueryObjectuiv(query.second, GL_QUERY_RESULT, &value);
	if(query.first == GL_PRIMITIVES_GENERATED) data->primitives_generated = value;
	if(query.first == GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN) data->transform_feedback_primitives_written = value;
	if(query.first == GL_TRANSFORM_FEEDBACK_OVERFLOW) data->transform_feedback_overflow = value;
	if(query.first == GL_TRANSFORM_FEEDBACK_STREAM_OVERFLOW) data->transform_feedback_stream_overflow = value;
	if(query.first == GL_SAMPLES_PASSED) data->samples_passed = value;
	if(query.first == GL_ANY_SAMPLES_PASSED) data->any_samples_passed = value;
	if(query.first == GL_ANY_SAMPLES_PASSED_CONSERVATIVE) data->any_samples_passed_conservative = value;
	if(query.first == GL_TIME_ELAPSED) data->time_elapsed = value;
	if(query.first == GL_VERTICES_SUBMITTED) data->vertices_submitted = value;
	if(query.first == GL_PRIMITIVES_SUBMITTED) data->primitives_submitted = value;
	if(query.first == GL_VERTEX_SHADER_INVOCATIONS) data->vertex_shader_invocations = value;
	if(query.first == GL_TESS_CONTROL_SHADER_PATCHES) data->tess_control_shader_patches = value;
	if(query.first == GL_TESS_EVALUATION_SHADER_INVOCATIONS) data->tess_evaluation_shader_invocations = value;
	if(query.first == GL_GEOMETRY_SHADER_INVOCATIONS) data->geometry_shader_invocations = value;
	if(query.first == GL_GEOMETRY_SHADER_PRIMITIVES_EMITTED) data->geometry_shader_primitives_emitted = value;
	if(query.first == GL_CLIPPING_INPUT_PRIMITIVES) data->clipping_input_primitives = value;
	if(query.first == GL_CLIPPING_OUTPUT_PRIMITIVES) data->clipping_output_primitives = value;
	if(query.first == GL_FRAGMENT_SHADER_INVOCATIONS) data->fragment_shader_invocations = value;
	if(query.first == GL_COMPUTE_SHADER_INVOCATIONS) data->compute_shader_invocations = value;
}

void Renderable::setBaseInstance(unsigned int baseInstance)
{
	this->baseInstance = baseInstance;
}

void Renderable::setInstanceCount(unsigned int instanceCount)
{
	this->instanceCount = instanceCount;
}

unsigned int Renderable::getInstanceCount() const {
	return instanceCount;
}

const RawModel& Renderable::getRawModel() const
{
	return *rawmodel;
}

Renderable::Renderable(RawModel &&rawmodel, std::shared_ptr<AbstractShaderProgram> program) :
	Renderable(std::make_shared<RawModel>(std::forward<RawModel>(rawmodel)), program)
{
}

Renderable::Renderable(std::shared_ptr<const RawModel> rawmodel, std::shared_ptr<AbstractShaderProgram> program) : 
	program(program),
	rawmodel(rawmodel)
{
}

void Renderable::render() const
{
	program->bind([this]() {
		rawmodel->draw(instanceCount, baseInstance);
	});
}

void Renderable::render(StaticBufferStorage& command) const
{
	program->bind([this, &command]() {
		rawmodel->draw(command);
	});
}

void Renderable::render(const std::uint32_t queries, QueryData* data) const {
	std::vector<std::pair<GLenum, GLuint>> queryObjects;

	[[unlikely]]
	if(queries != 0) {
		for(int i = 0; i < 18; i++) {
			int bit = 1 << i;
			if(queries & bit) {
				GLenum target = queryBitToTarget(bit);
				GLuint id;
				glGenQueries(1, &id);
				glBeginQuery(target, id);
				queryObjects.push_back(std::make_pair(target, id));
			}
		}
	}

	render();

	for(auto& obj: queryObjects) {
		glEndQuery(obj.first);
		writeQueryResult(obj, data);
	}
}


DrawIndirectCommand Renderable::createIndirectRenderingCommand() const
{
	DrawIndirectCommand command;
	if(rawmodel->hasElementBuffer()) {
		command = DrawElementsIndirectCommand {
			rawmodel->getNbVerticesToDraw(),
			instanceCount,
			rawmodel->getFirstVertex(),
			0,
			baseInstance
		};
		return command;
	}
	command = DrawArraysIndirectCommand {
		rawmodel->getNbVerticesToDraw(),
		instanceCount,
		rawmodel->getFirstVertex(),
		baseInstance
	};
	return command;
}

} // namespace plp
