#include "TransformFeedbackObject.hpp"
#include <iostream>

namespace plp
{

TransformFeedbackObject::TransformFeedbackObject()
{
	glCreateTransformFeedbacks(1, &id);
	buffers.resize(getMaxTransformFeedbackBuffers());
}

TransformFeedbackObject::~TransformFeedbackObject()
{
	glDeleteTransformFeedbacks(1, &id);
}


/* TransformFeedbackObject::TransformFeedbackObject(TransformFeedbackObject&& other) :
	ObjectGL(std::move(other)),
	query(std::move(other.query)),
	buffers(std::move(other.buffers)),
	recordPrimitivesGenerated(std::move(other.recordPrimitivesGenerated))
{}

TransformFeedbackObject& TransformFeedbackObject::operator=(TransformFeedbackObject&& other)
{
	ObjectGL::operator=(std::move(other));
	query = std::move(other.query);
	buffers = std::move(buffers);
	recordPrimitivesGenerated = std::move(recordPrimitivesGenerated);
} */

bool TransformFeedbackObject::setBuffer(std::shared_ptr<StaticBufferStorage> const& buffer, unsigned char index)
{
	if(index >= getMaxTransformFeedbackBuffers()) {
		std::cerr << "Error: provided index: " << index << " is greater than GL_MAX_TRANSFORM_FEEDBACK_BUFFERS (" << buffers.size() << ")" << std::endl;
		return false;
	}
	buffers[index] = buffer;
	return true;
}

std::shared_ptr<StaticBufferStorage> TransformFeedbackObject::getBuffer(unsigned char index) const
{
	return index < buffers.size() ? buffers[index] : nullptr;
}

GLuint TransformFeedbackObject::record(GLenum primitiveMode, std::function<void()> drawAction, bool save) const
{
	auto draw = [primitiveMode, &drawAction]() {
		glBeginTransformFeedback(primitiveMode);
		drawAction();
		glEndTransformFeedback();
	};

	GLint saved{};
	if(save)
		glGetIntegerv(GL_TRANSFORM_FEEDBACK_BINDING, &saved);
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, id);
	for(unsigned i = 0; i < buffers.size(); i++) {
		if(buffers[i])
			buffers[i]->bind<TRANSFORM_FEEDBACK_BUFFER>(i);
		else
			glBindBufferBase(TRANSFORM_FEEDBACK_BUFFER, i, 0);
	}

	if(recordPrimitivesGenerated)
		return query.querySync(draw);
	draw();
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, saved);
	return 0;
}


unsigned int TransformFeedbackObject::getMaxTransformFeedbackBuffers()
{
	static GLint res = -1;
	if(-1 == res)
		glGetIntegerv(GL_MAX_TRANSFORM_FEEDBACK_BUFFERS, &res);
	return static_cast<unsigned int>(res);
}

/* GLuint TransformFeedbackObject::bind(bool save) const
{
	GLint current = 0;
	if(save)
		glGetIntegerv(GL_TRANSFORM_FEEDBACK_BINDING, &current);
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, id);
	return current;
}

void TransformFeedbackObject::unbind(GLuint resetValue) const
{
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, resetValue);
} */



} // namespace plp
