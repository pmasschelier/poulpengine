#include "Textures/DepthTexture.hpp"

namespace plp
{

std::optional<DepthTexture>
DepthTexture::createEmpty(glm::uvec2 size)
{
	DepthTexture texture(TEXTURE_2D, DEPTH_COMPONENT32F);
	if(!texture.create(size))
		return std::nullopt;
	return texture;
}

} // namespace plp
