#include "SFML-based/Image.hpp"
#include "Textures/CubemapTexture.hpp"

namespace plp
{

CubemapTexture::CubemapTexture() :
	Texture2DStorage(TEXTURE_CUBE_MAP, RGB8)
{}

/* 0x8515	GL_TEXTURE_CUBE_MAP_POSITIVE_X
 * 0x8516	GL_TEXTURE_CUBE_MAP_NEGATIVE_X
 * 0x8517	GL_TEXTURE_CUBE_MAP_POSITIVE_Y
 * 0x8518	GL_TEXTURE_CUBE_MAP_NEGATIVE_Y
 * 0x8519	GL_TEXTURE_CUBE_MAP_POSITIVE_Z
 * 0x851A	GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
 */

std::optional<CubemapTexture> CubemapTexture::createFromFiles(std::array<std::filesystem::path, 6> const& filenames)
{
	glm::uvec2 lastSize;
	std::array<plp::Image, 6> images;
    for (unsigned int i = 0 ; i < 6 ; i++) {
        
        if(!images[i].loadFromFile(filenames[i], false))
			return std::nullopt;
		
		if(i > 0 && lastSize != images[i].getSize()) {
            std::cerr << "Error while loading: " << filenames[i] << std::endl;
			std::cerr << "Reason: all cubemap texture faces must have the same size." << std::endl;
            return std::nullopt;
		}

		lastSize = images[i].getSize();

	}

	CubemapTexture texture;
	if(!texture.create(lastSize))
		return std::nullopt;

    for (unsigned int i = 0 ; i < 6 ; i++) {
		glTextureSubImage3D(
			texture.id,
			0, 0, 0, i,
			lastSize.x, lastSize.y, 1,
			GL_RGBA, GL_UNSIGNED_BYTE,
			images[i].getPixelsPtr()
		);
	}
	
	texture.setSmooth(true);
	texture.setRepeated(false);

    return texture;
}

std::optional<CubemapTexture> CubemapTexture::createFromTileMap(unsigned widthTex, unsigned heightTex, std::array<unsigned, 6> const& index, std::filesystem::path const& filename)
{
    plp::Image image, tile;

    if(!image.loadFromFile(filename))
        return std::nullopt;

    unsigned uX(image.getSize().x/widthTex), 
        uY(image.getSize().y/heightTex);
	
	CubemapTexture texture;
	if(!texture.create({uX, uY}))
		return std::nullopt;
    
    for (unsigned int i = 0 ; i < index.size() ; i++) {
        unsigned posXonTex = index[i] % widthTex;
        unsigned posYonTex = index[i] / widthTex;

        if(!tile.copy(image, {}, plp::IntRect({posXonTex*uX, posYonTex*uY}, {uX, uY}), true))
			return std::nullopt;

		glTextureSubImage3D(
			texture.id, 0, 0, 0, 
			i,
			tile.getSize().x, tile.getSize().y, 1,
			GL_RGBA, GL_UNSIGNED_BYTE,
			tile.getPixelsPtr()
		);
    }
	
	texture.setSmooth(true);
	texture.setRepeated(false);

    return texture;
}

} // namespace plp
