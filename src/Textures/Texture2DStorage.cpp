#include "Textures/Texture2DStorage.hpp"

namespace plp
{

bool Texture2DStorage::create(const glm::uvec2& size)
{
    // Check if texture parameters are valid before creating it
    if ((size.x == 0) || (size.y == 0))
    {
        std::cerr << "Failed to create texture, invalid size (" << size.x << "x" << size.y << ")" << std::endl;
        return false;
    }

    // Check the maximum texture size
    const unsigned int maxSize = Texture::getMaximumSize();
    if ((size.x > maxSize) || (size.y > maxSize))
    {
        std::cerr	<< "Failed to create texture, its internal size is too high "
        			<< "(" << size.x << "x" << size.y << ", "
            		<< "maximum is " << maxSize << "x" << maxSize << ")" << std::endl;
        return false;
    }

    // All the validity checks passed, we can store the new texture settings
    m_size          = size;
    m_pixelsFlipped = false;
    m_fboAttachment = false;

    // Initialize the texture
	glTextureStorage2D(id, 1, format, m_size.x, m_size.y);

	GLint wrap = Texture::isRepeated() ? GL_REPEAT : GL_CLAMP_TO_EDGE;
	GLint filter = Texture::isSmooth() ? GL_LINEAR : GL_NEAREST;
	glTextureParameteri(id, GL_TEXTURE_WRAP_S, wrap);
	glTextureParameteri(id, GL_TEXTURE_WRAP_T, wrap);
    glTextureParameteri(id, GL_TEXTURE_MAG_FILTER, filter);
    glTextureParameteri(id, GL_TEXTURE_MIN_FILTER, filter);

    m_hasMipmap = false;

    return true;
}

////////////////////////////////////////////////////////////
void Texture2DStorage::updateWrapProperty()
{
	GLint param =  m_isRepeated ? GL_REPEAT : GL_CLAMP_TO_EDGE;
	glTextureParameteri(id, GL_TEXTURE_WRAP_S, param);
	glTextureParameteri(id, GL_TEXTURE_WRAP_T, param);
}

glm::uvec2 Texture2DStorage::getSize() const
{
	return m_size;
}

void Texture2DStorage::swap(Texture2DStorage& right) noexcept
{
	Texture::swap(right);
	std::swap(m_size, right.m_size);
}

} // namespace plp
