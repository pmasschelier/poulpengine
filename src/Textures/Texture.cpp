#include "Textures/Texture.hpp"

#include <iostream>
#include <utility>

namespace plp {

Texture::Texture(Target target, SizedInternalFormat format)
	: target(target), format(format)
{
	glCreateTextures(target, 1, &id);
}

////////////////////////////////////////////////////////////
Texture::~Texture()
{
	// Destroy the OpenGL texture
	if(glIsTexture(id))
		glDeleteTextures(1, &id);
}

void Texture::updateSmoothProperty()
{
	glTextureParameteri(id, GL_TEXTURE_MAG_FILTER, m_isSmooth ? GL_LINEAR : GL_NEAREST);
	GLint param = m_hasMipmap ? 
		(m_isSmooth ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST_MIPMAP_LINEAR) :
		(m_isSmooth ? GL_LINEAR : GL_NEAREST);
	glTextureParameteri(id, GL_TEXTURE_MIN_FILTER, param);
}

////////////////////////////////////////////////////////////
void Texture::setSmooth(bool smooth)
{
	if (smooth == m_isSmooth)
		return;
	
	m_isSmooth = smooth;

	if(!id)
		return;
	updateSmoothProperty();
}


////////////////////////////////////////////////////////////
bool Texture::isSmooth() const
{
	return m_isSmooth;
}


////////////////////////////////////////////////////////////
void Texture::setSrgb(bool sRgb)
{
	m_sRgb = sRgb;
}


////////////////////////////////////////////////////////////
bool Texture::isSrgb() const
{
	return m_sRgb;
}


////////////////////////////////////////////////////////////
void Texture::setRepeated(bool repeated)
{
	if(repeated == m_isRepeated)
		return;
	
	m_isRepeated = repeated;

	if(!id)
		return;
		
	updateWrapProperty();
}


////////////////////////////////////////////////////////////
bool Texture::isRepeated() const
{
	return m_isRepeated;
}


////////////////////////////////////////////////////////////
bool Texture::generateMipmap()
{
	if (!id)
		return false;

	glGenerateTextureMipmap(id);
	GLint param = m_isSmooth ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST_MIPMAP_LINEAR;
	glTextureParameteri(id, GL_TEXTURE_MIN_FILTER, param);

	m_hasMipmap = true;

	return true;
}

////////////////////////////////////////////////////////////
unsigned int Texture::getMaximumSize()
{
	GLint value = -1;
	if(value == -1)
		glGetIntegerv(GL_MAX_TEXTURE_SIZE, &value);
	return static_cast<unsigned int>(value);
}

////////////////////////////////////////////////////////////
void Texture::swap(Texture& right) noexcept
{
	std::swap(id, right.id);
	std::swap(m_isSmooth, right.m_isSmooth);
	std::swap(m_sRgb, right.m_sRgb);
	std::swap(m_isRepeated, right.m_isRepeated);
	std::swap(m_pixelsFlipped, right.m_pixelsFlipped);
	std::swap(m_fboAttachment, right.m_fboAttachment);
	std::swap(m_hasMipmap, right.m_hasMipmap);
}

////////////////////////////////////////////////////////////
void swap(Texture& left, Texture& right) noexcept
{
	left.swap(right);
}

////////////////////////////////////////////////////////////
GLuint Texture::bind(GLuint unit, bool save) const
{
	GLint current{};
	if(save) {
		GLint current_unit;
		glGetIntegerv(GL_ACTIVE_TEXTURE, &current_unit);
		glActiveTexture(unit);
		glGetIntegerv(target, &current);
		glActiveTexture(current_unit);
	}
	glBindTextureUnit(unit, id);
	return current;
}

////////////////////////////////////////////////////////////
void Texture::unbind(GLuint unit, GLuint resetValue) const
{
	glBindTextureUnit(unit, resetValue);
}

}