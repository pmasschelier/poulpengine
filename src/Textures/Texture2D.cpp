#include "Framebuffer.hpp"
#include "Textures/Texture2D.hpp"

#include <cassert>
#include <iostream>
#include <cstring>

namespace plp {

Texture2D::Texture2D(SizedInternalFormat format) : Texture2DStorage(TEXTURE_2D, format)
	{}

std::optional<Texture2D>
Texture2D::createEmpty(glm::uvec2 const& size)
{
	Texture2D texture;
	if(!texture.create(size))
	return texture;
}

std::optional<Texture2D>
Texture2D::createDepthMap(glm::uvec2 const& size)
{
	Texture2D texture(DEPTH_COMPONENT32F);
	if(!texture.create(size))
	return texture;
}

////////////////////////////////////////////////////////////
std::optional<Texture2D> Texture2D::createFromFile(const std::filesystem::path& filename, const IntRect& area)
{
    Image image;
	if(!image.loadFromFile(filename))
		return std::nullopt;
    return createFromImage(image, area);
}


////////////////////////////////////////////////////////////
std::optional<Texture2D> Texture2D::createFromMemory(const void* data, std::size_t size, const IntRect& area)
{
    Image image;
	if(!image.loadFromMemory(data, size))
		return std::nullopt;
    return createFromImage(image, area);
}


////////////////////////////////////////////////////////////
std::optional<Texture2D> Texture2D::createFromStream(InputStream& stream, const IntRect& area)
{
    Image image;
	if(!image.loadFromStream(stream))
		return std::nullopt;
    return createFromImage(image, area);
}


////////////////////////////////////////////////////////////
std::optional<Texture2D> Texture2D::createFromImage(const Image& image, const IntRect& area)
{
    // Retrieve the image size
    int width = image.getSize().x;
	int height = image.getSize().y;

	bool ret = false;

	Texture2D texture;

    // Load the entire image if the source area is either empty or contains the whole image
    if (area.width == 0 || (area.height == 0) ||
        ((area.left <= 0) && (area.top <= 0) && (area.width >= width) && (area.height >= height)))
    {
        // Load the entire image
		ret = texture.create(image.getSize());
        if (!ret)
			return std::nullopt;
        texture.update(image);
    }
    else
    {
        // Load a sub-area of the image

        // Adjust the rectangle to the size of the image
        IntRect rect = area;
		rect.left = std::min(0, rect.left);
		rect.top = std::min(0, rect.top);
		rect.width = std::min(width - rect.left, rect.width);
		rect.height = std::min(height - rect.top, rect.height);

        // Create the texture and upload the pixels
		ret = texture.create(Vector2u(rect.getSize()));
        if (ret)
        {
			glTextureSubImage2D(
				texture.id,
				0,
				rect.left,
				rect.top,
				rect.width,
				rect.height,
				GL_RGBA,
				GL_UNSIGNED_INT,
				image.getPixelsPtr()
			);
        }
    }

	texture.generateMipmap();
	texture.setSmooth(true);
	texture.setRepeated(true);

	return texture;
}

std::optional<Texture2D> Texture2D::createFromFramebuffer(Framebuffer const& fb, const Vector2u& size, const Vector2u& dest)
{
	Texture2D texture;
	if(!texture.create(size))
		return std::nullopt;
	texture.update(fb, size, dest);
	return texture;
}


////////////////////////////////////////////////////////////
Image Texture2D::copyToImage() const
{
    // Create an array of pixels
    std::vector<std::uint8_t> pixels(m_size.x * m_size.y * 4);

	if(!m_pixelsFlipped) {
        // Texture is not flipped, we can use a direct copy
    	glGetTextureImage(id, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels.size(), pixels.data());
	}
    else
    {
        // All the pixels will first be copied to a temporary array
        std::vector<std::uint8_t> flippedPixels(m_size.x * m_size.y * 4);
    	glGetTextureImage(id, 0, GL_RGBA, GL_UNSIGNED_BYTE, flippedPixels.size(), flippedPixels.data());

        // Then we copy the useful pixels from the temporary array to the final one
        const std::uint8_t* src = flippedPixels.data();
        std::uint8_t* dst = pixels.data();
        int srcPitch = static_cast<int>(m_size.x * 4);
        const unsigned int dstPitch = m_size.x * 4;

        // Handle the case where source pixels are flipped vertically
		src += static_cast<unsigned int>(srcPitch * static_cast<int>((m_size.y - 1)));
		srcPitch = -srcPitch;

        for (unsigned int i = 0; i < m_size.y; ++i)
        {
            std::memcpy(dst, src, dstPitch);
            src += srcPitch;
            dst += dstPitch;
        }
    }

    // Create the image
    Image image;
    image.create(m_size, pixels.data());

    return image;
}


////////////////////////////////////////////////////////////
void Texture2D::update(const std::uint8_t* pixels)
{
    // Update the whole texture
    update(pixels, m_size, {0, 0});
}


////////////////////////////////////////////////////////////
void Texture2D::update(const std::uint8_t* pixels, const Vector2u& size, const Vector2u& dest)
{
    assert(dest.x + size.x <= m_size.x);
    assert(dest.y + size.y <= m_size.y);

    if (pixels && id)
    {
        // Copy pixels from the given array to the texture
		glTextureSubImage2D(id,
							0,
							static_cast<GLint>(dest.x),
							static_cast<GLint>(dest.y),
							static_cast<GLsizei>(size.x),
							static_cast<GLsizei>(size.y),
							GL_RGBA,
							GL_UNSIGNED_BYTE,
							pixels);
        glTextureParameteri(id, GL_TEXTURE_MIN_FILTER, m_isSmooth ? GL_LINEAR : GL_NEAREST);
        m_hasMipmap     = false;
        m_pixelsFlipped = false;
    }
}


////////////////////////////////////////////////////////////
void Texture2D::update(const Texture2D& texture)
{
    // Update the whole texture
    update(texture, {0, 0});
}


////////////////////////////////////////////////////////////
void Texture2D::update(const Texture2D& texture, const Vector2u& dest)
{
    assert(dest.x + texture.m_size.x <= m_size.x);
    assert(dest.y + texture.m_size.y <= m_size.y);

    if (!id || !texture.id)
		return;
	
	glCopyImageSubData(
		id, TEXTURE_2D, 0, 0, 0, 0, 
		id, TEXTURE_2D, 0, dest.x, dest.y, 0,
		texture.getSize().x, texture.getSize().y, 1
	);

	// Set the parameters of this texture
	updateSmoothProperty();
	m_hasMipmap     = false;
	m_pixelsFlipped = false;
}


////////////////////////////////////////////////////////////
void Texture2D::update(const Image& image)
{
    // Update the whole texture
    update(image.getPixelsPtr(), image.getSize(), {0, 0});
}


////////////////////////////////////////////////////////////
void Texture2D::update(const Image& image, const Vector2u& dest)
{
    update(image.getPixelsPtr(), image.getSize(), dest);
}

////////////////////////////////////////////////////////////
void Texture2D::update(Framebuffer const& fb, const Vector2u& size, const Vector2u& dest)
{
    assert(dest.x + size.x <= m_size.x);
    assert(dest.y + size.y <= m_size.y);

	// Copy pixels from the back-buffer to the texture
	Framebuffer::bindReadBuffer(fb);
	glMemoryBarrier(GL_FRAMEBUFFER_BARRIER_BIT);
	glCopyTextureSubImage2D(id,
							0,
							static_cast<GLint>(dest.x),
							static_cast<GLint>(dest.y),
							0,
							0,
							static_cast<GLsizei>(size.x),
							static_cast<GLsizei>(size.y));
	updateSmoothProperty();
	m_hasMipmap     = false;
	m_pixelsFlipped = true;
}

}