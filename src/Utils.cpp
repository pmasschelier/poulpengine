#include <numeric>
#include <fstream>
#include <iostream>

#include "Utils.hpp"

namespace plp
{

std::optional<std::string> readFile(std::filesystem::path const& path)
{
    std::string code;
    std::ifstream file(path, std::ios::in);
    std::stringstream sstr;
    
	if(!file.is_open()) {
		std::cerr << "Error: Unable to open " << path << std::endl;
		return std::nullopt;
	}
    sstr << file.rdbuf();
	code = sstr.str();
	file.close();
	
    if(!sstr) {
        std::cerr << "Error: Unable to read " << path << "" << std::endl;
        return std::nullopt;
	}

    return code;
}

} // namespace plp
