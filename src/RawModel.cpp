#include "RawModel.hpp"

#include <utility>

namespace plp {

RawModel::RawModel(std::uint32_t nbVerticesToDraw, PrimitiveType type, std::uint32_t patchSize) : 
	nbVerticesToDraw(nbVerticesToDraw), type(type), patchSize(patchSize)
{
	if(type == PATCHES && patchSize == 0)
		throw std::runtime_error("Error: when creating a RawModel with type == PATCHES patchSize should be larger than 0.");
	glCreateVertexArrays(1, &id);
}

RawModel::~RawModel()
{
	glDeleteVertexArrays(1, &id);
}

void RawModel::setNbVerticesToDraw(std::uint32_t nb)
{
	nbVerticesToDraw = nb;
}

std::uint32_t RawModel::getNbVerticesToDraw() const
{
	return nbVerticesToDraw;
}

void RawModel::setFirstVertex(std::uint32_t nb)
{
	first = nb;
}

std::uint32_t RawModel::getFirstVertex() const
{
	return first;
}

PrimitiveType RawModel::getType() const
{
	return type;
}

glm::vec3 RawModel::getCenterAABB() const
{
	return center;
}

glm::vec3 RawModel::getExtentAABB() const
{
	return extent;
}

bool RawModel::hasElementBuffer() const
{
	return elementBuffer;
}

void RawModel::draw(StaticBufferStorage& command) const
{
	assert(id);
	// std::cout << "Draw VAO " << id << " type == POINTS: " << (type == POINTS) << " size == " << nbVerticesToDraw << std::endl;
	glBindVertexArray(id);
	command.bind<DRAW_INDIRECT_BUFFER>();

	if(PATCHES == type)
		glPatchParameteri(GL_PATCH_VERTICES, patchSize);
	if(elementBuffer)
		glDrawElementsIndirect(type, GL_UNSIGNED_INT, nullptr);
	else
		glDrawArraysIndirect(type, nullptr);

	glBindVertexArray(0);
}

void RawModel::draw(std::uint32_t instanceCount, unsigned int baseInstance) const
{
	assert(id);
	// std::cout << "Draw VAO " << id << " type == POINTS: " << (type == POINTS) << " size == " << nbVerticesToDraw << std::endl;
	glBindVertexArray(id);

#ifndef NDEBUG
	if(nbVerticesToDraw == 0)
		std::cerr << "Warning: Attempt to draw 0 elements of VAO n°" << id << std::endl;
#endif

	if(PATCHES == type)
		glPatchParameteri(GL_PATCH_VERTICES, patchSize);
	if(elementBuffer)
		glDrawElementsInstancedBaseInstance(type, nbVerticesToDraw, GL_UNSIGNED_INT, nullptr, instanceCount, baseInstance);
	else
		glDrawArraysInstancedBaseInstance(type, first, nbVerticesToDraw, instanceCount, baseInstance);

	glBindVertexArray(0);
}

void RawModel::draw(TransformFeedbackObject const &tf) const
{
	assert(id);
	// std::cout << "Draw XFB VAO " << id << " type == POINTS: " << (type == POINTS) << " size == " << nbVerticesToDraw << std::endl;
	glBindVertexArray(id);
	glDrawTransformFeedback(type, tf.getID());
	glBindVertexArray(0);
}

GLuint RawModel::getMaxVertexAttribs()
{
	static GLint value = -1;
	if(value == -1)
		glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &value);
	return static_cast<GLuint>(value);
}

GLuint RawModel::getMaxVertexAttribBindings()
{
	static GLint value = -1;
	if(value == -1)
		glGetIntegerv(GL_MAX_VERTEX_ATTRIB_BINDINGS, &value);
	return static_cast<GLuint>(value);
}

GLuint RawModel::getMaxVertexAttribStride()
{
	static GLint value = -1;
	if(value == -1)
		glGetIntegerv(GL_MAX_VERTEX_ATTRIB_STRIDE, &value);
	return static_cast<GLuint>(value);
}

GLuint RawModel::getMaxVertexAttribRelativeOffset()
{
	static GLint value = -1;
	if(value == -1)
		glGetIntegerv(GL_MAX_VERTEX_ATTRIB_RELATIVE_OFFSET, &value);
	return static_cast<GLuint>(value);
}

}
