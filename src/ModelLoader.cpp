#include "ModelLoader.hpp"
#include "Shaders/ShaderBuilder.hpp"
#include "Model.hpp"
#include "SceneManager.hpp"

#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

namespace plp
{

namespace 
{

class ModelLoaderImpl {

public:

	const aiScene* loadSceneFromFile(std::filesystem::path const &path);

	static std::optional<std::vector<ModelBuilder>>
	loadModelsFromScene(std::filesystem::path const &path, SceneManager& scene, const aiScene* assimpScene);

	static std::optional<ModelBuilder>
	loadNthModelFromScene(std::filesystem::path const &path, SceneManager& scene, const aiScene* assimpScene, unsigned int index);

	static ModelLoaderImpl& getInstance()
	{
		return instance;
	}

private:

	ModelLoaderImpl() = default;

	Assimp::Importer importer;

	static ModelLoaderImpl instance;

};

ModelLoaderImpl ModelLoaderImpl::instance;

} // namespace 


std::shared_ptr<ShaderProgram> ModelLoader::basicModelShader;

const aiScene* ModelLoaderImpl::loadSceneFromFile(std::filesystem::path const &path)
{
	std::cout << "Loading scene: " << path << std::endl;
	// And have it read the given file with some example postprocessing
	// Usually - if speed is not the most important aspect for you - you'll
	// probably to request more postprocessing than we do in this example.
	const aiScene *assimpScene = importer.ReadFile(path,
		aiProcess_Triangulate |
		aiProcess_GenSmoothNormals |
		aiProcess_JoinIdenticalVertices |
		aiProcess_GenBoundingBoxes);

	// If the import failed, report it
	if (nullptr == assimpScene)
		std::cerr << "Error while loading " << path << std::endl;
	return assimpScene;
}

std::optional<ModelBuilder> ModelLoaderImpl::loadNthModelFromScene(std::filesystem::path const &path, SceneManager& scene, const aiScene* assimpScene, unsigned int index)
{
	std::shared_ptr<Model> model;
	std::shared_ptr<Texture> texture;

	if(!assimpScene->HasMeshes() || index >= assimpScene->mNumMeshes) {
		std::cout << "Mesh n°" << index << " doesn't exist in " << path << std::endl;
		return std::nullopt;
	}

	const aiMesh *mesh = assimpScene->mMeshes[index];

	std::cout << "Load mesh n°" << index << ", name = \"" << mesh->mName.C_Str() << "\"" << std::endl;
	
	if(assimpScene->HasMaterials()) {
		unsigned int matIndex = mesh->mMaterialIndex;
		if(matIndex >= assimpScene->mNumMaterials) {
			std::cerr << "Material index of mesh n°" << index << " is greater than the number of materials loaded." << std::endl;
			return std::nullopt;
		}

		const aiMaterial *mat = assimpScene->mMaterials[matIndex];

		std::cout << "Loading material n°" << matIndex << "..." << std::endl;
		if (mat->GetTextureCount(aiTextureType_DIFFUSE) > 0)
		{
			aiString Path;

			if (mat->GetTexture(aiTextureType_DIFFUSE, 0, &Path, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS)
			{
				auto fullPath = path.parent_path().append(Path.data);
				auto tex = scene.getTexture(fullPath);
				if(tex) {
					texture = tex.value();
				}
				else {
					std::cout << "Loading texture " << fullPath << "..." << std::endl;
					texture = scene.loadTextureFromFile(fullPath, fullPath).value();
				}
			}
		}
	}

	std::size_t nbVerticesToDraw { mesh->HasFaces() ? 3 * mesh->mNumFaces : mesh->mNumVertices };
	plp::ModelBuilder builder(nbVerticesToDraw);
	builder.setName(mesh->mName.C_Str());

	if (mesh->HasPositions())
	{
		glm::vec3 min(mesh->mAABB.mMin.x, mesh->mAABB.mMin.y, mesh->mAABB.mMin.z);
		glm::vec3 max(mesh->mAABB.mMax.x, mesh->mAABB.mMax.y, mesh->mAABB.mMax.z);
		glm::vec3 center(0.5f * (min + max)), extent(0.5f * (max - min));
		builder.setBoundingBox(center, extent);
		std::cout << "Center: " << center.x << ", " << center.y << ", " << center.z << std::endl;
		std::cout << "Extent: " << extent.x << ", " << extent.y << ", " << extent.z << std::endl;
		builder.addBuffer(std::span(mesh->mVertices, mesh->mNumVertices), { Attribute::FLOAT, Attribute::VEC3, 0 });
	}
	if (mesh->HasNormals())
	{
		builder.addBuffer(std::span(mesh->mNormals, mesh->mNumVertices), NORMALS_3D);
	}
	// TODO: Manage multitextured models
	if (mesh->HasTextureCoords(0))
	{
		if(mesh->mNumUVComponents[0] != 2) {
			std::cerr << "Only 2D UVs are supported for models" << std::endl;
			return std::nullopt;
		}
		std::vector<glm::vec2> uvs;
		for(unsigned k = 0; k < mesh->mNumVertices; k++)
			uvs.push_back(glm::vec2(mesh->mTextureCoords[0][k].x, mesh->mTextureCoords[0][k].y));
		builder.addBuffer(std::span(uvs), TEXCOORDS);
	}
	if (mesh->HasFaces())
	{
		std::vector<unsigned int> elements;
		for (unsigned j = 0; j < mesh->mNumFaces; j++)
		{
			elements.push_back(mesh->mFaces[j].mIndices[0]);
			elements.push_back(mesh->mFaces[j].mIndices[1]);
			elements.push_back(mesh->mFaces[j].mIndices[2]);
		}
		builder.setElementBuffer(std::span(elements));
	}

	if(texture)
		builder.addTexture(texture);
	return builder;
}

std::optional<std::vector<ModelBuilder>> ModelLoaderImpl::loadModelsFromScene(std::filesystem::path const &path, SceneManager& scene, const aiScene* assimpScene)
{
	std::vector<ModelBuilder> models;
	if (assimpScene->HasMeshes())
	{
		std::cout << assimpScene->mNumMeshes << " meshes to load." << std::endl;
		for (unsigned i = 0; i < assimpScene->mNumMeshes; i++)
		{
			auto model = loadNthModelFromScene(path, scene, assimpScene, i);
			if(!model)
				return std::nullopt;
			models.push_back(model.value());
		}
	}
	return models;
}

void copyNodesWithMeshes(
	SceneManager& scene,
	Collection<Model> const& models,
	aiNode *node,
	std::shared_ptr<SceneNode> parentNode)
{
	aiVector3D scaling, position;
	aiQuaternion rotation;

	node->mTransformation.Decompose(scaling, rotation, position);
	parentNode->setTransformations(
		{position.x, position.y, position.z},
		{rotation.x, rotation.y, rotation.z, rotation.w},
		{scaling.x, scaling.y, scaling.z}
	);

	for (unsigned i = 0; i < node->mNumMeshes; i++) {
		parentNode->addModel(models[node->mMeshes[i]]);
	}

	// continue for all child nodes
	for (unsigned i = 0; i < node->mNumChildren; i++) {
		auto childNode = scene.createSceneNode(node->mChildren[i]->mName.C_Str(), parentNode);
		copyNodesWithMeshes(scene, models, node->mChildren[i], childNode);
	}
}

Opt<SceneNode> ModelLoader::loadSceneFromFile(SceneManager &scene, std::filesystem::path const &path)
{
	const aiScene* assimpScene = ModelLoaderImpl::getInstance().loadSceneFromFile(path);
	if(!assimpScene)
		return std::nullopt;
	// Now we can access the file's contents.

	auto ret = ModelLoaderImpl::loadModelsFromScene(path, scene, assimpScene);
	if(!ret) {
		std::cerr << "Error: Failed to upload model from scene" << std::endl;
		return std::nullopt;
	}
	
	std::vector<ModelBuilder> model_builders = std::move(ret.value());
	Collection<Model> models;
	for(auto& builder: model_builders) {
		auto model = builder.create(getBasicModelShader());
		if(!model) {
			std::cerr << "Error: Failed to build model: " << builder.getName() << std::endl;
			return std::nullopt;
		}
		auto shared_model = scene.addModel(builder.getName(), std::move(model.value()), true);
		if(!shared_model) {
			std::cerr << "Error: Failed to add node: " << builder.getName() << std::endl;
			return std::nullopt;
		}
		models.push_back(shared_model.value());
	}

	aiNode* assimpRootNode = assimpScene->mRootNode;
	auto rootNode = scene.createSceneNode(assimpRootNode->mName.C_Str());
	copyNodesWithMeshes(scene, models, assimpRootNode, rootNode);

	return rootNode;
}

std::optional<std::vector<ModelBuilder>> ModelLoader::loadModelsFromFile(SceneManager &scene, std::filesystem::path const &path)
{
	const aiScene* assimpScene = ModelLoaderImpl::getInstance().loadSceneFromFile(path);
	if(!assimpScene)
		return std::nullopt;
	// Now we can access the file's contents.

	return ModelLoaderImpl::loadModelsFromScene(path, scene, assimpScene);
}

std::optional<ModelBuilder> ModelLoader::loadNthModelFromFile(SceneManager &scene, std::filesystem::path const &path, unsigned int index)
{
	const aiScene* assimpScene = ModelLoaderImpl::getInstance().loadSceneFromFile(path);
	if(!assimpScene)
		return std::nullopt;
	// Now we can access the file's contents.

	return ModelLoaderImpl::loadNthModelFromScene(path, scene, assimpScene, index);
}

std::shared_ptr<ShaderProgram> ModelLoader::getBasicModelShader()
{
	if (!basicModelShader)
	{
		ShaderBuilder builder;
		builder.addShader(ShaderBuilder::getCommonShader(VERTEX_TEXTURE_SOURCE));
		builder.addShader(ShaderBuilder::getCommonShader(FRAGMENT_TEXTURE_SOURCE));
		basicModelShader = std::make_shared<ShaderProgram>(builder.createProgram().value());
	}
	return basicModelShader;
}

} // namespace plp
