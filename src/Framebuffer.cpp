#include "Framebuffer.hpp"

#include <glm/gtc/type_ptr.hpp>

namespace plp
{

Framebuffer::Framebuffer()
{
	glCreateFramebuffers(1, &id);
}

Framebuffer::~Framebuffer()
{
	glDeleteFramebuffers(1, &id);
}

bool Framebuffer::clearColor(std::uint16_t attachmentIndex, glm::vec4 const& rgba)
{
	if(attachmentIndex < colorAttachments.size())
		return false;
	glClearNamedFramebufferfv(id, GL_COLOR, attachmentIndex, glm::value_ptr(rgba));
	return true;
}

bool Framebuffer::clearDepth(GLfloat value)
{
	glClearNamedFramebufferfv(id, GL_DEPTH, 0, &value);
}

bool Framebuffer::clearStencil(GLint value)
{
	glClearNamedFramebufferiv(id, GL_STENCIL, 0, &value);
}

bool Framebuffer::clearStencilAndDepth(GLfloat depthValue, GLuint stencilValue)
{
	glClearNamedFramebufferfi(id, GL_DEPTH_STENCIL, 0, depthValue, stencilValue);
}

void Framebuffer::bindReadBuffer(Framebuffer const& fb)
{
	glBindFramebuffer(GL_READ_BUFFER, fb.id);
}

void Framebuffer::bindDrawBuffer(Framebuffer const& fb)
{
	glBindFramebuffer(GL_DRAW_BUFFER, fb.id);
}

GLuint Framebuffer::getMaxColorAttachments() {
	static GLint value = -1;
	if(value == -1)
		glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &value);
	return static_cast<GLuint>(value);
}


GLuint Framebuffer::getMaxFramebufferWith()
{
	static GLint value = -1;
	if(value == -1)
		glGetIntegerv(GL_MAX_FRAMEBUFFER_WIDTH, &value);
	return static_cast<GLuint>(value);
}

GLuint Framebuffer::getMaxFramebufferHeight()
{
	static GLint value = -1;
	if(value == -1)
		glGetIntegerv(GL_MAX_FRAMEBUFFER_HEIGHT, &value);
	return static_cast<GLuint>(value);
}

GLuint Framebuffer::getMaxFramebufferLayers()
{
	static GLint value = -1;
	if(value == -1)
		glGetIntegerv(GL_MAX_FRAMEBUFFER_LAYERS, &value);
	return static_cast<GLuint>(value);
}

GLuint Framebuffer::getMaxFramebufferSamples()
{
	static GLint value = -1;
	if(value == -1)
		glGetIntegerv(GL_MAX_FRAMEBUFFER_SAMPLES, &value);
	return static_cast<GLuint>(value);
}
	
} // namespace plp