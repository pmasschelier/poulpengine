#include "BoundingBox.hpp"

namespace plp
{

BoundingBox BoundingBox::transform(glm::mat4 const &transform)
{
	const glm::vec3 transformedCenter { transform * glm::vec4(center, 1.f) };
	const glm::vec3 left { transform[0] * extent.x };
	const glm::vec3 up { transform[1] * extent.y };
	const glm::vec3 forward { transform[2] * extent.z };

	const auto getAlignedAxis = [left, up, forward](const glm::vec3& axis) {
		return glm::abs(glm::dot(axis, left)) + glm::abs(glm::dot(axis, up)) + glm::abs(glm::dot(axis, forward));
	};
	const float newX = getAlignedAxis(glm::vec3(1.f, 0.f, 0.f));
	const float newY = getAlignedAxis(glm::vec3(0.f, 1.f, 0.f));
	const float newZ = getAlignedAxis(glm::vec3(0.f, 0.f, 1.f));

	return BoundingBox(transformedCenter, glm::vec3(newX, newY, newZ));
}

} // namespace plp