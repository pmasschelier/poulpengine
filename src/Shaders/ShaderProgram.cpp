#include "Shaders/ShaderProgram.hpp"
#include "Shaders/ProgramInterface.hpp"
#include "Utils.hpp"

#include <algorithm>
#include <array>
#include <iostream>
#include <map>
#include <utility>

#include <thread>
#include <chrono>
#include "Shaders/ShaderProgram.hpp"

namespace plp
{

ShaderProgram::ShaderProgram(std::uint8_t types)
{
	id = glCreateProgram();
	if(!id)
		throw std::runtime_error("ShaderProgram couldn't be created (OpenGL error)");
	this->types = types;
}

void ShaderProgram::outputInfoLog(std::ostream& out) const
{
	int InfoLogLength;
	glGetProgramiv(id, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> ProgramErrorMessage(InfoLogLength + 1);
	glGetProgramInfoLog(id, InfoLogLength, NULL, &ProgramErrorMessage[0]);
	out << &ProgramErrorMessage[0] << std::endl;
}

bool ShaderProgram::linkedSuccessfully() const
{
	GLint result = GL_FALSE;
	glGetProgramiv(id, GL_LINK_STATUS, &result);
	return result;
}

bool ShaderProgram::isSeparable() const
{
	return separable;
}

bool ShaderProgram::isRetrievable() const
{
	return retrievable;
}

void ShaderProgram::bind(std::function<void()> const& action, bool save) const
{
	GLint saved{};
	if(save)
		glGetIntegerv(GL_CURRENT_PROGRAM, &saved);
	glUseProgram(id);
	action();
	glUseProgram(saved);
}

bool ShaderProgram::attach(AbstractShaderObject const& shader)
{
	if(!(ShaderObject::getTypeBit(shader.getType()) & types)) {
		std::cerr << "An invalid shader object was provided (type mismatch)" << std::endl;
		return false;
	}

	#ifndef NDEBUG
		std::cout << "Attach shader object n°" << shader.getID() << "..." << std::endl;
#endif
	glAttachShader(getID(), shader.getID());
	return true;
}

void ShaderProgram::detach(AbstractShaderObject const& shader)
{
	#ifndef NDEBUG
		std::cout << "Attach shader object n°" << shader.getID() << "..." << std::endl;
#endif
	glDetachShader(getID(), shader.getID());
}

bool ShaderProgram::link(bool separable, bool retrievable)
{
	// can the program be bound for individual pipeline stages using UseProgramStages ?
	if (separable)
		glProgramParameteri(getID(), GL_PROGRAM_SEPARABLE, GL_TRUE);
	// can the program binary be retrieved later ?
	if (retrievable)
		glProgramParameteri(getID(), GL_PROGRAM_BINARY_RETRIEVABLE_HINT, GL_TRUE);

	glLinkProgram(getID());

	if (!linkedSuccessfully())
	{
#ifndef NDEBUG
		std::cout << "Error while linking." << std::endl;
#endif
		outputInfoLog(std::cerr);
		return false;
	}

	this->separable = separable;
	this->retrievable = retrievable;
	return true;
}

inline GLint ShaderProgram::getUniformLocation(std::string const &name) const {
	// int GetUniformLocation(uint program, const char *name);
	// is equivalent to :
	// glGetProgramResourceLocation(program, UNIFORM, name);
	
	if(locations.contains(name))
		return locations[name];
	GLint location = glGetUniformLocation(id, name.c_str());
	locations[name] = location;
	return location;
}

bool ShaderProgram::setUniform(const std::string &name, bool value) {
	GLint location = getUniformLocation(name);
	glProgramUniform1i(id, location, (int)value);
	return location != -1;
}

bool ShaderProgram::setUniform(const std::string &name, unsigned int value) {
	GLint location = getUniformLocation(name);
	glProgramUniform1ui(id, location, value);
	return location != -1;
}

bool ShaderProgram::setUniform(const std::string &name, int value) {
	GLint location = getUniformLocation(name);
	glProgramUniform1i(id, location, value);
	return location != -1;
}

bool ShaderProgram::setUniform(const std::string &name, float value) {
	GLint location = getUniformLocation(name);
	glProgramUniform1f(id, location, value);
	return location != -1;
}

bool ShaderProgram::setUniform(const std::string &name, glm::vec2 vec) {
	GLint location = getUniformLocation(name);
	glProgramUniform2f(id, location, vec.x, vec.y);
	return location != -1;
}

bool ShaderProgram::setUniform(const std::string &name, glm::vec3 vec) {
	GLint location = getUniformLocation(name);
	glProgramUniform3f(id, location, vec.x, vec.y, vec.z);
	return location != -1;
}

bool ShaderProgram::setUniform(const std::string &name, glm::vec4 vec) {
	GLint location = getUniformLocation(name);
	glProgramUniform4f(id, location, vec.x, vec.y, vec.z, vec.w);
	return location != -1;
}

bool ShaderProgram::setUniform(const std::string &name, glm::mat3 mat) {
	GLint location = getUniformLocation(name);
	glProgramUniformMatrix3fv(id, location, 1, GL_FALSE, glm::value_ptr(mat));
	return location != -1;
}

bool ShaderProgram::setUniform(const std::string &name, glm::mat4 mat) {
	GLint location = getUniformLocation(name);
	glProgramUniformMatrix4fv(id, location, 1, GL_FALSE, glm::value_ptr(mat));
	return location != -1;
}

bool ShaderProgram::bindUniformBlockToBindingPoint(const std::string& uniformBlockName, const GLuint bindingPoint) const
{
    const auto blockIndex = glGetUniformBlockIndex(id, uniformBlockName.c_str());
    if (blockIndex == GL_INVALID_INDEX)
		return false;
    glUniformBlockBinding(id, blockIndex, bindingPoint);
	return true;
}


/// PRINT INFORMATIONS

static constexpr std::string_view printValue(GLint value) {
	/// @see https://registry.khronos.org/OpenGL/specs/gl/glspec46.core.pdf#section.7.14
	switch(value) {
	/// Result of GL_GEOMETRY_INPUT_TYPE, GL_GEOMETRY_OUT_TYPE or GL_TESS_GEN_MODE
	CASE_VALUE(GL_POINTS); // GL_GEOMETRY_INPUT_TYPE | GL_GEOMETRY_OUTPUT_TYPE
	CASE_VALUE(GL_LINES); // GL_GEOMETRY_INPUT_TYPE
	CASE_VALUE(GL_LINE_STRIP); // GL_GEOMETRY_OUTPUT_TYPE
	CASE_VALUE(GL_TRIANGLES); // GL_GEOMETRY_INPUT_TYPE | GL_TESS_GEN_MODE
	CASE_VALUE(GL_TRIANGLE_STRIP); // GL_GEOMETRY_OUTPUT_TYPE
	CASE_VALUE(GL_QUADS); // GL_TESS_GEN_MODE
	CASE_VALUE(GL_LINES_ADJACENCY); // GL_GEOMETRY_INPUT_TYPE
	CASE_VALUE(GL_TRIANGLES_ADJACENCY); // GL_GEOMETRY_INPUT_TYPE
	CASE_VALUE(GL_ISOLINES); // GL_TESS_GEN_MODE
	// Result of GL_TESS_GEN_SPACING
	CASE_VALUE(GL_EQUAL);
	CASE_VALUE(GL_FRACTIONAL_EVEN);
	CASE_VALUE(GL_FRACTIONAL_ODD);
	// Result of GL_TESS_GEN_VERTEX_ORDER
	CASE_VALUE(GL_CCW);
	CASE_VALUE(GL_CW);
	// Result of GL_TRANSFORM_FEEDBACK_BUFFER_MODE
	CASE_VALUE(GL_SEPARATE_ATTRIBS);
	CASE_VALUE(GL_INTERLEAVED_ATTRIBS);
	default: return "UNKNOWN VALUE";
	}
}

#undef CASE_VALUE

enum {
	BOOLEAN,
	FORMATTED,
	UNFORMATTED,
	VEC3
};

#define STR_KEY_VALUE(a) std::pair<GLenum, std::string_view>(a, #a)

void ShaderProgram::outputProperties(std::ostream& out) const
{
	auto printGLProgramParam = [this, &out](std::pair<GLenum, std::string_view> const& pname, int format) {
		GLint param[3];
		glGetProgramiv(this->id, pname.first, param);
		out << "\t" << pname.second << ": ";
		if(format == BOOLEAN)
			out << (param[0] ? "GL_TRUE" : "GL_FALSE");
		else if(format == FORMATTED)
			out << printValue(param[0]);
		else if(format == VEC3)
			out << "(" << param[0] << ", " << param[1] << ", " << param[2] << ")";
		else
			out << param[0];
		out << std::endl;
	};

	out << "Properties of " << std::endl;
	printGLProgramParam(STR_KEY_VALUE(GL_DELETE_STATUS), BOOLEAN);
	printGLProgramParam(STR_KEY_VALUE(GL_LINK_STATUS), BOOLEAN);
	printGLProgramParam(STR_KEY_VALUE(GL_VALIDATE_STATUS), BOOLEAN);
	printGLProgramParam(STR_KEY_VALUE(GL_INFO_LOG_LENGTH), UNFORMATTED);
	printGLProgramParam(STR_KEY_VALUE(GL_ATTACHED_SHADERS), UNFORMATTED);
	printGLProgramParam(STR_KEY_VALUE(GL_ACTIVE_ATTRIBUTES), UNFORMATTED);
	printGLProgramParam(STR_KEY_VALUE(GL_ACTIVE_ATTRIBUTE_MAX_LENGTH), UNFORMATTED);
	printGLProgramParam(STR_KEY_VALUE(GL_TRANSFORM_FEEDBACK_BUFFER_MODE), FORMATTED);
	printGLProgramParam(STR_KEY_VALUE(GL_TRANSFORM_FEEDBACK_VARYINGS), UNFORMATTED);
	printGLProgramParam(STR_KEY_VALUE(GL_TRANSFORM_FEEDBACK_VARYING_MAX_LENGTH), UNFORMATTED);
	printGLProgramParam(STR_KEY_VALUE(GL_ACTIVE_UNIFORM_BLOCKS), UNFORMATTED);
	printGLProgramParam(STR_KEY_VALUE(GL_ACTIVE_UNIFORM_BLOCK_MAX_NAME_LENGTH), UNFORMATTED);
	if(types & GL_GEOMETRY_SHADER_BIT) {
		printGLProgramParam(STR_KEY_VALUE(GL_GEOMETRY_VERTICES_OUT), UNFORMATTED);
		// POINTS, LINES, LINES_ADJACENCY, TRIANGLES or TRIANGLES_ADJACENCY
		printGLProgramParam(STR_KEY_VALUE(GL_GEOMETRY_INPUT_TYPE), FORMATTED);
		// POINTS, LINE_STRIP or TRIANGLE_STRIP
		printGLProgramParam(STR_KEY_VALUE(GL_GEOMETRY_OUTPUT_TYPE), FORMATTED);
		printGLProgramParam(STR_KEY_VALUE(GL_GEOMETRY_SHADER_INVOCATIONS), UNFORMATTED);
	}
	if(types & GL_TESS_CONTROL_SHADER_BIT) {
		printGLProgramParam(STR_KEY_VALUE(GL_TESS_CONTROL_OUTPUT_VERTICES), UNFORMATTED);
	}
	if(types & GL_TESS_EVALUATION_SHADER_BIT) {
		// QUADS, TRIANGLES, or ISOLINES
		printGLProgramParam(STR_KEY_VALUE(GL_TESS_GEN_MODE), FORMATTED);
		printGLProgramParam(STR_KEY_VALUE(GL_TESS_GEN_SPACING), FORMATTED);
		printGLProgramParam(STR_KEY_VALUE(GL_TESS_GEN_VERTEX_ORDER), FORMATTED);
		printGLProgramParam(STR_KEY_VALUE(GL_TESS_GEN_POINT_MODE), BOOLEAN);
	}
	printGLProgramParam(STR_KEY_VALUE(GL_PROGRAM_SEPARABLE), BOOLEAN);
	printGLProgramParam(STR_KEY_VALUE(GL_PROGRAM_BINARY_RETRIEVABLE_HINT), BOOLEAN);
	printGLProgramParam(STR_KEY_VALUE(GL_ACTIVE_ATOMIC_COUNTER_BUFFERS), UNFORMATTED);
}

#undef STR_KEY_VALUE

void ShaderProgram::outputInterface(std::ostream &out) const
{
	GLint nb_active, max_name_length;
	for (ProgramInterface const &interface : PROGRAM_INTERFACES)
	{
		glGetProgramInterfaceiv(id, interface.pname, GL_ACTIVE_RESOURCES, &nb_active);
		if(nb_active == 0)
			continue;
		out << "Number of ressources for interface " << interface.name << ": " << nb_active << std::endl;

		if (interface.pname != GL_ATOMIC_COUNTER_BUFFER && interface.pname != GL_TRANSFORM_FEEDBACK_BUFFER)
			glGetProgramInterfaceiv(id, interface.pname, GL_MAX_NAME_LENGTH, &max_name_length);
		char name[max_name_length];

		std::vector<GLenum> props;
		std::transform(
			interface.props.cbegin(),
			interface.props.cend(),
			std::back_insert_iterator(props),
			[](ProgramInterface::Prop const &p)
			{ return p.pname; });

		bool active_prop = std::find(props.cbegin(), props.cend(), GL_ACTIVE_VARIABLES) != props.cend();

		for (int i = 0; i < nb_active; i++)
		{
			glGetProgramResourceName(id, interface.pname, i, max_name_length, nullptr, name);
			out << "\t" << i << ": " << name << std::endl;

			std::size_t params_size{props.size()};
			if (active_prop)
			{
				const GLenum NUM_ACTIVE_VARIABLES_PROP = GL_NUM_ACTIVE_VARIABLES;
				const GLenum ACTIVE_VARIABLES = GL_ACTIVE_VARIABLES;
				GLint nb_active_var;
				glGetProgramResourceiv(id, interface.pname, i, 1, &NUM_ACTIVE_VARIABLES_PROP, sizeof(GLint), nullptr, &nb_active_var);
				GLsizei length;
				GLint active_vars[nb_active_var];
				glGetProgramResourceiv(id, interface.pname, i, 1, &ACTIVE_VARIABLES, nb_active_var * sizeof(GLint), &length, active_vars);
				out << "\t\tGL_ACTIVE_VARIABLES : ";
				for(int j = 0; j < nb_active_var; j++)
					out << active_vars[j] << ", ";
				out << std::endl;
			}

			GLsizei length;
			GLint params[params_size];
			glGetProgramResourceiv(id, interface.pname, i, props.size(), props.data(), params_size * sizeof(GLint), &length, params);
			
			for (std::size_t pindex{}; pindex < interface.props.size(); pindex++)
			{
				ProgramInterface::Prop const& p = interface.props[pindex];
				if(p.pname == GL_ACTIVE_VARIABLES || p.pname == GL_NAME_LENGTH)
					continue;
				if(params[pindex] == 0 && (
					p.pname == GL_REFERENCED_BY_COMPUTE_SHADER
				 || p.pname == GL_REFERENCED_BY_VERTEX_SHADER
				 || p.pname == GL_REFERENCED_BY_TESS_CONTROL_SHADER
				 || p.pname == GL_REFERENCED_BY_TESS_EVALUATION_SHADER
				 || p.pname == GL_REFERENCED_BY_GEOMETRY_SHADER
				 || p.pname == GL_REFERENCED_BY_FRAGMENT_SHADER))
					continue;
				if(p.pname == GL_ARRAY_SIZE && params[pindex] == 1)
					continue;
				if(p.pname == GL_ARRAY_STRIDE && params[pindex] == 0)
					continue;
				if(p.pname == GL_BLOCK_INDEX && params[pindex] == -1)
					continue;
				if(p.pname == GL_ATOMIC_COUNTER_BUFFER_INDEX && params[pindex] == -1)
					continue;
				if(p.pname == GL_TYPE)
					out << "\t\t" << p.name << ": " << printType(params[pindex]);
				else
					out << "\t\t" << p.name << ": " << params[pindex];
				out << std::endl;
			}
		}
	}
}

#define PRINT_GL_INTEGER_PARAM(pname) \
	do { \
		glGetIntegerv(pname, &params); \
		out << "\t" << #pname << ": " << params << std::endl; \
	} while(false)

void ShaderProgram::implementationLimits(std::ostream& out) {
	GLint params;
	// pname for querying default uniform block storage, in components
	out << "Default uniform block storage in components:" << std::endl;
	PRINT_GL_INTEGER_PARAM(GL_MAX_VERTEX_UNIFORM_COMPONENTS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_TESS_CONTROL_UNIFORM_COMPONENTS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_TESS_EVALUATION_UNIFORM_COMPONENTS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_GEOMETRY_UNIFORM_COMPONENTS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_FRAGMENT_UNIFORM_COMPONENTS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_COMPUTE_UNIFORM_COMPONENTS);
	// pname for querying combined uniform block storage, in components
	out << "Combined uniform block storage in components:" << std::endl;
	PRINT_GL_INTEGER_PARAM(GL_MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_COMBINED_TESS_CONTROL_UNIFORM_COMPONENTS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_COMBINED_TESS_EVALUATION_UNIFORM_COMPONENTS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS);
	// pname for querying uniform block storage, in blocks
	out << "Uniform block storage in blocks:" << std::endl;
	PRINT_GL_INTEGER_PARAM(GL_MAX_VERTEX_UNIFORM_BLOCKS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_TESS_CONTROL_UNIFORM_BLOCKS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_TESS_EVALUATION_UNIFORM_BLOCKS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_GEOMETRY_UNIFORM_BLOCKS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_FRAGMENT_UNIFORM_BLOCKS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_COMPUTE_UNIFORM_BLOCKS);
	out << "General limits of uniform blocks:" << std::endl;
	PRINT_GL_INTEGER_PARAM(GL_MAX_COMBINED_UNIFORM_BLOCKS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_UNIFORM_BLOCK_SIZE);
	PRINT_GL_INTEGER_PARAM(GL_MAX_UNIFORM_BUFFER_BINDINGS);
	out << "Limits of atomic counter buffers:" << std::endl;
	PRINT_GL_INTEGER_PARAM(GL_MAX_VERTEX_ATOMIC_COUNTER_BUFFERS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_TESS_CONTROL_ATOMIC_COUNTER_BUFFERS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_TESS_EVALUATION_ATOMIC_COUNTER_BUFFERS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_GEOMETRY_ATOMIC_COUNTER_BUFFERS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_FRAGMENT_ATOMIC_COUNTER_BUFFERS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS);
	out << "General limit for atomic counter buffers:" << std::endl;
	PRINT_GL_INTEGER_PARAM(GL_MAX_COMBINED_ATOMIC_COUNTER_BUFFERS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_ATOMIC_COUNTER_BUFFER_SIZE);
	PRINT_GL_INTEGER_PARAM(GL_MAX_ATOMIC_COUNTER_BUFFER_BINDINGS);
	out << "Limits of shader storage blocks:" << std::endl;
	PRINT_GL_INTEGER_PARAM(GL_MAX_VERTEX_SHADER_STORAGE_BLOCKS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_GEOMETRY_SHADER_STORAGE_BLOCKS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_FRAGMENT_SHADER_STORAGE_BLOCKS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_COMPUTE_SHADER_STORAGE_BLOCKS);
	out << "General limits for shader storage blocks:" << std::endl;
	PRINT_GL_INTEGER_PARAM(GL_MAX_COMBINED_SHADER_STORAGE_BLOCKS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_SHADER_STORAGE_BLOCK_SIZE);
	PRINT_GL_INTEGER_PARAM(GL_MAX_SHADER_STORAGE_BUFFER_BINDINGS);
	out << "General limit for subroutine uniforms:" << std::endl;
	PRINT_GL_INTEGER_PARAM(GL_MAX_SUBROUTINE_UNIFORM_LOCATIONS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_SUBROUTINES);
	out << "General limit for images:" << std::endl;
	PRINT_GL_INTEGER_PARAM(GL_MAX_IMAGE_UNITS);
	PRINT_GL_INTEGER_PARAM(GL_MAX_TEXTURE_UNITS);
}

#undef PRINT_GL_INTEGER_PARAM

}

