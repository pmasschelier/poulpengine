#include "Shaders/ProgramPipeline.hpp"

#include <iostream>

namespace plp {

ProgramPipeline::ProgramPipeline()
{
	glCreateProgramPipelines(1, &id);
}

ProgramPipeline::~ProgramPipeline()
{
	glDeleteProgramPipelines(1, &id);
}

bool ProgramPipeline::useProgram(std::shared_ptr<ShaderProgram> const& program)
{
	GLbitfield stages = program->types;
	return useProgramStages(program, stages);
}

void ProgramPipeline::foreachStage(GLbitfield stages, std::function<void(unsigned)> action)
{
	for(unsigned i = 0; i < m_stages.size(); i++) {
		if(stages & (1 << i))
			action(i);
	}
}

bool ProgramPipeline::useProgramStages(std::shared_ptr<ShaderProgram> const &program, GLbitfield stages)
{
	if(program) {
		[[unlikely]]
		if(!program->linkedSuccessfully()) {
			std::cerr << "Attempt to use a program which was not linked successfuly in a shader pipeline." << std::endl;
			return false;
		}
		[[unlikely]]
		if(!program->isSeparable()) {
			std::cerr << "Attempt to use a program which is not separable in a shader pipeline." << std::endl;
			return false;
		}

		foreachStage(stages, [this, program](unsigned i) { m_stages[i] = program; });
	}
	
	glUseProgramStages(id, stages, program ? program->getID() : 0);
	return true;
}

void ProgramPipeline::removeProgram(std::shared_ptr<ShaderProgram> const& program)
{
	GLbitfield stages{};
	for(unsigned i = 0; i < m_stages.size(); i++) {
		if(m_stages[i] == program) {
			m_stages[i] = nullptr;
			stages |= 1 << i;
		}
	}

	glUseProgramStages(id, stages, 0);
}

bool ProgramPipeline::setUniform(const std::string &name, bool value) {
	return setUniformForStage(GL_ALL_SHADER_BITS, name, value);
}

bool ProgramPipeline::setUniform(const std::string &name, int value) {
	return setUniformForStage(GL_ALL_SHADER_BITS, name, value);
}

bool ProgramPipeline::setUniform(const std::string &name, unsigned int value) {
	return setUniformForStage(GL_ALL_SHADER_BITS, name, value);
}

bool ProgramPipeline::setUniform(const std::string &name, float value) {
	return setUniformForStage(GL_ALL_SHADER_BITS, name, value);
}

bool ProgramPipeline::setUniform(const std::string &name, glm::vec2 vec) {
	return setUniformForStage(GL_ALL_SHADER_BITS, name, vec);
}

bool ProgramPipeline::setUniform(const std::string &name, glm::vec3 vec) {
	return setUniformForStage(GL_ALL_SHADER_BITS, name, vec);
}

bool ProgramPipeline::setUniform(const std::string &name, glm::vec4 vec) {
	return setUniformForStage(GL_ALL_SHADER_BITS, name, vec);
}

bool ProgramPipeline::setUniform(const std::string &name, glm::mat3 mat) {
	return setUniformForStage(GL_ALL_SHADER_BITS, name, mat);
}

bool ProgramPipeline::setUniform(const std::string &name, glm::mat4 mat) {
	return setUniformForStage(GL_ALL_SHADER_BITS, name, mat);
}


void ProgramPipeline::bind(std::function<void()> const &action, bool save) const
{
	GLint saved{};
	if(save)
		glGetIntegerv(GL_PROGRAM_PIPELINE_BINDING, &saved);
	glBindProgramPipeline(id);
	action();
	glBindProgramPipeline(saved);
}

} // namespace plp