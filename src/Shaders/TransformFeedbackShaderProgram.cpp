#include "Shaders/TransformFeedbackShaderProgram.hpp"

namespace plp
{

TransformFeedbackShaderProgram::TransformFeedbackShaderProgram(
	std::uint8_t types,
	PrimitiveType recordedPrimitive,
	std::vector<std::string> const& recordedAttributesNames
) :
	ShaderProgram(types),
	recordedPrimitive(recordedPrimitive)
{
	std::vector<const char*> recordedVarNamesPtrs;
	recordedVarNamesPtrs.reserve(recordedAttributesNames.size());
	for(auto& name: recordedAttributesNames)
		recordedVarNamesPtrs.push_back(name.c_str());

	glTransformFeedbackVaryings(getID(),
		static_cast<GLsizei>(recordedVarNamesPtrs.size()),
		recordedVarNamesPtrs.data(),
		GL_INTERLEAVED_ATTRIBS
	);
}

TransformFeedbackObject &TransformFeedbackShaderProgram::getTransformFeedBackObject()
{
	return tf;
}

void TransformFeedbackShaderProgram::setTransformFeedBackObject(std::shared_ptr<TransformFeedbackObject>& tf) {
	replacement_tf = tf;
}

void TransformFeedbackShaderProgram::bind(std::function<void()> const &action, bool save) const
{
	ShaderProgram::bind([this, &action, save]() {
		if(replacement_tf) {
			replacement_tf->record(recordedPrimitive, action, save);
			return;
		}
		tf.record(recordedPrimitive, action, save);
	}, save);
}

} // namespace plp
