#include "Shaders/ShaderBuilder.hpp"

#include <algorithm>
#include <iostream>

namespace plp
{

#include "Shaders/ShaderSources.inl"

constexpr std::tuple<ShaderType, std::string, std::string_view> getSource(CommonShaderSource src)
{
	switch (src)
	{
	case VERTEX_PASSTHROUGH_SOURCE:
		return std::make_tuple(VERTEX_SHADER, "Passthrough Vertex Shader", Sources::PASSTHROUGH_VERT);
	case VERTEX_POSITIONS_SOURCE:
		return std::make_tuple(VERTEX_SHADER, "Positions Vertex Shader", Sources::POSITION_VERT);
	case VERTEX_2D_SPRITE_SOURCE:
		return std::make_tuple(VERTEX_SHADER, "Simple Sprite Vertex Shader", Sources::SPRITE2D_VERT);
	case FRAGMENT_COLOR_TEXTURE_SOURCE:
		return std::make_tuple(FRAGMENT_SHADER, "Simple Texture Color Fragment Shader",
							   Sources::COLOR_TEXTURE_FRAG);
	case VERTEX_COLORS_SOURCE:
		return std::make_tuple(VERTEX_SHADER, "Simple Color Vertex Shader", Sources::COLORS_VERT);
	case FRAGMENT_COLORS_SOURCE:
		return std::make_tuple(FRAGMENT_SHADER, "Simple Color Fragment Shader", Sources::COLORS_FRAG);
	case VERTEX_TEXTURE_SOURCE:
		return std::make_tuple(VERTEX_SHADER, "Simple Texture Vertex Shader", Sources::TEXTURE_VERT);
	case VERTEX_TEXTURE_INSTANCED_SOURCE:
		return std::make_tuple(VERTEX_SHADER, "Simple Instanced Texture Vertex Shader",
							   Sources::TEXTURE_INSTANCED_VERT);
	case FRAGMENT_TEXTURE_SOURCE:
		return std::make_tuple(FRAGMENT_SHADER, "Simple Texture Fragment Shader", Sources::TEXTURE_FRAG);
	case VERTEX_CULLING_SOURCE:
		return std::make_tuple(VERTEX_SHADER, "Frustum Culling Vertex Shader", Sources::CULLING_VERT);
	case GEOMETRY_CULLING_SOURCE:
		return std::make_tuple(GEOMETRY_SHADER, "Frustum Culling Geometry Shader", Sources::CULLING_GEOM);
	case FRAGMENT_UNICOLOR_SOURCE:
		return std::make_tuple(FRAGMENT_SHADER, "Unicolor Fragment Shader", Sources::UNICOLOR_FRAG);
	case GEOMETRY_NORMALS_SOURCE:
		return std::make_tuple(GEOMETRY_SHADER, "Normals Geometry Shader", Sources::NORMALS_GEOM);
	case FRAGMENT_MATERIAL_SOURCE:
		return std::make_tuple(FRAGMENT_SHADER, "Simple Material Shader", Sources::MATERIALS_FRAG);
	case NUM_SHADER_SOURCES:
	default:
		std::unreachable();
	}
}

std::map<std::string, std::filesystem::path> ShaderBuilder::modulesLoaded;

static void bindCommonUniformBlocks(ShaderProgram &program)
{
	program.bindUniformBlockToBindingPoint("Camera", ShaderProgram::CAMERA);
	program.bindUniformBlockToBindingPoint("Lights", ShaderProgram::LIGHTS);
	program.bindUniformBlockToBindingPoint("Material", ShaderProgram::MATERIAL);
}

std::array<std::shared_ptr<ShaderObject>, NUM_SHADER_SOURCES> ShaderBuilder::common_shaders;
std::array<bool, NUM_MODULE_SOURCES> ShaderBuilder::common_modules;
std::array<std::shared_ptr<ShaderProgram>, NUM_PROGRAMS> ShaderBuilder::common_programs;
std::array<std::shared_ptr<TransformFeedbackShaderProgram>, NUM_TRANSFORMFEEDBACK_PROGRAMS>
	ShaderBuilder::common_tf_programs;

ShaderBuilder::ShaderBuilder()
{
	for (int name{}; name < NUM_MODULE_SOURCES; name++)
	{
		if (!common_modules[name])
			loadCommonModule((CommonModuleSource)name);
	}
}

std::shared_ptr<ShaderObject> ShaderBuilder::getCommonShader(CommonShaderSource num)
{
	if (!common_shaders.at(num))
	{
		const auto &[type, name, source] = getSource(num);
		auto shader = ShaderObject::createFromMemory(type, name, source);
		assert(shader);
		if (!shader)
		{
			throw std::runtime_error("A common shader source couldn't be loaded.");
		}
		common_shaders[num] = std::make_shared<ShaderObject>(std::move(shader.value()));
	}
	return common_shaders[num];
}

std::shared_ptr<ShaderProgram> ShaderBuilder::getCommonProgram(CommonProgram num)
{
	if (!common_programs[num])
	{
		ShaderBuilder builder;
		switch (num)
		{
		case BASIC_MODEL_PROGRAM:
			builder.addShader(getCommonShader(VERTEX_TEXTURE_SOURCE));
			builder.addShader(getCommonShader(FRAGMENT_TEXTURE_SOURCE));
			break;
		case INSTANCED_MODEL_PROGRAM:
			builder.addShader(getCommonShader(VERTEX_TEXTURE_INSTANCED_SOURCE));
			builder.addShader(getCommonShader(FRAGMENT_TEXTURE_SOURCE));
			break;
		default:
			std::unreachable();
		}
		common_programs[num] = std::make_shared<ShaderProgram>(std::move(builder.createProgram().value()));
	}
	return common_programs[num];
}

std::shared_ptr<TransformFeedbackShaderProgram> ShaderBuilder::getCommonTransformFeedbackProgram(
	CommonTransformFeedbackProgram num)
{
	if (!common_tf_programs[num])
	{
		ShaderBuilder builder;
		switch (num)
		{
		case CULLING_PROGRAM:
			builder.addShader(getCommonShader(VERTEX_CULLING_SOURCE));
			builder.addShader(getCommonShader(GEOMETRY_CULLING_SOURCE));
			break;
		default:
			std::unreachable();
		}
		common_tf_programs[num] = std::make_shared<TransformFeedbackShaderProgram>(
			std::move(builder.createTransformFeedbackProgram(POINTS, {"model", "inversedTransformedModel"}).value()));
	}

	return common_tf_programs[num];
}

Opt<ShaderProgram> ShaderBuilder::createStandaloneShaderFromFile(ShaderType type,
																 std::filesystem::path const &filename,
																 bool retrievable)
{
	auto program = std::shared_ptr<ShaderProgram>(new ShaderProgram(ShaderObject::getTypeBit(type)));
	auto source = readFile(filename);
	if (!source)
		return std::nullopt;

	const char *c_source = source.value().c_str();
	program->id = glCreateShaderProgramv(type, 1, &c_source);

	program->types = ShaderObject::getTypeBit(type);
	program->separable = true;
	program->retrievable = retrievable;

	bindCommonUniformBlocks(*program);

	if (!program->linkedSuccessfully())
	{
		program->outputInfoLog(std::cerr);
		return std::nullopt;
	}
	return program;
}

Opt<ShaderObject> ShaderBuilder::addFile(ShaderType shaderType, std::filesystem::path const &filename)
{
	auto shader = ShaderObject::createFromFile(shaderType, filename);
	if (!shader)
		return std::nullopt;
	auto shared_shader = std::make_shared<ShaderObject>(std::move(shader.value()));
	objects.push_back(shared_shader);
	types |= ShaderObject::getTypeBit(shaderType);
	return shared_shader;
}

Opt<ShaderObject> ShaderBuilder::addSource(ShaderType shaderType, std::string const &name,
										   std::string_view const &source)
{
	auto shader = ShaderObject::createFromMemory(shaderType, name, source);
	if (!shader)
		return std::nullopt;

	auto shared_shader = std::make_shared<ShaderObject>(std::move(shader.value()));
	objects.push_back(shared_shader);
	types |= ShaderObject::getTypeBit(shaderType);
	return shared_shader;
}

bool ShaderBuilder::addShader(std::shared_ptr<ShaderObject> const &shader)
{
	if (!shader->isCompiled())
		return false;
	objects.push_back(shader);
	types |= ShaderObject::getTypeBit(shader->getType());
	return true;
}

/* bool ShaderBuilder::linkProgram(ShaderProgram &program, bool separable, bool retrievable)
{
#ifndef NDEBUG
	std::cout << "Linking shader program n°" << program.id << "..." << std::endl;
#endif

	for (auto &shader : objects)
	{
#ifndef NDEBUG
		std::cout << "Attach shader object n°" << shader->id << "..." << std::endl;
#endif
		glAttachShader(program.id, shader->id);
	}

	// can the program be bound for individual pipeline stages using UseProgramStages ?
	if (separable)
		glProgramParameteri(program.id, GL_PROGRAM_SEPARABLE, GL_TRUE);
	// can the program binary be retrieved later ?
	if (retrievable)
		glProgramParameteri(program.id, GL_PROGRAM_BINARY_RETRIEVABLE_HINT, GL_TRUE);

	glLinkProgram(program.id);

	if (!program.linkedSuccessfully())
	{
#ifndef NDEBUG
		std::cout << "Error while linking." << std::endl;
#endif
		program.outputInfoLog(std::cerr);
		return false;
	}

	program.separable = separable;
	program.retrievable = retrievable;
	program.types = types;

	for (auto &shader : objects)
		glDetachShader(program.id, shader->id);
	return true;
} */

bool ShaderBuilder::create(ShaderProgram& program, bool separable, bool retrievable)
{
	for(auto& shader: objects)
		program.attach(*shader);
	if(!program.link(separable, retrievable)) {
		clear();
		return false;
	}
	for(auto& shader: objects)
		program.detach(*shader);
	clear();
	return true;
}

std::optional<ShaderProgram> ShaderBuilder::createProgram(bool separable, bool retrievable)
{
	ShaderProgram program(types);

	if (!create(program, separable, retrievable))
		return std::nullopt;

	bindCommonUniformBlocks(program);
	return program;
}

std::optional<TransformFeedbackShaderProgram> ShaderBuilder::createTransformFeedbackProgram(
	PrimitiveType recordedPrimitive, std::vector<std::string> const &recordedAttributesNames, bool separable,
	bool retrievable)
{
	TransformFeedbackShaderProgram program(types, recordedPrimitive, recordedAttributesNames);

	if (!create(program, separable, retrievable))
		return std::nullopt;

	bindCommonUniformBlocks(program);

	return program;
}

std::optional<ComputeShader> ShaderBuilder::createComputeShaderFromFile(std::filesystem::path const &filename, bool retrievable)
{
	auto shader = ComputeShaderObject::createFromFile(filename);
	if(!shader)
		return std::nullopt;
	ComputeShader program;
	program.attach(shader.value());
	if(!program.link(retrievable)) {
		std::cerr << "Failed to link compute shader : " << filename << std::endl;
		clear();
		return std::nullopt;
	}
	program.detach(shader.value());
	clear();
	
	return program;
}

void ShaderBuilder::clear()
{
	objects.clear();
	types = 0;
}

static bool checkARBShadingLanguageInclude()
{
	// https://registry.khronos.org/OpenGL/extensions/ARB/ARB_shading_language_include.txt
	if (!GLEW_ARB_shading_language_include)
		std::cerr << "Error: Your OpenGL implementation doesn't support the ARB_shading_language_include required by "
					 "ShaderBuilder::loadModule."
				  << std::endl;
	return GLEW_ARB_shading_language_include;
}

constexpr std::pair<std::string, std::string_view> getModule(CommonModuleSource source)
{
	switch (source)
	{
	case FRUSTUM_GLSL_SOURCE:
		return std::make_pair("/common/frustum.glsl", Sources::FRUSTUM_GLSL);
	case CAMERA_GLSL_SOURCE:
		return std::make_pair("/common/camera.glsl", Sources::CAMERA_GLSL);
	case BOUNDINGBOX_GLSL_SOURCE:
		return std::make_pair("/common/boundingbox.glsl", Sources::BOUNDINGBOX_GLSL);
	case RANDOM_GLSL_SOURCE:
		return std::make_pair("/common/random.glsl", Sources::RANDOM_GLSL);
	case LIGHT_CASTERS_GLSL_SOURCE:
		return std::make_pair("/common/light_casters.glsl", Sources::LIGHT_CASTERS_GLSL);
	case FOG_GLSL_SOURCE:
		return std::make_pair("/common/fog.glsl", Sources::FOG_GLSL);
	case GLOBAL_TRANSFORMS_GLSL_SOURCE:
		return std::make_pair("/common/global_transforms.glsl", Sources::GLOBAL_TRANSFORMS_GLSL);
	default:
		std::unreachable();
	}
}

bool ShaderBuilder::loadCommonModule(CommonModuleSource source)
{
	auto shaderInfos = getModule(source);
	if (!checkARBShadingLanguageInclude())
	{
		std::cerr << "Error: Unable to load common module: " << shaderInfos.first << std::endl;
		return false;
	}

	if (common_modules[source])
	{
		std::cout << shaderInfos.first << " was already loaded, ignoring..." << std::endl;
		return true;
	}

	std::cout << "Loading module " << shaderInfos.first << "..." << std::endl;
	glNamedStringARB(GL_SHADER_INCLUDE_ARB, shaderInfos.first.size(), shaderInfos.first.data(),
					 shaderInfos.second.size(), shaderInfos.second.data());
	common_modules[source] = true;
	return true;
}

bool ShaderBuilder::loadModule(std::string const &virtual_filename, std::filesystem::path const &path)
{
	if (!checkARBShadingLanguageInclude())
	{
		std::cerr << "Error: Unable to load module: " << virtual_filename << std::endl;
		return false;
	}
	if (virtual_filename.starts_with("/common"))
	{
		std::cerr << "Error: No user module can be loaded to folder /common" << std::endl;
		return false;
	}

	auto [it, success] = modulesLoaded.emplace(virtual_filename, path);
	if (!success)
	{
		if (it->second == path)
		{
			std::cout << path << " was already loaded with name " << virtual_filename << ", ignoring..." << std::endl;
			return true;
		}
		std::cerr << "A module named " << virtual_filename << "already exists (file: " << it->second << ")."
				  << std::endl;
		return false;
	}

	auto content = readFile(path);
	if (!content)
	{
		std::cerr << "Unable to load module " << path << std::endl;
		modulesLoaded.erase(it);
		return false;
	}

	std::cout << "Loading module " << virtual_filename << "..." << std::endl;
	glNamedStringARB(GL_SHADER_INCLUDE_ARB, virtual_filename.size(), virtual_filename.data(), content.value().size(),
					 content.value().data());
	return true;
}

} // namespace plp
