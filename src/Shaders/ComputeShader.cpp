#include "Shaders/ComputeShader.hpp"

namespace plp
{

ComputeShader::ComputeShader() : ShaderProgram(GL_COMPUTE_SHADER_BIT)
{}

void ComputeShader::execute(glm::uvec3 numWorkGroups)
{
	bind([&numWorkGroups]() {
		glDispatchCompute(numWorkGroups.x, numWorkGroups.y, numWorkGroups.z);
	});
}

glm::uvec3 ComputeShader::getWorkGroupSize()
{
	if(!cachedWorkGroupSize)
		glGetProgramiv(id, GL_COMPUTE_WORK_GROUP_SIZE, &workGroupSize.x);
	cachedWorkGroupSize = true;
	return workGroupSize;
}

glm::uvec3 ComputeShader::getMaxComputeWorkGroupCount()
{
	static glm::ivec3 value(0.f);
	if(value == glm::ivec3(0.f)) {
		glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &value.x);
		glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &value.x);
		glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &value.x);
	}
	return value;
}
glm::uvec3 ComputeShader::getMaxComputeWorkGroupSize()
{
	static glm::ivec3 value(0.f);
	if(value == glm::ivec3(0.f)) {
		glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0, &value.x);
		glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 1, &value.x);
		glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 2, &value.x);
	}
	return value;
}
unsigned ComputeShader::getMaxComputeWorkGroupInvocations()
{
	static int value = 0;
	if(value == 0)
		glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, &value); 
	return value;
}
} // namespace plp