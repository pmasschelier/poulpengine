#include "Shaders/ShaderObject.hpp"
#include "Utils.hpp"

#include <algorithm>
#include <iostream>
#include <fstream>
#include <map>
#include <utility>
#include <vector>

namespace plp
{

constexpr std::string_view printShaderType(ShaderType type) {
	switch(type) {
	CASE_VALUE(VERTEX_SHADER);
	CASE_VALUE(TESS_CONTROL_SHADER);
	CASE_VALUE(TESS_EVALUATION_SHADER);
	CASE_VALUE(GEOMETRY_SHADER);
	CASE_VALUE(FRAGMENT_SHADER);
	CASE_VALUE(COMPUTE_SHADER);
	default: return "UNKNOWN";
	}
};

std::uint8_t AbstractShaderObject::getTypeBit(ShaderType type)
{
	switch (type)
	{
	case VERTEX_SHADER:
		return GL_VERTEX_SHADER_BIT;
	case GEOMETRY_SHADER:
		return GL_GEOMETRY_SHADER_BIT;
	case TESS_CONTROL_SHADER:
		return GL_TESS_CONTROL_SHADER_BIT;
	case TESS_EVALUATION_SHADER:
		return GL_TESS_EVALUATION_SHADER_BIT;
	case FRAGMENT_SHADER:
		return GL_FRAGMENT_SHADER_BIT;
	case COMPUTE_SHADER:
		return GL_COMPUTE_SHADER_BIT;
	}
	std::unreachable();
}

AbstractShaderObject::AbstractShaderObject(ShaderType type) : type(type)
{
	id = glCreateShader(type);
#ifndef NDEBUG
	std::cout << "Shader object n°" << id << " created." << std::endl;
#endif
}

AbstractShaderObject::~AbstractShaderObject()
{
	if(glIsShader(id)) {
		glDeleteShader(id);
#ifndef NDEBUG
		std::cout << "Shader object n°" << id << " deleted." << std::endl;
#endif
	}
}

ShaderType AbstractShaderObject::getType() const
{
	return type;
}

void AbstractShaderObject::outputInfoLog(std::ostream& out) const {
	int InfoLogLength;
	glGetShaderiv(id, GL_INFO_LOG_LENGTH, &InfoLogLength);

	std::vector<char> shaderErrorMessage(InfoLogLength+1);
	glGetShaderInfoLog(id, InfoLogLength, NULL, &shaderErrorMessage[0]);
	out << &shaderErrorMessage[0] << std::endl;
}

bool AbstractShaderObject::compile(std::string const& name, std::string_view const& source)
{
	filename = name;

	const char *sourcePointer = source.data(); // Variable nécessaire parce qu'on ne peut pas récupérer l'adresse d'une lvalue
	glShaderSource(id, 1, &sourcePointer, NULL); // shader id, count, string, length

#ifndef NDEBUG
	std::cout << "Compiling shader object n°" << id << "(" << name << ")" << std::endl;
#endif
	if (GLEW_ARB_shading_language_include)
		glCompileShaderIncludeARB(id, 0, nullptr, nullptr);
	else
		glCompileShader(id);

	// Vérifie le vertex shader
	GLint result = isCompiled();

	if (!result)
	{
		outputInfoLog(std::cerr);
		return false;
	}
	return true;
}

bool AbstractShaderObject::isCompiled() const
{
    GLint compiled = GL_FALSE;
	glGetShaderiv(id, GL_COMPILE_STATUS, &compiled);
	return compiled;
}

static void printGLShaderParam(GLuint id, std::ostream& out, std::pair<GLenum, std::string_view> const& pname, GLenum type) {
	GLint param;
	glGetShaderiv(id, pname.first, &param);
	out << "\t" << pname.second << ": ";
	if(type == GL_BOOL)
		out << (param ? "GL_TRUE" : "GL_FALSE");
	else if(type == GL_SHADER_TYPE)
		out << printShaderType(static_cast<ShaderType>(param));
	else
		out << param;
	out << std::endl;
}


#define STR_KEY_VALUE(a) std::pair<GLenum, std::string_view>(a, #a)

void AbstractShaderObject::outputProperties(std::ostream& out) const
{
	out << "Properties of " << filename << std::endl;
	printGLShaderParam(id, out, STR_KEY_VALUE(GL_SHADER_TYPE), GL_SHADER_TYPE);
	printGLShaderParam(id, out, STR_KEY_VALUE(GL_DELETE_STATUS), GL_BOOL);
	printGLShaderParam(id, out, STR_KEY_VALUE(GL_COMPILE_STATUS), GL_BOOL);
	printGLShaderParam(id, out, STR_KEY_VALUE(GL_INFO_LOG_LENGTH), GL_INT);
	printGLShaderParam(id, out, STR_KEY_VALUE(GL_SHADER_SOURCE_LENGTH), GL_INT);
	printGLShaderParam(id, out, STR_KEY_VALUE(GL_SPIR_V_BINARY), GL_BOOL);
}


std::optional<ShaderObject> ShaderObject::createFromFile(ShaderType type, std::filesystem::path const &path)
{
	auto content = readFile(path);
	if(!content)
		return std::nullopt;
	return createFromMemory(type, path, content.value());
}

std::optional<ShaderObject> ShaderObject::createFromMemory(ShaderType type, std::string const &name, std::string_view const &source)
{
	if(type == COMPUTE_SHADER)
		return std::nullopt;
	ShaderObject shader(type);
	if(!shader.compile(name, source))
		return std::nullopt;
	return shader;
}

std::optional<ComputeShaderObject> ComputeShaderObject::createFromFile(std::filesystem::path const &path)
{
	auto content = readFile(path);
	if(!content)
		return std::nullopt;
	return createFromMemory(path, content.value());
}

std::optional<ComputeShaderObject> ComputeShaderObject::createFromMemory(std::string const &name, std::string_view const &source)
{
	ComputeShaderObject shader;
	if(!shader.compile(name, source))
		return std::nullopt;
	return shader;
}

#undef STR_KEY_VALUE

} // namespace plp