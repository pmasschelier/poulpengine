#include "Camera.hpp"
#include "Shaders/ShaderProgram.hpp"

namespace plp {

using namespace std::numbers;

Camera::Camera(glm::vec3 const& pos, float horizontalAngle, float verticalAngle, float ratio, float FoV, float speed, float mouseSpeed, float nearClipping, float farClipping) :
    m_direction(cos(verticalAngle) * sin(horizontalAngle), sin(verticalAngle), cos(verticalAngle) * cos(horizontalAngle)),
    m_right(sin(horizontalAngle - 3.14f/2.0f), 0, cos(horizontalAngle - 3.14f/2.0f)),
    m_up(glm::cross(m_right, m_direction)),
    m_horizontalAngle(horizontalAngle),
    m_verticalAngle(verticalAngle),
    m_FOV(FoV),
    m_speed(speed),
    m_mouseSpeed(mouseSpeed),
    m_ratio(ratio),
    m_nearClipping(nearClipping),
    m_farClipping(farClipping),
	mappedUBO(ubo.map())
{
	position = pos;
	projection = glm::perspective(glm::radians(m_FOV), m_ratio, m_nearClipping, m_farClipping);
	view = glm::lookAt(position, position + m_direction, m_up);
	projectionView = projection * view;

	mappedUBO->position = position;
	mappedUBO->projection = projection;
	mappedUBO->view = view;
	mappedUBO->projectionView = projectionView;

	auto [hbb, vbb] = computeBillboardingVectors();
	mappedUBO->billboardHorizontalVector = hbb;
	mappedUBO->billboardVerticalVector = vbb;

	activate();
}

glm::mat4 Camera::updateProjection()
{
    mappedUBO->projection = projection = glm::perspective(glm::radians(m_FOV), m_ratio, m_nearClipping, m_farClipping);
    mappedUBO->projectionView = projectionView = projection * view;
    return projectionView;
}

glm::mat4 Camera::updateView()
{
    mappedUBO->view = view = glm::lookAt(position, position+m_direction, glm::vec3(0.f, 1.f, 0.f));
    mappedUBO->projectionView = projectionView = projection * view;
	mappedUBO->inversedTransposedView = glm::transpose(glm::inverse(view));

	auto [hbb, vbb] = computeBillboardingVectors();
	mappedUBO->billboardHorizontalVector = hbb;
	mappedUBO->billboardVerticalVector = vbb;
	mappedUBO->frustum = computeFrustum();
	
    return projectionView;
}

void Camera::updateDirectionRightUp()
{
    m_direction = glm::vec3(
		cos(m_verticalAngle) * sin(m_horizontalAngle),
		sin(m_verticalAngle),
		cos(m_verticalAngle) * cos(m_horizontalAngle)
	);
    
    m_right = glm::vec3(
        sin(m_horizontalAngle - pi/2.0f),
        0,
        cos(m_horizontalAngle - pi/2.0f)
    );

    m_up = glm::cross( m_right, m_direction );
}

CameraUniformData::Frustum Camera::computeFrustum() const
{
    const float halfVSide = m_farClipping * glm::tan(glm::radians(m_FOV) * .5f);
    const float halfHSide = halfVSide * m_ratio;
    const glm::vec3 frontMultFar = m_farClipping * m_direction;

	const auto computePlane = [](glm::vec3 point, glm::vec3 normal) {
		normal = glm::normalize(normal);
		return glm::vec4(normal, - glm::dot(normal, point));
	};

	CameraUniformData::Frustum frustum;

    frustum.nearPlane = computePlane(position + m_nearClipping * m_direction, m_direction);
    frustum.farPlane = computePlane(position + frontMultFar, -m_direction);
    frustum.leftPlane = computePlane(position, glm::cross(frontMultFar - m_right * halfHSide, m_up));
    frustum.rightPlane = computePlane(position, glm::cross(m_up,frontMultFar + m_right * halfHSide));
    frustum.bottomPlane = computePlane(position, glm::cross(m_right, frontMultFar - m_up * halfVSide));
    frustum.topPlane = computePlane(position, glm::cross(frontMultFar + m_up * halfVSide, m_right));

	return frustum;
}

std::pair<glm::vec3, glm::vec3> Camera::computeBillboardingVectors() const {
	const auto hvec = glm::normalize(glm::cross(m_direction, m_up));
	const auto vvec = glm::normalize(glm::cross(m_direction, -hvec));
	return {hvec, vvec};
}

void Camera::copyFrustum(Camera const &other)
{
	mappedUBO->frustum = other.computeFrustum();
}

glm::mat4 Camera::setPosition(glm::vec3 const& pos) {
    mappedUBO->position = position = pos;
    return updateView();
}

glm::vec3 Camera::getPosition() const {
    return position;
}

glm::vec3 Camera::getDirection() const {
	return m_direction;
}

glm::quat Camera::getRotation() const {
	glm::quat q1(glm::angleAxis(m_verticalAngle, glm::vec3(-1.f, 0.f, 0.f)));
	glm::quat q2(glm::angleAxis(m_horizontalAngle, glm::vec3(0.f, 1.f, 0.f)));
	return q2 * q1;
}

glm::mat4 Camera::move(glm::vec3 const& displacement) {
    return setPosition(position + displacement);
}

glm::mat4 Camera::move(Movement movement, float elaspedTimeSec) {
	glm::vec3 displacement{};
	if(movement & LEFT)		displacement -= m_right 	* m_speed * elaspedTimeSec;
	if(movement & RIGHT) 	displacement += m_right 	* m_speed * elaspedTimeSec;
	if(movement & UP) 		displacement += m_up 		* m_speed * elaspedTimeSec;
	if(movement & DOWN) 	displacement -= m_up 		* m_speed * elaspedTimeSec;
	if(movement & FORWARD) 	displacement += m_direction * m_speed * elaspedTimeSec;
	if(movement & BACKWARD) displacement -= m_direction * m_speed * elaspedTimeSec;
	return move(displacement);
}

glm::mat4 Camera::moveView(glm::vec2 const& euler)
{
	m_horizontalAngle -= euler.x * m_mouseSpeed;
	m_verticalAngle   += euler.y * m_mouseSpeed;

	const float lim_angle = 19./40.*pi;
	if(m_verticalAngle < -lim_angle) m_verticalAngle = -lim_angle;
	if(m_verticalAngle > lim_angle) m_verticalAngle = lim_angle;

	updateDirectionRightUp();
	return updateView();
}

glm::mat4 Camera::setTarget(glm::vec3 const& target) {
    m_direction = target - position;
    float norm = glm::length(m_direction);
    m_verticalAngle = asin(m_direction.y/norm);
	float norm2 = glm::length(glm::vec2(m_direction.x, m_direction.z));
    m_horizontalAngle = acos(m_direction.z/norm2);
    if(m_direction.x < 0)
        m_horizontalAngle = -m_horizontalAngle;
    updateDirectionRightUp();
    return updateView();
}

glm::mat4 Camera::setFov(float FoV) {
    m_FOV = FoV;
    return updateProjection();
}

float Camera::getFov() const {
    return m_FOV;
}

void Camera::setSpeed(float speed) {
    m_speed = speed;
}

float Camera::getSpeed() const {
    return m_speed;
}

void Camera::setMouseSpeed(float mouseSpeed) {
    m_mouseSpeed = mouseSpeed;
}

float Camera::getMouseSpeed() const {
    return m_mouseSpeed;
}

glm::mat4 Camera::setRatio(float ratio) {
    m_ratio = ratio;
    return updateProjection();
}

float Camera::getRatio() const {
    return m_ratio;
}

glm::mat4 Camera::setNearClipping(float nearClipping) {
    m_nearClipping = nearClipping;
    return updateProjection();
}

float Camera::getNearClipping() const {
    return m_nearClipping;
}

glm::mat4 Camera::setFarClipping(float farClipping) {
    m_farClipping = farClipping;
    return updateProjection();
}

float Camera::getFarClipping() const
{
    return m_farClipping;
}

glm::mat4 Camera::getViewMatrix() const {
    return view;
}

glm::mat4 Camera::getProjectionMatrix() const {
    return projection;
}

glm::mat4 Camera::getProjectionViewMatrix() const {
    return projectionView;
}

void Camera::activate() const {
	ubo.bind<UNIFORM_BUFFER>(ShaderProgram::CAMERA);
}

}

