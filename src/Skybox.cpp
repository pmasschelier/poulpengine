#include "Skybox.hpp"
#include "Textures/TextureBinder.hpp"
#include "Shaders/ShaderBuilder.hpp"

namespace plp {

std::shared_ptr<ShaderProgram> shader;

static const std::string vertexShader = R"(
#version 460 core

layout (location = 0) in vec3 aPos;

layout (std140) uniform Camera
{
	mat4 projection;
	mat4 view;
	mat4 projectionView;
	vec3 position;
};

out gl_PerVertex
{
	vec4 gl_Position;
	float gl_PointSize;
	float gl_ClipDistance[];
};

out vec3 texCoords;

void main()
{
    vec4 MVPpos = projection * mat4(mat3(view)) * vec4(aPos, 1.0);
    gl_Position = MVPpos.xyww; // On fait z=1 ainsi la skybox perd tous les tests de profondeur
    texCoords = aPos;
};
)";

static const std::string fragmentShader = R"(
#version 460 core

in vec3 texCoords;

out vec4 FragColor;

uniform samplerCube cubeTexture;

void main()
{
    FragColor = texture(cubeTexture, texCoords);
};
)";

Skybox::Skybox()
{
    if(!shader) {
		ShaderBuilder builder;
		builder.addSource(VERTEX_SHADER, "Default Skybox Vertex Shader", vertexShader);
		builder.addSource(FRAGMENT_SHADER, "Default Skybox Fragment Shader", fragmentShader);
		shader = std::make_shared<ShaderProgram>(std::move(builder.createProgram().value()));
    }

	Cube::createNudeCube();
}

void Skybox::setCubemapTexture(std::shared_ptr<CubemapTexture> cubemapTex)
{
    m_cubemapTex = cubemapTex;
}

bool Skybox::loadFromFiles(std::array<std::filesystem::path, 6> const& filenames)
{
    std::optional<CubemapTexture> result = m_cubemapTex->createFromFiles(filenames);
    if(!result)
		return false;
	m_cubemapTex = std::make_shared<CubemapTexture>(std::move(result.value()));
	m_cubemapTex->setSmooth(true);
	return true;
}

void Skybox::render() const
{
    glDepthFunc(GL_LEQUAL);
    glCullFace(GL_FRONT);

	TextureBinder texture_binder(*m_cubemapTex);

	shader->bind([]() { Cube::createNudeCube()->draw(); });
	
    glCullFace(GL_BACK);
    glDepthFunc(GL_LESS);
}

}

