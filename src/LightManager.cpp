#include "LightManager.hpp"
#include "Shaders/ShaderProgram.hpp"

#include <iostream>

namespace plp
{

LightManager::LightManager() :
	mappedUBO(ubo.map())
{
	ubo.bind<UNIFORM_BUFFER>(ShaderProgram::LIGHTS);
}


bool LightManager::addLight(Light const& light)
{
	std::size_t size = lights.size();
	if(size >= MAX_LIGHT_NUMBER)
		return false;
	
	std::cout << "Add light : type = " << light.type << ", constant = " << light.constant << ", linear = " << light.linear << ", quadratic = " << light.quadratic << std::endl;

	lights.push_back(light);
	mappedUBO->lights[size] = light;
	mappedUBO->nb_lights = size + 1;
	mappedUBO->active = 1;
	
	return true;
}

void LightManager::setActive(bool active)
{
	mappedUBO->active = active;
}

} // namespace plp
