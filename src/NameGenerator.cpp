#include "NameGenerator.hpp"

#include <cassert>
#include <format>
#include <functional>

#include <iostream>

namespace plp
{

namespace
{

constexpr char suffix = '#';

std::string remove_suffix(std::string const& name) {
	std::size_t pos = name.find_first_of(suffix);
	return name.substr(0, pos);
}

bool has_suffix(std::string const& name) {
	std::size_t pos = name.find_first_of(suffix);
	return pos != std::string::npos;
}
	
} // namespace

std::vector<std::string> NameGenerator::getUsedNames() const
{
  std::vector<std::string> keys;
  for(auto& [key, _]: names)
    keys.push_back(key);
  return keys;
}

std::uint32_t NameGenerator::getMaxSuffix(std::string const &name) const
{
	auto it = names.find(name);
	if(it == names.end() || !it->second)
		return 0;
	return it->second->max_suffix;
}

bool NameGenerator::addName(std::string const &name)
{
	auto [it, occured] = names.try_emplace(remove_suffix(name), std::make_shared<NameInfos>());
	if(occured)
		return true;
	bool ret = !it->second->root_used;
	it->second->root_used = true;
	return ret;
}

std::string NameGenerator::addNameWithSuffix(std::string const &name)
{
	// std::cout << remove_suffix(name) << "is being suffixed" << std::endl;
	return addNameWithSuffix_helper(remove_suffix(name), std::make_shared<NameInfos>());
}

bool NameGenerator::free_name(std::string const &name)
{
	auto it = names.find(name);
	if(it == names.end())
		return false;
	if(!has_suffix(name))
		it->second->root_used = false;
	else {
		if(it->second.use_count() > 1) {
			if(it->second.use_count() == 2 && !it->second->root_used)
				names.erase(remove_suffix(name));
			it->second->free_alt.push_back(name);
		}
		names.erase(it);
	}
	return true;
}

std::string NameGenerator::addNameWithSuffix_helper(std::string const &name, std::shared_ptr<NameInfos> infos)
{
	auto [it, occured] = names.try_emplace(name, infos);
	if(occured)
		return name;
	assert(it->second);
	if(!it->second->root_used) {
		it->second->root_used = true;
		return name;
	}
	
	std::deque<std::string>& alts = it->second->free_alt;
	if(!alts.empty()) {
		std::string alt = alts.front();
		alts.pop_front();
		return addNameWithSuffix_helper(alt, it->second);
	}
	return addNameWithSuffix_helper(std::format("{}#{}", name, ++it->second->max_suffix), it->second);
}

} // namespace plp
