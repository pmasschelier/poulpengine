#include "TransformBuffer.hpp"

#include "ModelBuilder.hpp"
#include "StaticBufferStorage.hpp"

#include <cstdint>
#include <memory>

namespace plp
{

TransformBuffer::TransformBuffer() :
	models(INITIAL_NODE_CAPACITY),
	commands(INITIAL_NODE_CAPACITY),
	instances(INITIAL_NODE_CAPACITY)
{
	// createNewBuffers();
	
	/* const std::vector<Attribute> attributes = {
		{ Attribute::FLOAT, Attribute::VEC4, Attribute::MODEL},
		{ Attribute::FLOAT, Attribute::VEC4, Attribute::MODEL + 1},
		{ Attribute::FLOAT, Attribute::VEC4, Attribute::MODEL + 2},
		{ Attribute::FLOAT, Attribute::VEC4, Attribute::MODEL + 3},
		{ Attribute::FLOAT, Attribute::VEC3, Attribute::INVERSED_TRANSPOSED_MODEL},
		{ Attribute::FLOAT, Attribute::VEC3, Attribute::INVERSED_TRANSPOSED_MODEL + 1},
		{ Attribute::FLOAT, Attribute::VEC3, Attribute::INVERSED_TRANSPOSED_MODEL + 2},
	};

	ModelBuilder builder(nodeCapacity, POINTS);
	builder.addBuffer(globalTransforms, attributes);
	builder.create();

	cullingModel = std::make_unique<TransformFeedbackModel>(builder.create(), ShaderBuilder::getCommonTransformFeedbackProgram(CULLING_PROGRAM)); */
}

std::uint32_t TransformBuffer::addCommand(
	Command const& command,
	bool caster,
	bool receiver
)
{
	std::uint32_t commandIndex = commands.create(1);
	auto& command_ref = commands.map()[commandIndex];
	command_ref = command;

	std::uint32_t count = std::visit([](auto&& cmd) {
		return cmd.instanceCount;
	}, command.command);
	std::uint32_t transformIndex = addTransform(count);

	std::uint32_t baseInstance = instances.create(1);
	auto& instanceBuffer = instances.map();
	for(int i = 0; i < count; i++) {
		auto& instanceRef = instanceBuffer[baseInstance + i];
		instanceRef.transformIndex = transformIndex;
		instanceRef.commandIndex = commandIndex;
		instanceRef.status = (Instance::CASTER * caster) | (Instance::RECEIVER * receiver);
	}
	return transformIndex;
}

std::uint32_t TransformBuffer::addTransform(std::uint32_t count)
{
	std::uint32_t first = models.create(count);
	shouldUpdateTransform.resize(models.getCapacity(), true);
	return first;
}

void TransformBuffer::notifyUpdate(std::uint32_t transformId) {
	shouldUpdateTransform[transformId] = true;
}

bool TransformBuffer::updateTransform(SceneNode& node) {
	if(!shouldUpdateTransform[node.getTransformId()])
		return false;
	
	auto &mappedBuffer = models.map();
	mappedBuffer[node.getTransformId()].model = node.getGlobalTransformMatrix();
	mappedBuffer[node.getTransformId()].inversedTransposedModel = node.getInversedTransposedGlobalTransformMatrix();

	shouldUpdateTransform[node.getTransformId()] = false;

	return true;
}

void TransformBuffer::bind(std::uint32_t baseInstance, std::uint32_t instanceCount) const
{
	const std::size_t size = sizeof(GlobalTransformData);
	models.bind<SHADER_STORAGE_BUFFER>(ShaderProgram::GLOBAL_TRANSFORMS, baseInstance * size, instanceCount * size);
}

void TransformBuffer::computeCulling()
{
	models.bind<SHADER_STORAGE_BUFFER>(ShaderProgram::GLOBAL_TRANSFORMS);
	commands.bind<SHADER_STORAGE_BUFFER>(ShaderProgram::COMMANDS);
	instances.bind<SHADER_STORAGE_BUFFER>(ShaderProgram::INSTANCES);
}

void TransformBuffer::cullTransforms(
	std::shared_ptr<StaticBufferStorage>& culledTransforms,
	StaticBufferStorage& commandBuffer,
	GLintptr commandOffset,
	glm::vec3 center,
	glm::vec3 extent,
	std::uint32_t baseInstance,
	std::uint32_t instanceCount)
{
	GLuint vao;
	glCreateVertexArrays(1, &vao);
	bind(baseInstance, instanceCount);

	auto program = ShaderBuilder::getCommonTransformFeedbackProgram(CULLING_PROGRAM);
	program->setUniform("aabbCenter", center);
	program->setUniform("aabbExtent", extent);
	program->getTransformFeedBackObject().setBuffer(culledTransforms, 0);
	glBindVertexArray(vao);

	commandBuffer.bind<QUERY_BUFFER>();

	QueryObject<TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN> query;
	query.query(commandBuffer, commandOffset, [this, instanceCount, &program]() {
		program->bind([instanceCount]() {
			glDrawArraysInstanced(POINTS, 0, 1, instanceCount);
		});
	});
}

} // namespace plp
