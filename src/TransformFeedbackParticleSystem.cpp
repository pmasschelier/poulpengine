// STL
#include <algorithm>
#include <iostream>
#include <random>

// GLM
#include <glm/gtc/matrix_transform.hpp>

// Project
#include "TransformFeedbackParticleSystem.hpp"
#include "Textures/TextureBinder.hpp"
#include "Enabler.hpp"

namespace plp
{

TransformFeedbackParticleSystem::TransformFeedbackParticleSystem()
{
}

void TransformFeedbackParticleSystem::setGeneratedAmount(int numParticlesToGenerate, float generateEverySeconds)
{
	this->numParticlesToGenerate = numParticlesToGenerate;
	this->generateEverySeconds = generateEverySeconds;
}

void TransformFeedbackParticleSystem::swapBuffers()
{
	std::swap(buffers.first, buffers.second);
	std::swap(renderVAO.first, renderVAO.second);
	std::swap(updateVAO.first, updateVAO.second);
}

bool TransformFeedbackParticleSystem::init(TransformFeedbackParticleSystemInitialParameters params)
{
	maxParticleCount = params.maxParticleCount;
	texture = params.texture;

	std::size_t offset{};
	offset_map.clear();
	for (Param &p : params.params)
	{
		auto attrib = p.toAttribute();
		std::size_t baseAlignement = attrib.alignOf();
		std::size_t dist = offset % baseAlignement;
		if (dist == 0)
			p.offset = offset;
		else
			p.offset = offset + (baseAlignement - dist);
		auto [_, succeeded] = offset_map.insert(std::make_pair(p.name, p.offset));
		if (!succeeded)
		{
			std::cerr << "Error: the name " << p.name << " was used twice for a parameter of the particle system." << std::endl;
			return false;
		}
		offset = p.offset + attrib.getByteSize();
		std::cout << p.name << " align = " << baseAlignement << " offset = " << p.offset << " end = " << offset << std::endl;
	}

	params_ubo = std::make_unique<MappedBufferStorage<>>(offset);
	params_ubo->map();

	ShaderBuilder builder;

	for (auto &object : params.updateShaderPaths)
		builder.addFile(object.first, object.second);

	std::vector<RecordedAttribute> recordedAttributes = {
		{Attribute::INT, Attribute::SCALAR, 0, "outType", false},
		{Attribute::FLOAT, Attribute::VEC3, 1, "outPosition", true}};
	std::copy(params.recordedVariables.begin(), params.recordedVariables.end(), std::back_insert_iterator(recordedAttributes));

	std::vector<std::string> recordedNames;
	for (auto &var : recordedAttributes)
	{
		updateAttribute.push_back(var);
		recordedNames.push_back(var.name);
		if (var.neededForRendering)
			renderAttribute.push_back(var);
		else
			renderAttribute.emplace_back(var.type, var.width, Attribute::DUMMY);
	}

	// If the initialization of shaders and recorded variables fails, don't continue
	auto update = builder.createTransformFeedbackProgram(POINTS, recordedNames);
	if (!update)
	{
		std::cerr << "Error: The UPDATE shader program of the particle system couldn't be loaded." << std::endl;
		return false;
	}
	updateShader = std::make_shared<TransformFeedbackShaderProgram>(std::move(update.value()));
	updateShader->bindUniformBlockToBindingPoint("Parameters", ShaderProgram::PARTICLE_SYSTEM_PARAMETERS);

	for (auto &object : params.renderShaderPaths)
		builder.addFile(object.first, object.second);

	auto render = builder.createProgram();
	if (!render)
	{
		std::cerr << "Error: The RENDER shader program of the particle system couldn't be loaded." << std::endl;
		return false;
	}
	renderShader = std::make_shared<ShaderProgram>(std::move(render.value()));

	// Now that all went through just fine, let's generate all necessary OpenGL objects
	generateOpenGLObjects();
	status = INITIALIZED;

	return true;
}

void TransformFeedbackParticleSystem::render(glm::mat4 modelMatrix, glm::mat3 inversedTransposedModelMatrix, const std::uint32_t queries, Renderable::QueryData * data) const
{
	// Can't render if system is not initialized
	if (status == UNINITIALIZED || status == INITIALIZED)
		return;

	TextureBinder texture_binder(*texture);

	Enabler enabler(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glDepthMask(GL_FALSE);
	renderShader->setUniform("model", modelMatrix);

	renderShader->bind([this]() {
		renderVAO.first.draw(updateShader->getTransformFeedBackObject()); 
	});

	glDepthMask(GL_TRUE);
}

void TransformFeedbackParticleSystem::updateParticles(const float elapsedTime)
{
	// Can't render if system is not initialized
	if (status == UNINITIALIZED)
		return;

	updateShader->setUniform("elapsedTime", elapsedTime);

	// Update remaining time to generate particles and then decide how many particles will be generated
	remainingTimeToGenerateSeconds -= elapsedTime;
	if (remainingTimeToGenerateSeconds <= 0.0f)
	{
		updateShader->setUniform("numParticlesToGenerate", numParticlesToGenerate);
		updateShader->setUniform("initialRandomGeneratorSeed", generateRandomNumberGeneratorSeed());
		remainingTimeToGenerateSeconds += generateEverySeconds;
	}
	else
	{
		updateShader->setUniform("numParticlesToGenerate", 0u);
	}

	updateShader->getTransformFeedBackObject().setBuffer(buffers.second);

	// Update particles with special update shader program and also observe how many particles have been written
	// Discard rasterization - we don't want to render this, it's only about updating
	Enabler enabler(GL_RASTERIZER_DISCARD);

	params_ubo->bind<UNIFORM_BUFFER>(ShaderProgram::PARTICLE_SYSTEM_PARAMETERS);
	updateShader->bind([this]() {
		if(status == INITIALIZED)
			updateVAO.first.draw();
		else
			updateVAO.first.draw(updateShader->getTransformFeedBackObject());
	});

	// Swap read / write buffers for the next frame
	swapBuffers();
	status = UPDATED;
}

std::size_t TransformFeedbackParticleSystem::getMaxParticleCount() const
{
	return maxParticleCount;
}

std::size_t TransformFeedbackParticleSystem::getNbParticles() const
{
	return nbParticles;
}

glm::vec3 TransformFeedbackParticleSystem::generateRandomNumberGeneratorSeed()
{
	static std::random_device rd;
	static std::mt19937 generator(rd());
	std::uniform_real_distribution<float> seedDistribution(-10.0f, 10.0f);

	return glm::vec3(seedDistribution(generator), seedDistribution(generator), seedDistribution(generator));
}

void TransformFeedbackParticleSystem::generateOpenGLObjects()
{
	// Gather some constants
	const auto particleByteSize = Attribute::computeStride(updateAttribute.begin(), updateAttribute.end());
	GLsizeiptr bufferByteSize = particleByteSize * maxParticleCount;
	const int32_t initialGeneratorType = PARTICLE_TYPE_GENERATOR;

	buffers.first = std::make_shared<StaticBufferStorage>(bufferByteSize, std::span(&initialGeneratorType, 1));
	buffers.second = std::make_shared<StaticBufferStorage>(bufferByteSize);
	
	renderVAO.first = ModelBuilder::create(1, { buffers.first, renderAttribute }, nullptr, POINTS).value();
	renderVAO.second = ModelBuilder::create(0, { buffers.second, renderAttribute }, nullptr, POINTS).value();

	updateVAO.first = ModelBuilder::create(1, { buffers.first, updateAttribute }, nullptr, POINTS).value();
	updateVAO.second = ModelBuilder::create(0, { buffers.second, updateAttribute }, nullptr, POINTS).value();
}

} // namespace plp