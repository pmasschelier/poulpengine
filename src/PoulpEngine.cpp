#include "Utils.hpp"

#include <glm/glm.hpp>

#include <format>
#include <fstream>
#include <iostream>
#if defined (__GNUG__) && !defined (__clang__)
#include <stacktrace>
#endif
#include "Framebuffer.hpp"
#include "Textures/Texture2D.hpp"

namespace plp
{


static void message_callback(
	GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	[[maybe_unused]] GLsizei length,
	GLchar const* message,
	[[maybe_unused]] void const* user_param
)
{
	auto const src_str = [source]() {
		switch (source)
		{
		case GL_DEBUG_SOURCE_API: return "API";
		case GL_DEBUG_SOURCE_WINDOW_SYSTEM: return "WINDOW SYSTEM";
		case GL_DEBUG_SOURCE_SHADER_COMPILER: return "SHADER COMPILER";
		case GL_DEBUG_SOURCE_THIRD_PARTY: return "THIRD PARTY";
		case GL_DEBUG_SOURCE_APPLICATION: return "APPLICATION";
		case GL_DEBUG_SOURCE_OTHER: return "OTHER";
		default: return "UNKNOWN";
		}
	}();

	auto const type_str = [type]() {
		switch (type)
		{
		case GL_DEBUG_TYPE_ERROR: return "ERROR";
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: return "DEPRECATED_BEHAVIOR";
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: return "UNDEFINED_BEHAVIOR";
		case GL_DEBUG_TYPE_PORTABILITY: return "PORTABILITY";
		case GL_DEBUG_TYPE_PERFORMANCE: return "PERFORMANCE";
		case GL_DEBUG_TYPE_MARKER: return "MARKER";
		case GL_DEBUG_TYPE_OTHER: return "OTHER";
		default: return "UNKNOWN";
		}
	}();

	auto const severity_str = [severity]() {
		switch (severity) {
		case GL_DEBUG_SEVERITY_NOTIFICATION: return "NOTIFICATION";
		case GL_DEBUG_SEVERITY_LOW: return "LOW";
		case GL_DEBUG_SEVERITY_MEDIUM: return "MEDIUM";
		case GL_DEBUG_SEVERITY_HIGH: return "HIGH";
		default: return "UNKNOWN";
		}
	}();

#if defined (__GNUG__) && !defined (__clang__)
	std::cout << std::stacktrace::current() << std::endl;
#endif
	std::cout << src_str << ", " << type_str << ", " << severity_str << ", " << id << ": " << message << std::endl;
}

bool init() {
	int majorVersion{};
	int minorVersion{};
    glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
    glGetIntegerv(GL_MINOR_VERSION, &minorVersion);

	if(majorVersion < 4 && minorVersion < 6) {
		std::cerr << "An OpenGL context should be activated with minimum version 4.6" << std::endl;
		std::cerr << "Your current OpenGL context version is " << majorVersion << "." << minorVersion << std::endl;
		return false;
	}

#ifndef NDEBUG
	std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;
	std::cout << "GLSL version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
	std::cout << "GL implementation vendor: " << glGetString(GL_VENDOR) << std::endl;
	std::cout << "GL hardware renderer: " << glGetString(GL_RENDERER) << std::endl;
#endif

	if (glewInit() != GLEW_OK) {
		std::cerr << "glew initialisation failed" << std::endl;
        return EXIT_FAILURE;
    }

	// Accepte le fragment s'il est plus proche de la caméra que l'ancien
	glDepthFunc(GL_LESS);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_MULTISAMPLE);

	/* glEnable(GL_DEBUG_OUTPUT);
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageCallback(message_callback, nullptr); */
	
	return true;
}

void clear(glm::vec4 color) {
	glClearColor(color.r, color.g, color.b, color.a);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void setViewPort(glm::uvec4 const& viewport) {
	glViewport(viewport.x, viewport.y, viewport.z, viewport.w);
}

void setWireframeMode(bool activate) {
	if(activate)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

bool screenshot(glm::uvec2 wsize, Image& screen) {
	std::optional<Texture2D> tex = Texture2D::createFromFramebuffer(Framebuffer(), wsize);
	if(!tex)
		return false;
	screen = tex.value().copyToImage();
	return true;
}

} // namespace plp


