#include "SceneManager.hpp"

#include "ModelLoader.hpp"
#include "Textures/Texture2D.hpp"

#include <chrono>
#include <cstdint>
#include <optional>

using namespace std::chrono_literals;

namespace plp
{
SceneManager::SceneManager()
	: // localTransforms(nodeCapacity * sizeof(TransformData)),
	  rootNode(SceneNode::create("root", 0, transforms))
{
}

Opt<Texture> SceneManager::getTexture(std::string const &name) const
{
	auto tex = textures.find(name);
	if (tex == textures.end())
		return std::nullopt;
	return tex->second;
}

Opt<SceneNode> SceneManager::loadSceneFromFile(std::filesystem::path const &path)
{
	return ModelLoader::loadSceneFromFile(*this, path);
}

Opt<Texture2D> SceneManager::loadTextureFromFile(std::string const &name, std::filesystem::path const &filename)
{
	auto [it, occured] = textures.emplace(name, nullptr);
	if (!occured)
	{
		std::cerr << "A texture with the name " << name << "already exists" << std::endl;
		return std::nullopt;
	}
	std::optional<Texture2D> texture = Texture2D::createFromFile(filename);
	if (!texture)
	{
		std::cerr << "Error while loading: " << filename << std::endl;
		goto fail;
	}
	it->second = std::make_shared<Texture2D>(std::move(texture.value()));
	return it->second;
fail:
	textures.erase(it);
	return std::nullopt;
}

bool SceneManager::loadSkyboxFromFiles(std::array<std::filesystem::path, 6> const &filenames)
{
	skybox = std::make_unique<Skybox>();
	return skybox->loadFromFiles(filenames);
}

Opt<Model> SceneManager::addModel(std::string name, Model &&model, bool suffix)
{
	if (suffix)
		name = meshNameDomain.addNameWithSuffix(name);
	else if (!meshNameDomain.addName(name))
	{
		std::cerr << "The name " << name << " is already used by another node" << std::endl;
		return std::nullopt;
	}
	std::cout << "ADD MESH TO SCENE : " << name << std::endl;
	auto [it, occured] = models.try_emplace(name, std::make_shared<Model>(std::forward<Model>(model)));
	assert(occured);
	return it->second.model;
}

Opt<ShaderProgram> SceneManager::addShaderProgram(std::string const &name, ShaderProgram &&program)
{
	return addShaderProgram(name, std::make_shared<ShaderProgram>(std::forward<ShaderProgram>(program)));
}

Opt<ShaderProgram> SceneManager::addShaderProgram(std::string const &name, std::shared_ptr<ShaderProgram> const &program)
{
	auto [it, occured] = programs.emplace(name, program);
	if (occured)
		return it->second;
	return std::nullopt;
}

bool SceneManager::addLight(LightManager::Light const &light)
{
	if (!light_manager)
		light_manager = std::make_unique<LightManager>();
	return light_manager->addLight(light);
}

std::shared_ptr<SceneNode> SceneManager::createSceneNode(std::string const &nameHint, std::shared_ptr<SceneNode> parent)
{
	std::string name = nodeNameDomain.addNameWithSuffix(nameHint);
	std::uint32_t tranformId = transforms.addTransform(1);
	SceneNodeInfos infos = emplaceSceneNode(name, tranformId, parent);
	return infos.node;
}

std::shared_ptr<SceneNode> SceneManager::createSceneNode(std::string const &name, std::shared_ptr<Model> model, std::shared_ptr<SceneNode> parent)
{
	auto node = createSceneNode(name, parent);
	node->addModel(model);
	return node;
}

std::optional<std::pair<std::shared_ptr<SceneNode>, std::shared_ptr<Model>>> SceneManager::createSceneNode(
	std::string const &name, std::shared_ptr<RawModel> const &raw, std::shared_ptr<ShaderProgram> const &shader, std::shared_ptr<SceneNode> parent
)
{
	auto model = std::make_shared<Model>(raw, shader);
	std::shared_ptr<SceneNode> node = createSceneNode(name, model, parent);
	return std::make_pair(node, model);
}

std::shared_ptr<SceneNode> SceneManager::createSceneNode(std::string const &name, std::shared_ptr<TransformFeedbackParticleSystem> const &system, std::shared_ptr<SceneNode> parent)
{
	std::shared_ptr<SceneNode> node = createSceneNode(name, parent);
	node->addParticleSystem(system);
	return node;
}

std::optional<Collection<SceneNode>> SceneManager::createInstancedSceneNodes(
	std::string const &nameHint, std::shared_ptr<Model> model, std::shared_ptr<SceneNode> parent
)
{
	Collection<SceneNode> nodes;
	std::uint16_t base = transforms.addTransform(model->getInstanceCount());
	std::cout << "Created: transform : " << base << " -> " << base + model->getInstanceCount() << std::endl;
	for (unsigned i = 0; i < model->getInstanceCount(); i++)
	{
		std::string name = nodeNameDomain.addNameWithSuffix(nameHint);
		SceneNodeInfos infos = emplaceSceneNode(name, base + i, parent);
		// infos.node->addModel(model, i);
		nodes.push_back(infos.node);
	}
	instanced_models.push_back({
		model,
		base,
		model->getInstanceCount(),
		std::make_shared<StaticBufferStorage>(model->getInstanceCount() * sizeof(GlobalTransformData))
	});
	return nodes;
}

std::shared_ptr<SceneNode> SceneManager::duplicateSceneNode(std::shared_ptr<SceneNode> const &node)
{
	return duplicateSceneNode(node, node->getParent().lock());
}

std::shared_ptr<SceneNode> SceneManager::duplicateSceneNode(std::shared_ptr<SceneNode> const &node, std::shared_ptr<SceneNode> parent)
{
	auto copy = createSceneNode(node->getName(), parent);
	copy->copyLocalTransform(*node);
	copy->copyObjects(*node);

	for (auto const &child : node->getChildren())
		auto copy_child = duplicateSceneNode(child.lock(), copy);
	return copy;
}

Opt<SceneNode> SceneManager::duplicateSceneNode(std::string const &name)
{
	auto it = nodes.find(name);
	if (it == nodes.end())
	{
		std::cerr << "There is no node with name " << name << std::endl;
		return std::nullopt;
	}
	auto copy = duplicateSceneNode(it->second.node);
	return copy;
}

void SceneManager::render(bool frustumCulling)
{
	for (auto const &[_, infos] : nodes)
	{
		if (infos.shouldRender)
			transforms.updateTransform(*infos.node);
	}
	for (auto const &[name, infos] : nodes)
	{
		if (infos.shouldRender)
			infos.node->renderMeshes();
	}
	for (auto &instanced : instanced_models)
	{
		transforms.bind(instanced.baseInstance, instanced.instanceCount);
		if(!frustumCulling) {
			instanced.model->render();
			continue;
		}

		DrawIndirectCommand command = instanced.model->createIndirectRenderingCommand();
		StaticBufferStorage command_buffer(std::as_bytes(std::span(&command, 1)));
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
		transforms.cullTransforms(
			instanced.transforms,
			command_buffer,
			offsetof(DrawElementsIndirectCommand, instanceCount),
			instanced.model->getRawModel().getCenterAABB(), 
			instanced.model->getRawModel().getExtentAABB(),
			instanced.baseInstance,
			instanced.instanceCount
		);

		const std::size_t size = sizeof(GlobalTransformData);
		glMemoryBarrier(GL_QUERY_BUFFER_BARRIER_BIT | GL_TRANSFORM_FEEDBACK_BARRIER_BIT);
		instanced.transforms->bind<SHADER_STORAGE_BUFFER>(ShaderProgram::GLOBAL_TRANSFORMS, instanced.baseInstance * size, instanced.instanceCount * size);
		instanced.model->render(command_buffer);
	}
	if (skybox)
		skybox->render();
	for (auto const &[_, infos] : nodes)
	{
		if (infos.shouldRender)
			infos.node->renderParticleSystems();
	}
}

SceneManager::SceneNodeInfos SceneManager::emplaceSceneNode(std::string name, std::uint32_t transformId, std::shared_ptr<SceneNode> parent)
{
	auto [it, occured] = nodes.try_emplace(name, SceneNode::create(name, transformId, transforms));
	assert(occured);
	if(!parent)
		parent = rootNode;
	parent->addChild(it->second.node);
	return it->second;
}

bool SceneManager::enableRender(std::string const &name, bool enable)
{
	auto it = nodes.find(name);
	if (it == nodes.end())
		return false;
	it->second.shouldRender = enable;
	return true;
}

} // namespace plp
