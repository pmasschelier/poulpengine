#include "SceneNode.hpp"
#include "TransformBuffer.hpp"

#include <algorithm>

namespace plp
{

std::shared_ptr<SceneNode> SceneNode::create(std::string const &name, std::uint32_t transformId, TransformBuffer& buffer)
{
	return std::shared_ptr<SceneNode>(new SceneNode(name, transformId, buffer));
}

std::string SceneNode::getName() const
{
	return name;
}

std::uint32_t SceneNode::getTransformId() const
{
	return transformId;
}

std::weak_ptr<SceneNode> SceneNode::getParent() const
{
	return parent;
}

bool SceneNode::addChild(std::weak_ptr<SceneNode> const &child)
{
	std::shared_ptr<SceneNode> shared_child = child.lock();
	if (!shared_child)
		return false;
	std::shared_ptr<SceneNode> current = shared_from_this();

	while (!current->getParent().expired())
	{
		current = getParent().lock();
		if (current == shared_child)
		{
			std::cerr << "Error: Cyclic scene node insertion detected." << std::endl;
			return false;
		}
	}
	if (auto old_parent = shared_child->parent.lock(); old_parent)
		old_parent->removeChild(shared_child);
	shared_child->notifyGlobalUpdate();
	shared_child->parent = weak_from_this();
	children.push_back(child);
	return true;
}

bool SceneNode::removeChild(std::shared_ptr<SceneNode> const &child)
{
	return 0 != std::erase_if(children, [&child](std::weak_ptr<SceneNode> &other) {
		auto sh_other = other.lock();
		return sh_other && sh_other == child;
	});
}

std::size_t SceneNode::childrenCount() const
{
	return children.size();
}

void SceneNode::addModel(std::shared_ptr<Model> renderable, std::uint32_t queries, Renderable::QueryData *data)
{
	objects.push_back({renderable, queries, data});
}

/* void SceneNode::addModel(std::shared_ptr<InstancedModel> instancedModel, unsigned int instanceId)
{
	instancedModels.push_back({instancedModel, instanceId});
} */

std::size_t SceneNode::modelCount() const
{
	return objects.size();
}

void SceneNode::addParticleSystem(std::shared_ptr<TransformFeedbackParticleSystem> system)
{
	systems.push_back(system);
}

void SceneNode::setTransformations(glm::vec3 const &position, glm::quat const &rotation, glm::vec3 scaleFactor)
{
	this->position = position;
	this->rotation = rotation;
	this->scaleFactor = scaleFactor;
	notifyLocalUpdate();
}

void SceneNode::setPosition(glm::vec3 const &position)
{
	this->position = position;
	notifyLocalUpdate();
}

void SceneNode::move(glm::vec3 const &displacement)
{
	this->position += displacement;
	notifyLocalUpdate();
}

glm::vec3 SceneNode::getPosition() const
{
	return position;
}

void SceneNode::setRotation(glm::quat const &rotation)
{
	this->rotation = rotation;
	notifyLocalUpdate();
}

void SceneNode::rotate(glm::quat const &rotation)
{
	this->rotation = rotation * this->rotation;
	notifyLocalUpdate();
}

glm::quat SceneNode::getRotation() const
{
	return rotation;
}

void SceneNode::setScale(glm::vec3 const &scaleFactor)
{
	this->scaleFactor = scaleFactor;
	notifyLocalUpdate();
}

void SceneNode::scale(glm::vec3 const &scaleFactor)
{
	this->scaleFactor *= scaleFactor;
	notifyLocalUpdate();
}

glm::vec3 SceneNode::getScale() const
{
	return scaleFactor;
}

bool SceneNode::localTransformUpdated() const
{
	return localTransformNeedsUpdate;
}

bool SceneNode::globalTransformUpdated() const
{
	return globalTransformNeedsUpdate;
}

glm::mat4 SceneNode::getLocalTransformMatrix()
{
	if (!localTransformNeedsUpdate)
		return localTransform;

	const glm::mat4 mat_rotation = glm::mat4_cast(rotation);
	glm::mat4 scale_and_translate(
		glm::vec4(scaleFactor.x, 0.f, 0.f, 0.f), glm::vec4(0.f, scaleFactor.y, 0.f, 0.f), glm::vec4(0.f, 0.f, scaleFactor.z, 0.f),
		glm::vec4(position, 1.f)
	);

	localTransform = scale_and_translate * mat_rotation;
	localTransformNeedsUpdate = false;
	return localTransform;
}

glm::mat4 SceneNode::getGlobalTransformMatrix()
{
	if (!globalTransformNeedsUpdate)
		return globalTransform;
	if (std::shared_ptr<SceneNode> shared_parent = parent.lock(); shared_parent)
		globalTransform = shared_parent->getGlobalTransformMatrix() * getLocalTransformMatrix();
	else
		globalTransform = getLocalTransformMatrix();
	globalTransformNeedsUpdate = false;
	return globalTransform;
}

glm::mat3 SceneNode::getInversedTransposedLocalTransformMatrix()
{
	if (inversedTransposedLocalTransformNeedsUpdate)
		inversedTransposedLocalTransform = glm::transpose(glm::inverse(glm::mat3(getLocalTransformMatrix())));
	inversedTransposedLocalTransformNeedsUpdate = false;
	return inversedTransposedLocalTransform;
}

glm::mat3 SceneNode::getInversedTransposedGlobalTransformMatrix()
{
	if (inversedTransposedGlobalTransformNeedsUpdate)
		inversedTransposedGlobalTransform = glm::transpose(glm::inverse(glm::mat3(getGlobalTransformMatrix())));
	inversedTransposedGlobalTransformNeedsUpdate = false;
	return inversedTransposedGlobalTransform;
}

void SceneNode::copyLocalTransform(SceneNode const &other)
{
	position = other.position;
	rotation = other.rotation;
	scaleFactor = other.scaleFactor;
	notifyLocalUpdate();
}

void SceneNode::copyObjects(SceneNode const &other)
{
	objects = other.objects;
	systems = other.systems;
}

void SceneNode::renderMeshes()
{
	glm::mat4 model = getGlobalTransformMatrix();
	glm::mat3 inversedTranformedModel = getInversedTransposedGlobalTransformMatrix();
	for (auto &obj : objects)
		obj.renderable->render(model, inversedTranformedModel, obj.queries, obj.data);
}

void SceneNode::renderParticleSystems()
{
	for (auto &system : systems)
		system->render(getGlobalTransformMatrix(), getInversedTransposedGlobalTransformMatrix());
}

std::vector<std::weak_ptr<SceneNode>> SceneNode::getChildren() const
{
	return children;
}

void SceneNode::notifyLocalUpdate()
{
	localTransformNeedsUpdate = true;
	inversedTransposedLocalTransformNeedsUpdate = true;
	notifyGlobalUpdate();
}

void SceneNode::notifyGlobalUpdate()
{
	globalTransformNeedsUpdate = true;
	inversedTransposedGlobalTransformNeedsUpdate = true;

	buffer.notifyUpdate(transformId);

	for (auto const &child : children)
		if (std::shared_ptr<SceneNode> sharedChild = child.lock(); sharedChild)
			sharedChild->notifyGlobalUpdate();
}

} // namespace plp
