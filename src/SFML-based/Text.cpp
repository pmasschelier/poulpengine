////////////////////////////////////////////////////////////
//
// SFML - Simple and Fast Multimedia Library
// Copyright (C) 2007-2023 Laurent Gomila (laurent@sfml-dev.org)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////

/* Replace this:
 * #include <SFML/Graphics/Font.hpp>
 * #include <SFML/Graphics/RenderTarget.hpp>
 * #include <SFML/Graphics/Text.hpp>
 * #include <SFML/Graphics/Texture.hpp>
 * With this: */
#include "SFML-based/Font.hpp"
#include "SFML-based/Text.hpp"
#include "Shaders/ShaderBuilder.hpp"
#include "Textures/TextureBinder.hpp"
#include "Textures/Texture2D.hpp"
////////////////////////////////////////////////////////////

#include <glm/gtc/matrix_transform.hpp>

#include <algorithm>
#include <span>
#include <utility>


namespace
{
// Add an underline or strikethrough line to the vertex array
void addLine(std::vector<glm::vec4>& vertices,
             float            lineLength,
             float            lineTop,
             float            offset,
             float            thickness,
             float            outlineThickness = 0)
{
    const float top    = std::floor(lineTop + offset - (thickness / 2) + 0.5f);
    const float bottom = top + std::floor(thickness + 0.5f);

    vertices.emplace_back(-outlineThickness, top - outlineThickness, 1.f, 1.f);
    vertices.emplace_back(lineLength + outlineThickness, top - outlineThickness, 1.f, 1.f);
    vertices.emplace_back(-outlineThickness, bottom + outlineThickness, 1.f, 1.f);
    vertices.emplace_back(-outlineThickness, bottom + outlineThickness, 1.f, 1.f);
    vertices.emplace_back(lineLength + outlineThickness, top - outlineThickness, 1.f, 1.f);
    vertices.emplace_back(lineLength + outlineThickness, bottom + outlineThickness, 1.f, 1.f);
}

// Add a glyph quad to the vertex array
void addGlyphQuad(std::vector<glm::vec4>& vertices, glm::uvec2 fontPageSize, Vector2f position, const plp::Glyph& glyph, float italicShear)
{
	const float padding = 1.0;

	const float left   = glyph.bounds.left - padding;
	const float top    = glyph.bounds.top - padding;
	const float right  = glyph.bounds.left + glyph.bounds.width + padding;
	const float bottom = glyph.bounds.top + glyph.bounds.height + padding;

	const float u1 = (static_cast<float>(glyph.textureRect.left) - padding) / fontPageSize.x;
	const float v1 = (static_cast<float>(glyph.textureRect.top) - padding) / fontPageSize.y;
	const float u2 = (static_cast<float>(glyph.textureRect.left + glyph.textureRect.width) + padding) / fontPageSize.x;
	const float v2 = (static_cast<float>(glyph.textureRect.top + glyph.textureRect.height) + padding) / fontPageSize.y;

	vertices.emplace_back(position.x + left - italicShear * top, position.y + top, u1, v1);
	vertices.emplace_back(position.x + right - italicShear * top, position.y + top, u2, v1);
	vertices.emplace_back(position.x + left - italicShear * bottom, position.y + bottom, u1, v2);
	vertices.emplace_back(position.x + left - italicShear * bottom, position.y + bottom, u1, v2);
	vertices.emplace_back(position.x + right - italicShear * top, position.y + top, u2, v1);
	vertices.emplace_back(position.x + right - italicShear * bottom, position.y + bottom, u2, v2);
}
} // namespace


namespace plp
{

std::shared_ptr<ShaderProgram> shader;

////////////////////////////////////////////////////////////
Text::Text(const Font& font, std::string string, unsigned int characterSize) :
m_string(std::move(string)),
m_font(&font),
m_characterSize(characterSize)
{
	{
		ModelBuilder builder(0);
		builder.addBuffer(m_buffer, {
			{ Attribute::FLOAT, Attribute::VEC2, Attribute::POSITIONS },
			{ Attribute::FLOAT, Attribute::VEC2, Attribute::UVS }
		});
		m_rawmodel = builder.create().value();
	}

	{
		ShaderBuilder builder;
		if(!shader) {
			builder.addShader(ShaderBuilder::getCommonShader(VERTEX_2D_SPRITE_SOURCE));
			builder.addShader(ShaderBuilder::getCommonShader(FRAGMENT_COLOR_TEXTURE_SOURCE));
			shader = std::make_shared<ShaderProgram>(builder.createProgram().value());
		}
	}
}


////////////////////////////////////////////////////////////
// Text::Text(const Text&) = default;


////////////////////////////////////////////////////////////
// Text& Text::operator=(const Text&) = default;


////////////////////////////////////////////////////////////
Text::Text(Text&&) noexcept = default;


////////////////////////////////////////////////////////////
Text& Text::operator=(Text&&) noexcept = default;


////////////////////////////////////////////////////////////
void Text::setString(const std::string& string)
{
    if (m_string != string)
    {
        m_string             = string;
        m_geometryNeedUpdate = true;
    }
}


////////////////////////////////////////////////////////////
void Text::setFont(const Font& font)
{
    if (m_font != &font)
    {
        m_font               = &font;
        m_geometryNeedUpdate = true;
    }
}


////////////////////////////////////////////////////////////
void Text::setCharacterSize(unsigned int size)
{
    if (m_characterSize != size)
    {
        m_characterSize      = size;
        m_geometryNeedUpdate = true;
    }
}


////////////////////////////////////////////////////////////
void Text::setLetterSpacing(float spacingFactor)
{
    if (m_letterSpacingFactor != spacingFactor)
    {
        m_letterSpacingFactor = spacingFactor;
        m_geometryNeedUpdate  = true;
    }
}


////////////////////////////////////////////////////////////
void Text::setLineSpacing(float spacingFactor)
{
    if (m_lineSpacingFactor != spacingFactor)
    {
        m_lineSpacingFactor  = spacingFactor;
        m_geometryNeedUpdate = true;
    }
}


////////////////////////////////////////////////////////////
void Text::setStyle(std::uint32_t style)
{
    if (m_style != style)
    {
        m_style              = style;
        m_geometryNeedUpdate = true;
    }
}


////////////////////////////////////////////////////////////
void Text::setFillColor(const FloatColor& color)
{
    m_fillColor = color;
}


////////////////////////////////////////////////////////////
void Text::setOutlineColor(const FloatColor& color)
{
    m_outlineColor = color;
}


////////////////////////////////////////////////////////////
void Text::setOutlineThickness(float thickness)
{
    if (thickness != m_outlineThickness)
    {
        m_outlineThickness   = thickness;
        m_geometryNeedUpdate = true;
    }
}


////////////////////////////////////////////////////////////
const std::string& Text::getString() const
{
    return m_string;
}


////////////////////////////////////////////////////////////
const Font* Text::getFont() const
{
    return m_font;
}


////////////////////////////////////////////////////////////
unsigned int Text::getCharacterSize() const
{
    return m_characterSize;
}


////////////////////////////////////////////////////////////
float Text::getLetterSpacing() const
{
    return m_letterSpacingFactor;
}


////////////////////////////////////////////////////////////
float Text::getLineSpacing() const
{
    return m_lineSpacingFactor;
}


////////////////////////////////////////////////////////////
std::uint32_t Text::getStyle() const
{
    return m_style;
}


////////////////////////////////////////////////////////////
const FloatColor& Text::getFillColor() const
{
    return m_fillColor;
}


////////////////////////////////////////////////////////////
const FloatColor& Text::getOutlineColor() const
{
    return m_outlineColor;
}


////////////////////////////////////////////////////////////
float Text::getOutlineThickness() const
{
    return m_outlineThickness;
}


////////////////////////////////////////////////////////////
Vector2f Text::findCharacterPos(std::size_t index) const
{
    // Adjust the index if it's out of range
    if (index > m_string.size())
        index = m_string.size();

    // Precompute the variables needed by the algorithm
    const bool  isBold          = m_style & Bold;
    float       whitespaceWidth = m_font->getGlyph(U' ', m_characterSize, isBold).advance;
    const float letterSpacing   = (whitespaceWidth / 3.f) * (m_letterSpacingFactor - 1.f);
    whitespaceWidth += letterSpacing;
    const float lineSpacing = m_font->getLineSpacing(m_characterSize) * m_lineSpacingFactor;

    // Compute the position
    Vector2f      position;
    std::uint32_t prevChar = 0;
    for (std::size_t i = 0; i < index; ++i)
    {
        const std::uint32_t curChar = m_string[i];

        // Apply the kerning offset
        position.x += m_font->getKerning(prevChar, curChar, m_characterSize, isBold);
        prevChar = curChar;

        // Handle special characters
        switch (curChar)
        {
            case U' ':
                position.x += whitespaceWidth;
                continue;
            case U'\t':
                position.x += whitespaceWidth * 4;
                continue;
            case U'\n':
                position.y += lineSpacing;
                position.x = 0;
                continue;
        }

        // For regular characters, add the advance offset of the glyph
        position.x += m_font->getGlyph(curChar, m_characterSize, isBold).advance + letterSpacing;
    }

    return position;
}

////////////////////////////////////////////////////////////
FloatRect Text::getLocalBounds() const
{
    ensureGeometryUpdate();

    return m_bounds;
}

void Text::render(glm::vec2 const& screen) const {
	ensureGeometryUpdate();

	glm::mat4 projection = glm::scale(glm::mat4(1.f), glm::vec3(1.f, -1.f, 1.f)) *  glm::ortho(0.f, screen.x, 0.f, screen.y);//, glm::vec3(1.f, -1.f, 1.f));

	shader->setUniform("projection", projection);
	shader->setUniform("color", FloatColor::Red);

	TextureBinder tex_binder(m_font->getTexture(m_characterSize));
	
	shader->bind([this]() {
		glDisable(GL_CULL_FACE);
		m_rawmodel.draw();
		glEnable(GL_CULL_FACE);
	});
}

////////////////////////////////////////////////////////////
void Text::ensureGeometryUpdate() const
{
    // Do nothing, if geometry has not changed and the font texture has not changed
    if (!m_geometryNeedUpdate && m_font->getTexture(m_characterSize).getID() == m_fontTextureId)
        return;

    // Save the current fonts texture id
    m_fontTextureId = m_font->getTexture(m_characterSize).getID();

    // Mark geometry as updated
    m_geometryNeedUpdate = false;

    // Clear the previous geometry
    m_vertices.clear();
    m_outlineVertices.clear();
    m_bounds = FloatRect();

    // No text: nothing to draw
    if (m_string.empty())
        return;

    // Compute values related to the text style
    const bool  isBold             = m_style & Bold;
    const bool  isUnderlined       = m_style & Underlined;
    const bool  isStrikeThrough    = m_style & StrikeThrough;
    const float italicShear        = (m_style & Italic) ? glm::radians(12.f) : 0.f;
    const float underlineOffset    = m_font->getUnderlinePosition(m_characterSize);
    const float underlineThickness = m_font->getUnderlineThickness(m_characterSize);

    // Compute the location of the strike through dynamically
    // We use the center point of the lowercase 'x' glyph as the reference
    // We reuse the underline thickness as the thickness of the strike through as well
    const FloatRect xBounds             = m_font->getGlyph(U'x', m_characterSize, isBold).bounds;
    const float     strikeThroughOffset = xBounds.top + xBounds.height / 2.f;

    // Precompute the variables needed by the algorithm
    float       whitespaceWidth = m_font->getGlyph(U' ', m_characterSize, isBold).advance;
    const float letterSpacing   = (whitespaceWidth / 3.f) * (m_letterSpacingFactor - 1.f);
    whitespaceWidth += letterSpacing;
    const float lineSpacing = m_font->getLineSpacing(m_characterSize) * m_lineSpacingFactor;
    float       x           = 0.f;
    auto        y           = static_cast<float>(m_characterSize);

    // Create one quad for each character
    auto          minX     = static_cast<float>(m_characterSize);
    auto          minY     = static_cast<float>(m_characterSize);
    float         maxX     = 0.f;
    float         maxY     = 0.f;
    std::uint32_t prevChar = 0;
    for (std::size_t i = 0; i < m_string.size(); ++i)
    {
        const std::uint32_t curChar = m_string[i];

        // Skip the \r char to avoid weird graphical issues
        if (curChar == U'\r')
            continue;

        // Apply the kerning offset
        x += m_font->getKerning(prevChar, curChar, m_characterSize, isBold);

        // If we're using the underlined style and there's a new line, draw a line
        if (isUnderlined && (curChar == U'\n' && prevChar != U'\n'))
        {
            addLine(m_vertices, x, y, underlineOffset, underlineThickness);

            if (m_outlineThickness != 0)
                addLine(m_outlineVertices, x, y, underlineOffset, underlineThickness, m_outlineThickness);
        }

        // If we're using the strike through style and there's a new line, draw a line across all characters
        if (isStrikeThrough && (curChar == U'\n' && prevChar != U'\n'))
        {
            addLine(m_vertices, x, y, strikeThroughOffset, underlineThickness);

            if (m_outlineThickness != 0)
                addLine(m_outlineVertices, x, y, strikeThroughOffset, underlineThickness, m_outlineThickness);
        }

        prevChar = curChar;

        // Handle special characters
        if ((curChar == U' ') || (curChar == U'\n') || (curChar == U'\t'))
        {
            // Update the current bounds (min coordinates)
            minX = std::min(minX, x);
            minY = std::min(minY, y);

            switch (curChar)
            {
                case U' ':
                    x += whitespaceWidth;
                    break;
                case U'\t':
                    x += whitespaceWidth * 4;
                    break;
                case U'\n':
                    y += lineSpacing;
                    x = 0;
                    break;
            }

            // Update the current bounds (max coordinates)
            maxX = std::max(maxX, x);
            maxY = std::max(maxY, y);

            // Next glyph, no need to create a quad for whitespace
            continue;
        }

		glm::uvec2 fontPageSize = m_font->getTexture(m_characterSize).getSize();

        // Apply the outline
        if (m_outlineThickness != 0)
        {
            const Glyph& glyph = m_font->getGlyph(curChar, m_characterSize, isBold, m_outlineThickness);

            // Add the outline glyph to the vertices
            addGlyphQuad(m_outlineVertices, fontPageSize, Vector2f(x, y), glyph, italicShear);
        }

        // Extract the current glyph's description
        const Glyph& glyph = m_font->getGlyph(curChar, m_characterSize, isBold);

        // Add the glyph to the vertices
        addGlyphQuad(m_vertices, fontPageSize, Vector2f(x, y), glyph, italicShear);

        // Update the current bounds
        const float left   = glyph.bounds.left;
        const float top    = glyph.bounds.top;
        const float right  = glyph.bounds.left + glyph.bounds.width;
        const float bottom = glyph.bounds.top + glyph.bounds.height;

        minX = std::min(minX, x + left - italicShear * bottom);
        maxX = std::max(maxX, x + right - italicShear * top);
        minY = std::min(minY, y + top);
        maxY = std::max(maxY, y + bottom);

        // Advance to the next character
        x += glyph.advance + letterSpacing;
    }

    // If we're using outline, update the current bounds
    if (m_outlineThickness != 0)
    {
        const float outline = std::abs(std::ceil(m_outlineThickness));
        minX -= outline;
        maxX += outline;
        minY -= outline;
        maxY += outline;
    }

    // If we're using the underlined style, add the last line
    if (isUnderlined && (x > 0))
    {
        addLine(m_vertices, x, y, underlineOffset, underlineThickness);

        if (m_outlineThickness != 0)
            addLine(m_outlineVertices, x, y, underlineOffset, underlineThickness, m_outlineThickness);
    }

    // If we're using the strike through style, add the last line across all characters
    if (isStrikeThrough && (x > 0))
    {
        addLine(m_vertices, x, y, strikeThroughOffset, underlineThickness);

        if (m_outlineThickness != 0)
            addLine(m_outlineVertices, x, y, strikeThroughOffset, underlineThickness, m_outlineThickness);
    }

    // Update the bounding rectangle
    m_bounds.left   = minX;
    m_bounds.top    = minY;
    m_bounds.width  = maxX - minX;
    m_bounds.height = maxY - minY;

	m_buffer->update(std::span(m_vertices));
	m_rawmodel.setNbVerticesToDraw(m_vertices.size());
}

} // namespace sf
