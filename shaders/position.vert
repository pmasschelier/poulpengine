#version 460 core
#extension GL_ARB_shading_language_include : require

#include "/common/camera.glsl"

layout (location = 0) in vec3 aPos;

out gl_PerVertex
{
	vec4 gl_Position;
	float gl_PointSize;
	float gl_ClipDistance[];
};

uniform mat4 model;

void main()
{
    gl_Position = projectionView * model * vec4(aPos, 1.0);
}