#version 460 core
#extension GL_ARB_shading_language_include : require

#include "/common/camera.glsl"
#include "/common/light_casters.glsl"

in VS_OUT {
	vec3 wPos;
    vec3 wNormal;
	vec2 texCoords;
};

uniform sampler2D tex;

out vec4 FragColor;

void main() {
	vec3 viewDir = normalize(wPos - position);
	vec3 diffuse = texture(tex, texCoords).xyz;
	vec3 color = additivePhongLighting(viewDir, wPos, wNormal, diffuse, diffuse, vec3(0.0), 0.0);
	FragColor = vec4(color, 1.0);
}