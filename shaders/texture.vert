#version 460 core
#extension GL_ARB_shading_language_include : require

#include "/common/camera.glsl"

layout (location = 0) in vec3 aPos;
layout (location = 2) in vec2 aTex;
layout (location = 3) in vec3 aNormal;

out VS_OUT {
	vec3 wPos;
    vec3 wNormal;
	vec2 texCoords;
};


out gl_PerVertex
{
	vec4 gl_Position;
	float gl_PointSize;
	float gl_ClipDistance[];
};

uniform mat4 model;
uniform mat3 inversedTransposedModel;

void main()
{
    gl_Position = projectionView * model * vec4(aPos, 1.0);
	wPos = vec3(model * vec4(aPos, 1.0));
	wNormal = normalize(inversedTransposedModel * aNormal);
	texCoords = aTex;
}