#ifndef GLOBAL_TRANSFORM_GLSL
#define GLOBAL_TRANSFORM_GLSL

struct GlobalTransform {
	mat4 model;
	mat4 inversedTransposedModel;
};

layout(std140, binding = 0) readonly buffer GlobalTransforms {
	GlobalTransform nodes[];
};

#endif