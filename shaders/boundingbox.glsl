#ifndef BOUNDINGBOX_GLSL
#define BOUNDINGBOX_GLSL

#include "/common/frustum.glsl"

struct AABB {
	vec3 center;
	vec3 extent;
};

void transformAABB(inout AABB aabb, mat4 transform) {
	aabb.center = vec3(transform * vec4(aabb.center, 1.0));

	vec3 left = vec3(transform[0]) * aabb.extent.x;
	vec3 up = vec3(transform[1]) * aabb.extent.y;
	vec3 forward = vec3(transform[2]) * aabb.extent.z;

	float newX = abs(dot(vec3(1.0, 0.0, 0.0), left)) 
	 + abs(dot(vec3(1.0, 0.0, 0.0), up))
	 + abs(dot(vec3(1.0, 0.0, 0.0), forward));
	float newY = abs(dot(vec3(0.0, 1.0, 0.0), left)) 
	 + abs(dot(vec3(0.0, 1.0, 0.0), up))
	 + abs(dot(vec3(0.0, 1.0, 0.0), forward));
	float newZ = abs(dot(vec3(0.0, 0.0, 1.0), left)) 
	 + abs(dot(vec3(0.0, 0.0, 1.0), up))
	 + abs(dot(vec3(0.0, 0.0, 1.0), forward));
	aabb.extent = vec3(newX, newY, newZ);
}
#line 0

float getSignedDistanceToPlane(vec4 plane, vec3 point)
{
    return dot(plane.xyz, point) + plane.w;
}

bool isOnForwardPlane(vec4 plane, AABB aabb) {
	// Compute the projection interval radius of b onto L(t) = b.c + t * p.n
    const float r = dot(aabb.extent, abs(plane.xyz));
    return getSignedDistanceToPlane(plane, aabb.center) + r >= 0;
}

bool isInFrustum(AABB aabb, Frustum frustum) {
	return isOnForwardPlane(frustum.leftFace, aabb)
	&& isOnForwardPlane(frustum.rightFace, aabb)
	&& isOnForwardPlane(frustum.topFace, aabb)
	&& isOnForwardPlane(frustum.bottomFace, aabb)
	&& isOnForwardPlane(frustum.nearFace, aabb)
	&& isOnForwardPlane(frustum.farFace, aabb); 
}

#endif