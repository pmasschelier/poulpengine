#version 460 core

layout(points) in;
layout(points, max_vertices = 1) out;

in uint shouldRender[1];
in mat4 model[1];
in mat4 inversedTransposedModel[1];

layout(xfb_buffer = 0) out GlobalTransformData {
	layout(xfb_offset = 0) mat4 outModel;
	layout(xfb_offset = 64) mat4 outInversedTransposedModel;
};

void main() {
    /* only emit primitive if the object is visible */
    if (shouldRender[0] == 1)
    {
        outModel = model[0];
		outInversedTransposedModel = inversedTransposedModel[0];
        EmitVertex();
        EndPrimitive();
    }
}