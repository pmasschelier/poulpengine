#version 460 core
#extension GL_ARB_shading_language_include : require

#include "/common/camera.glsl"
#include "/common/boundingbox.glsl"
#include "/common/global_transforms.glsl"

uniform vec3 aabbCenter;
uniform vec3 aabbExtent;

/* layout (location = 8) in mat4 model;
layout (location = 13) in mat3 inversedTransposedModel; */

out uint shouldRender;
out mat4 model;
out mat4 inversedTransposedModel;

void main()
{
	model = nodes[gl_InstanceID].model;
	inversedTransposedModel = nodes[gl_InstanceID].inversedTransposedModel;

	AABB aabb;
	aabb.center = aabbCenter;
	aabb.extent = aabbExtent;
	transformAABB(aabb, nodes[gl_InstanceID].model);
	shouldRender = uint(isInFrustum(aabb, frustum));
}
