echo "#include <string_view>"
echo
echo "namespace Sources {"
echo

for filename in *; do
	test "$filename" = "embed_shaders.sh" && continue
	echo "constexpr std::string_view" `echo $filename | tr .a-z _A-Z` "= R\"("
	cat $filename
	echo
	echo ')";'
	echo
	echo
done

echo "} // namespace Sources"