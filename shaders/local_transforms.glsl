#ifndef LOCAL_TRANSFORM_GLSL
#define LOCAL_TRANSFORM_GLSL

struct SceneNodeTransform {
	uint parentIndex;
	uint boundingBoxIndex;
	uint shouldUpdate;
	mat4 model;
};

layout(std140, binding = 0) buffer LocalTransforms {
	SceneNodeTransform nodes[];
};

#endif