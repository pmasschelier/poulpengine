#ifndef CAMERA_GLSL
#define CAMERA_GLSL

#include "/common/frustum.glsl"

layout (std140, binding = 0) uniform Camera
{
	mat4 projection; // 4 * 4 * 4 bytes = align 16
	mat4 view; // 4 * 4 * 4 bytes = align 16
	mat4 projectionView; // 4 * 4 * 4 bytes = align 16
	mat4 inversedTransposedView; // 4 * 4 * 4 bytes = align 12
	vec3 position; // 3 * 3 bytes = align 12
	// padding 4
	vec3 billboardHorizontalVector; // 3 * 3 bytes = align 12
	// padding 4
	vec3 billboardVerticalVector; // 3 * 3 bytes = align 12
	Frustum frustum;
};

#endif