#version 460 core
#extension GL_ARB_shading_language_include : require

#include "/common/camera.glsl"
#include "/common/global_transforms.glsl"

layout (location = 0) in vec3 aPos;
layout (location = 2) in vec2 aTex;
layout (location = 3) in vec3 aNormal;

out VS_OUT {
	vec3 wPos;
    vec3 wNormal;
	vec2 texCoords;
};

out gl_PerVertex
{
	vec4 gl_Position;
	float gl_PointSize;
	float gl_ClipDistance[];
};

/* layout (location = 8) in mat4 model;
layout (location = 12) in mat3 inversedTransposedModel; */

void main()
{
	const mat4 model = nodes[gl_InstanceID].model;
	const mat4 inversedTransposedModel = nodes[gl_InstanceID].inversedTransposedModel;

    gl_Position = projectionView * model * vec4(aPos, 1.0);
	wPos = vec3(model * vec4(aPos, 1.0));
	wNormal = normalize(mat3(inversedTransposedModel) * aNormal);
	texCoords = aTex;
}