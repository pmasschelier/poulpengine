#version 460 core

in vec2 texCoords;

uniform sampler2D sampler;
uniform vec4 color;

out vec4 FragColor;

void main()
{
	vec4 texel = texture(sampler, texCoords);
	if(texel.a < 0.5)
		discard;
	FragColor = texel;
}
