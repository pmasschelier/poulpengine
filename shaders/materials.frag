#version 460 core
#extension GL_ARB_shading_language_include : require

in VS_OUT {
	vec3 wPos;
    vec3 wNormal;
};

#include "/common/light_casters.glsl"
#include "/common/camera.glsl"

out vec4 FragColor;

void main()
{
	vec3 viewDir = normalize(wPos - position);
	vec3 color = additivePhongLighting(viewDir, wPos, wNormal);
	FragColor = vec4(color, 1.0);
}