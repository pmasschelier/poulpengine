#version 460 core
#extension GL_ARB_shading_language_include : require

#include "/common/camera.glsl"

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;

out gl_PerVertex
{
	vec4 gl_Position;
	float gl_PointSize;
	float gl_ClipDistance[];
};

out vec3 vertexColor;

void main()
{
    gl_Position = projectionView * vec4(aPos, 1.0);
	vertexColor = aColor;
}