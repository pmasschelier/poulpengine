#version 460 core
#extension GL_ARB_shading_language_include : require

layout (triangles) in;
layout (line_strip, max_vertices = 6) out;

#include "/common/camera.glsl"

in gl_PerVertex
{
	vec4 gl_Position;
	float gl_PointSize;
	float gl_ClipDistance[];
} gl_in[];

out gl_PerVertex
{
	vec4 gl_Position;
	float gl_PointSize;
	float gl_ClipDistance[];
};

in VS_OUT {
	vec3 wPos;
    vec3 wNormal;
} gs_in[];

const float MAGNITUDE = 0.4;

void GenerateLine(int index)
{
	vec3 p1 = gs_in[index].wPos;
	vec3 p2 = p1 + gs_in[index].wNormal * MAGNITUDE;

    gl_Position = projectionView * vec4(p1, 1.0);
    EmitVertex();

    gl_Position = projectionView * vec4(p2, 1.0);

    EmitVertex();
    EndPrimitive();
}

void main()
{
    GenerateLine(0); // first vertex normal
    GenerateLine(1); // second vertex normal
    GenerateLine(2); // third vertex normal
}  
