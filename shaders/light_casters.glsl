#ifndef LIGHTCASTERS_GLSL
#define LIGHTCASTERS_GLSL

struct Light {
	uint type; 		// 4 bytes = align 4
	float constant; // 4 bytes = align 4
	float linear; 	// 4 bytes = align 4
	float quadratic; // 4 bytes = align 4
	vec3 position; 	// 4 * 4 bytes = align 16
	vec3 direction;	// 4 * 4 bytes = align 16
	vec3 color;		// 4 * 4 bytes = align 16
};

layout (std140, binding = 1) uniform Lights
{
	uint nb_lights;		// 4 bytes = align 4
	uint on;
	Light lights[32];	// 64 bytes, align 16
};

layout (std140, binding = 2) uniform Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
} material;

const uint POINT_LIGHT = 0;
const uint DIRECTIONAL_LIGHT = 1;

vec3 calcLight(uint index, vec3 fragPos, vec3 fragNormal, vec3 viewDir, vec3 ambient, vec3 diffuse, vec3 specular, float shininess)
{
	vec3 lightDir;
	uint type = lights[index].type;
	float constant = lights[index].constant;
	float linear = lights[index].linear;
	float quadratic = lights[index].quadratic;
	vec3 position = lights[index].position;
	vec3 direction = lights[index].direction;
	vec3 color = lights[index].color;

	if(type == POINT_LIGHT)
		lightDir = normalize(fragPos - position);
	else if(type == DIRECTIONAL_LIGHT)
		lightDir = normalize(direction);
	
	// diffuse shading
	float diff = max(- dot(fragNormal, lightDir), 0.0);

	// specular shading
	vec3 reflectDir = reflect(lightDir, fragNormal);
	float spec = pow(max(- dot(viewDir, reflectDir), 0.0), shininess);

	// attenuation
	float distance    = length(fragPos - position);
	float attenuation = 1.0 / (constant + linear * distance + quadratic * (distance * distance));

	// combine results
	ambient *= 0.1;
	diffuse *= 1.0 * diff;
	specular *= 0.5 * spec;
	return (ambient + diffuse + specular) * attenuation * color;
}

vec3 additivePhongLighting(vec3 viewDir, vec3 fragPos, vec3 fragNormal, vec3 ambient, vec3 diffuse, vec3 specular, float shininess) {
	if(on == 0)
		return ambient;
	vec3 color = vec3(0.0);
	for(uint i = 0; i < nb_lights; i++)
		color += calcLight(i, fragPos, fragNormal, viewDir, ambient, diffuse, specular, shininess);
	return color;
}

vec3 additivePhongLighting(vec3 viewDir, vec3 fragPos, vec3 fragNormal) {
	if(on == 0)
		return material.ambient;
	vec3 color = vec3(0.0);
	for(uint i = 0; i < nb_lights; i++)
		color += calcLight(i, fragPos, fragNormal, viewDir, material.ambient, material.diffuse, material.specular, material.shininess);
	return color;
}

#endif