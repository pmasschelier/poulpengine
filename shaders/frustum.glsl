#ifndef FRUSTUM_GLSL
#define FRUSTUM_GLSL


struct Frustum {
	vec4 topFace;
    vec4 bottomFace;

    vec4 rightFace;
    vec4 leftFace;

    vec4 farFace;
    vec4 nearFace;
};

#endif