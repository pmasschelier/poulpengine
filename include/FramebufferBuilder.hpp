#ifndef FRAMEBUFFERBUILDER_HPP
#define FRAMEBUFFERBUILDER_HPP

#include "Framebuffer.hpp"
#include "Textures/Texture.hpp"
#include "Utils.hpp"

#include <glm/vec2.hpp>

#include <optional>

namespace plp
{

class FramebufferBuilder
{
	
public:

	FramebufferBuilder() = default;

	void addColorAttachment(std::shared_ptr<Texture> texture);
	void setDepthAttachment(std::shared_ptr<Texture> depthTexture);
	void setStencilAttachment(std::shared_ptr<Texture> stencilTexture);

	std::optional<Framebuffer> create(bool noWriteNoRead = false);

	[[nodiscard]] std::optional<Framebuffer>
	static createDepthMap(glm::uvec2 resolution);

private:

	Collection<Texture> colorAttachments;
	std::shared_ptr<Texture> depthAttachment, stencilAttachment;

};

} // namespace plp


#endif