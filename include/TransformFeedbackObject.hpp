#ifndef TRANSFORM_FEEDBACK_OBJECT_HPP
#define TRANSFORM_FEEDBACK_OBJECT_HPP


#include "ObjectGL.hpp"
#include "QueryObject.hpp"
#include "StaticBufferStorage.hpp"

#include <functional>
#include <memory>
#include <vector>

namespace plp
{

class TransformFeedbackObject : public ObjectGL
{

public:

	TransformFeedbackObject();

	~TransformFeedbackObject();

	TransformFeedbackObject(TransformFeedbackObject&&) = default;

	TransformFeedbackObject& operator=(TransformFeedbackObject&&) = default;

	GLuint record(GLenum primitiveMode, std::function<void()> drawAction, bool save = false) const;

	bool setBuffer(std::shared_ptr<StaticBufferStorage> const& buffer, unsigned char index = 0);

	std::shared_ptr<StaticBufferStorage> getBuffer(unsigned char index) const;

	static unsigned int getMaxTransformFeedbackBuffers();

private:

	QueryObject<TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN> query;

	std::vector<std::shared_ptr<StaticBufferStorage>> buffers;

	bool recordPrimitivesGenerated = false;

};

} // namespace plp

#endif