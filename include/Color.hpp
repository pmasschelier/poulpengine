#ifndef COLOR_HPP
#define COLOR_HPP

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

using b4 = glm::vec<4, std::uint8_t>;
using f4 = glm::vec<4, float>;

namespace plp
{

struct ByteColor : public b4 {

ByteColor() : b4() {}
ByteColor(b4 const& other): b4(other) {}

static constexpr auto White = b4(1);
static constexpr auto Transparent = b4(0);
static constexpr auto Black = b4(glm::vec3(0), 1);
static constexpr auto Red = b4(1, 0, 0, 1);
static constexpr auto Green = b4(0, 1, 0, 1);
static constexpr auto Blue = b4(0, 0, 1, 1);

};

struct FloatColor : public f4 {

FloatColor() : f4() {}
FloatColor(b4 const& other): f4(other) {}

static constexpr auto White = f4(1);
static constexpr auto Transparent = f4(0);
static constexpr auto Black = f4(glm::vec3(0.f), 1.f);
static constexpr auto Red = f4(1.f, 0.f, 0.f, 1.f);
static constexpr auto Green = f4(0.f, 1.f, 0.f, 1.f);
static constexpr auto Blue = f4(0.f, 0.f, 1.f, 1.f);

};

} // namespace plp

#endif