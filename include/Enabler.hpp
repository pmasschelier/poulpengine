#ifndef ENABLER_HPP
#define ENABLER_HPP

#include <GL/glew.h>

namespace plp
{

class Enabler
{
private:
	GLenum cap;
public:
	Enabler(GLenum cap) : cap(cap)
	{
		glEnable(cap);
	}

	~Enabler()
	{
		glDisable(cap);
	}
};

} // namespace plp

#endif