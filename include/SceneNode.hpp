#ifndef SCENENODE_HPP
#define SCENENODE_HPP

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>

#include <memory>
#include <mutex>
#include <set>
#include <string>

#include <Renderable.hpp>
#include <TransformFeedbackParticleSystem.hpp>

namespace plp {

class TransformBuffer;

class SceneNode : public std::enable_shared_from_this<SceneNode>
{

public:

	struct ModelData {
		std::shared_ptr<Model> renderable;
		std::uint32_t queries;
		Renderable::QueryData* data;
	};

	struct RenderInstance {
		std::shared_ptr<InstancedModel> model;
		unsigned int instanceId;
	};

private:

	SceneNode(std::string const& name, std::uint32_t transformId, TransformBuffer& buffer)
		: name(name), transformId(transformId), buffer(buffer) {}

public:

	static std::shared_ptr<SceneNode> create(std::string const& name, std::uint32_t transformId, TransformBuffer& buffer);

	virtual ~SceneNode() = default;

	SceneNode(SceneNode const&) = delete;
	SceneNode& operator=(SceneNode const&) = delete;

	std::string getName() const;

	std::uint32_t getTransformId() const;

	std::weak_ptr<SceneNode> getParent() const;

	bool addChild(std::weak_ptr<SceneNode> const& child);
	
	bool removeChild(std::shared_ptr<SceneNode> const& child);
	
	std::size_t childrenCount() const;

	void addModel(std::shared_ptr<Model> renderable, std::uint32_t queries = 0, Renderable::QueryData* data = nullptr);

	//void addModel(std::shared_ptr<InstancedModel> instancedModel, unsigned int instanceId);

	std::size_t modelCount() const;

	void addParticleSystem(std::shared_ptr<TransformFeedbackParticleSystem> system);

	void setTransformations(glm::vec3 const& position, glm::quat const& rotation, glm::vec3 scaleFactor);

	void setPosition(glm::vec3 const& position);
	void move(glm::vec3 const& displacement);
	glm::vec3 getPosition() const;

	void setRotation(glm::quat const& rotation);
	void rotate(glm::quat const& rotation);
	glm::quat getRotation() const;

	void setScale(glm::vec3 const& scaleFactor);
	void scale(glm::vec3 const& scaleFactor);
	glm::vec3 getScale() const;

	bool localTransformUpdated() const;
	bool globalTransformUpdated() const;

	glm::mat4 getLocalTransformMatrix();
	glm::mat4 getGlobalTransformMatrix();

	glm::mat3 getInversedTransposedLocalTransformMatrix();
	glm::mat3 getInversedTransposedGlobalTransformMatrix();

	void copyLocalTransform(SceneNode const& other);
	void copyObjects(SceneNode const& other);

	void renderMeshes();
	void renderParticleSystems();

	std::vector<std::weak_ptr<SceneNode>> getChildren() const;

protected:

	void notifyLocalUpdate();
	void notifyGlobalUpdate();

	std::weak_ptr<SceneNode> parent;
	std::vector<std::weak_ptr<SceneNode>> children;

	std::vector<ModelData> objects;
	Collection<TransformFeedbackParticleSystem> systems;
	std::vector<RenderInstance> instancedModels;

	glm::vec3 position = glm::vec3(0.f);
	glm::quat rotation = glm::quat(1.f, 0.f, 0.f, 0.f);
	glm::vec3 scaleFactor = glm::vec3(1.f);

private:

	std::string name;
	std::uint32_t transformId;

	bool updateCachedData = true;

	bool localTransformNeedsUpdate = true;
	bool globalTransformNeedsUpdate = true;

	glm::mat4 localTransform;
	glm::mat4 globalTransform;

	bool inversedTransposedLocalTransformNeedsUpdate = true;
	bool inversedTransposedGlobalTransformNeedsUpdate = true;

	glm::mat3 inversedTransposedLocalTransform;
	glm::mat3 inversedTransposedGlobalTransform;

	TransformBuffer& buffer;

};

}

#endif
