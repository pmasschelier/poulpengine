#pragma once

#include <vector>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include "Color.hpp"

using Vector2i = glm::ivec2;
using Vector2u = glm::uvec2;
using Vector2f = glm::vec2;
template<typename T>
using Vector2 = glm::vec<2, T>;
using VertexArray = std::vector<glm::vec2>;
