#ifndef ATTRIBUTE_HPP
#define ATTRIBUTE_HPP

#include <GL/glew.h>

#include <array>
#include <numeric>
#include <utility>

#include "Utils.hpp"

namespace plp
{

struct Attribute
{
	enum Type
	{
		BYTE = GL_BYTE,
		UNSIGNED_BYTE = GL_UNSIGNED_BYTE,
		SHORT = GL_SHORT,
		UNSIGNED_SHORT = GL_UNSIGNED_SHORT,
		INT = GL_INT,
		UNSIGNED_INT = GL_UNSIGNED_INT,
		FIXED = GL_FIXED,
		HALF_FLOAT = GL_HALF_FLOAT,
		FLOAT = GL_FLOAT,
		DOUBLE = GL_DOUBLE
	};

	enum Width {
		SCALAR = 1,
		VEC2 = 2,
		VEC3 = 3,
		VEC4 = 4
	};

	enum Layout
	{
		POSITIONS,
		COLORS,
		UVS,
		NORMALS,
		MODEL = 8,
		INVERSED_TRANSPOSED_MODEL = 12
	};

	static constexpr auto DUMMY = static_cast<unsigned int>(-1);

	Type type { FLOAT };
	Width width { VEC3 };
	unsigned index { POSITIONS };
 
	constexpr std::size_t getByteSize() const
	{
		return width * sizeOf(type);
	}

	constexpr static std::size_t sizeOf(Type type)
	{
		if(BYTE == type || UNSIGNED_BYTE == type) return 1;
		if(SHORT == type || UNSIGNED_SHORT == type || HALF_FLOAT == type) return 2;
		if(INT == type || UNSIGNED_INT == type || FIXED == type || FLOAT == type) return 4;
		if(DOUBLE == type) return 8;
		std::unreachable();
	}

	constexpr std::size_t alignOf()
	{
		if(Attribute::VEC3 == width)
			return Attribute::VEC4 * sizeOf(type);
		return width * sizeOf(type);
	}

	template<std::size_t Rows, Width Column>
	constexpr std::array<Attribute, Rows> static matrixFloat(unsigned int index) {
		std::array<Attribute, Rows> ret;
		for(unsigned int i = 0; i < Rows; i++)
			ret[i] = Attribute { FLOAT, Column, index + i };
		return ret;
	}

	constexpr static bool isInteger(Type type)
	{
		return !(FIXED == type || HALF_FLOAT == type || FLOAT == type || DOUBLE == type);
	}

	constexpr std::string_view typeStr() const
	{
		switch(type) {
			CASE_VALUE(BYTE);
			CASE_VALUE(UNSIGNED_BYTE);
			CASE_VALUE(SHORT);
			CASE_VALUE(UNSIGNED_SHORT);
			CASE_VALUE(INT);
			CASE_VALUE(UNSIGNED_INT);
			CASE_VALUE(FIXED);
			CASE_VALUE(HALF_FLOAT);
			CASE_VALUE(FLOAT);
			CASE_VALUE(DOUBLE);
		}
		std::unreachable();
	}

	template <std::input_iterator Iter, 
		std::enable_if_t<std::is_base_of<Attribute, typename std::iterator_traits<Iter>::value_type>::value, bool> = true>
	constexpr static std::size_t computeStride(Iter const& begin, Iter const& end)
	{
		return std::accumulate(begin, end, 0, [](std::size_t const& acc, Attribute const& attrib) {
			return acc + attrib.getByteSize();
		});
	}

	template<Type T>
	constexpr static std::vector<Attribute> matrix(std::size_t R, Width C, unsigned int index) {
		std::vector<Attribute> attributes;
		for(unsigned i = 0; i < R; i++)
			attributes.emplace_back(T, C, index + i);
		return attributes;
	}
};

constexpr Attribute POSITIONS_3D = { Attribute::FLOAT, Attribute::VEC3, Attribute::POSITIONS };
constexpr Attribute NORMALS_3D = { Attribute::FLOAT, Attribute::VEC3, Attribute::NORMALS };
constexpr Attribute TEXCOORDS = { Attribute::FLOAT, Attribute::VEC2, Attribute::UVS };

} // namespace plp


#endif