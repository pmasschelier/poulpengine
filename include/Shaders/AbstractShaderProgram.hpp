#ifndef ABSTRACTSHADERPROGRAM_HPP
#define ABSTRACTSHADERPROGRAM_HPP

#include "ObjectGL.hpp"
#include "Binder.hpp"

#include <glm/glm.hpp>

#include <string>

namespace plp
{

class AbstractShaderProgram : public Bindable, ObjectGL
{

public:

	using ObjectGL::getID;

    virtual bool setUniform(const std::string &name, bool value) = 0;  
    virtual bool setUniform(const std::string &name, int value) = 0;
	virtual bool setUniform(const std::string &name, unsigned int value) = 0;
    virtual bool setUniform(const std::string &name, float value) = 0;
	virtual bool setUniform(const std::string &name, glm::vec2 vec) = 0;
    virtual bool setUniform(const std::string &name, glm::vec3 vec) = 0;
	virtual bool setUniform(const std::string &name, glm::vec4 vec) = 0;
    virtual bool setUniform(const std::string &name, glm::mat3 mat) = 0;
    virtual bool setUniform(const std::string &name, glm::mat4 mat) = 0;


protected:

	using ObjectGL::id;

};

} // namespace plp


#endif