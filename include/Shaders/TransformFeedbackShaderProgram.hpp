#ifndef TRANSFORMFEEDBACK_SHADERPROGRAM_HPP
#define TRANSFORMFEEDBACK_SHADERPROGRAM_HPP

#include "Shaders/ShaderProgram.hpp"
#include "TransformFeedbackObject.hpp"
#include "RawModel.hpp"

namespace plp
{



class TransformFeedbackShaderProgram : public ShaderProgram
{

	friend ShaderBuilder;
	
private:

	TransformFeedbackShaderProgram(std::uint8_t types, PrimitiveType recordedPrimitive, std::vector<std::string> const& recordedAttributesNames);

public:

	TransformFeedbackShaderProgram(TransformFeedbackShaderProgram&) =  delete;
	TransformFeedbackShaderProgram& operator=(TransformFeedbackShaderProgram&) = delete;

	TransformFeedbackShaderProgram(TransformFeedbackShaderProgram&&) = default;
	TransformFeedbackShaderProgram& operator=(TransformFeedbackShaderProgram&&) = default;

	TransformFeedbackObject& getTransformFeedBackObject();
	void setTransformFeedBackObject(std::shared_ptr<TransformFeedbackObject>& tf);
	
	void bind(std::function<void()> const& action, bool save = false) const;

private:

	TransformFeedbackObject tf;
	std::shared_ptr<TransformFeedbackObject> replacement_tf;

	PrimitiveType recordedPrimitive;

};


} // namespace plp


#endif