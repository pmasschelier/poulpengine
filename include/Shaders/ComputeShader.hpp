#ifndef COMPUTESHADER_HPP
#define COMPUTESHADER_HPP

#include "Shaders/ShaderProgram.hpp"

namespace plp
{

class ShaderBuilder;

class ComputeShader : public ShaderProgram
{

	friend ShaderBuilder;

protected:

	ComputeShader();

public:

	void execute(glm::uvec3 numWorkGroups);

	glm::uvec3 getWorkGroupSize();

	static glm::uvec3 getMaxComputeWorkGroupCount();

	static glm::uvec3 getMaxComputeWorkGroupSize();

	static unsigned getMaxComputeWorkGroupInvocations();

private:

	bool cachedWorkGroupSize = false;

	glm::ivec3 workGroupSize;

	using ShaderProgram::bind;

};

} // namespace plp


#endif