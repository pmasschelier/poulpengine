#ifndef SHADER_HPP
#define SHADER_HPP

#include "Binder.hpp"
#include "Shaders/AbstractShaderProgram.hpp"
#include "Shaders/ShaderObject.hpp"
#include "Utils.hpp"

#include <array>
#include <functional>
#include <glm/gtc/type_ptr.hpp>
#include <unordered_map>
#include <vector>

namespace plp
{

class ProgramPipeline;
class ProgramInterface;
class ShaderBuilder;

class ShaderProgram : public AbstractShaderProgram
{

	friend ProgramPipeline;
	friend ShaderBuilder;

public:

	enum UniformBlockBindingPoint
	{
		CAMERA,
		LIGHTS,
		MATERIAL,
		PARTICLE_SYSTEM_PARAMETERS
	};

	enum ShaderStorageBindingPoint
	{
		GLOBAL_TRANSFORMS,
		LOCAL_TRANSFORMS,
		COMMANDS,
		INSTANCES
	};

protected:

	ShaderProgram(std::uint8_t types);

public:

	ShaderProgram(ShaderProgram &other) = delete;
	ShaderProgram &operator=(ShaderProgram const &other) = delete;

	ShaderProgram(ShaderProgram &&other) = default;
	ShaderProgram &operator=(ShaderProgram &&other) = default;

	void outputInterface(std::ostream &out) const;

	bool linkedSuccessfully() const;

	bool isSeparable() const;

	bool isRetrievable() const;

	bool setUniform(const std::string &name, bool value) override;
	bool setUniform(const std::string &name, int value) override;
	bool setUniform(const std::string &name, unsigned int value) override;
	bool setUniform(const std::string &name, float value) override;
	bool setUniform(const std::string &name, glm::vec2 vec) override;
	bool setUniform(const std::string &name, glm::vec3 vec) override;
	bool setUniform(const std::string &name, glm::vec4 vec) override;
	bool setUniform(const std::string &name, glm::mat3 mat) override;
	bool setUniform(const std::string &name, glm::mat4 mat) override;

	bool bindUniformBlockToBindingPoint(const std::string &uniformBlockName, const GLuint bindingPoint) const;

	void outputProperties(std::ostream &out) const;
	void outputInfoLog(std::ostream &out) const;
	static void implementationLimits(std::ostream &out);

	static const std::array<ProgramInterface, 21> PROGRAM_INTERFACES;

	virtual void bind(std::function<void()> const &action, bool save = false) const override;

protected:

	bool attach(AbstractShaderObject const& shader);

	void detach(AbstractShaderObject const& shader);

	bool link(bool separable = false, bool retrievable = false);

	GLint getUniformLocation(std::string const &name) const;

	static constexpr std::string_view printType(GLenum type);

private:

	std::uint8_t types{};

	bool separable = false;
	bool retrievable = false;

	mutable std::unordered_map<std::string, GLint> locations;
};

} // namespace plp

#endif
