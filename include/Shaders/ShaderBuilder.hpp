#ifndef SHADER_BUILDER_HPP
#define SHADER_BUILDER_HPP

#include "Shaders/ComputeShader.hpp"
#include "TransformFeedbackShaderProgram.hpp"

#include <map>

namespace plp
{

enum CommonShaderSource
{
	VERTEX_PASSTHROUGH_SOURCE,
	VERTEX_POSITIONS_SOURCE,
	VERTEX_2D_SPRITE_SOURCE,
	FRAGMENT_COLOR_TEXTURE_SOURCE,
	VERTEX_COLORS_SOURCE,
	FRAGMENT_COLORS_SOURCE,
	VERTEX_TEXTURE_SOURCE,
	VERTEX_TEXTURE_INSTANCED_SOURCE,
	FRAGMENT_TEXTURE_SOURCE,
	VERTEX_CULLING_SOURCE,
	GEOMETRY_CULLING_SOURCE,
	FRAGMENT_UNICOLOR_SOURCE,
	GEOMETRY_NORMALS_SOURCE,
	FRAGMENT_MATERIAL_SOURCE,
	NUM_SHADER_SOURCES
};

enum CommonProgram
{
	BASIC_MODEL_PROGRAM,
	INSTANCED_MODEL_PROGRAM,
	NUM_PROGRAMS
};

enum CommonTransformFeedbackProgram
{
	CULLING_PROGRAM,
	NUM_TRANSFORMFEEDBACK_PROGRAMS,
};

enum CommonModuleSource
{
	FRUSTUM_GLSL_SOURCE,
	CAMERA_GLSL_SOURCE,
	BOUNDINGBOX_GLSL_SOURCE,
	RANDOM_GLSL_SOURCE,
	LIGHT_CASTERS_GLSL_SOURCE,
	FOG_GLSL_SOURCE,
	GLOBAL_TRANSFORMS_GLSL_SOURCE,
	NUM_MODULE_SOURCES
};

class ShaderBuilder
{

public:

	ShaderBuilder();

	/* \brief Ajoute un shader au pipeline
	 * Charge le fichier source et compile avant de l'ajouter à la liste des shaders
	 * \param filename Chemin vers le fichier à charger
	 * \param shaderType GL_VERTEX_SHADER, GL_TESS_CONTROL_SHADER, GL_TESS_EVALUATION_SHADER, GL_GEOMETRY_SHADER, GL_FRAGMENT_SHADER,
	 * GL_COMPUTE_SHADER
	 */
	Opt<ShaderObject> addFile(ShaderType type, std::filesystem::path const &filename);

	/* \brief Ajoute un shader au pipeline
	 * Compile le code source avant de l'ajouter à la liste des shaders
	 * \param filename Code source
	 * \param shaderType GL_VERTEX_SHADER, GL_TESS_CONTROL_SHADER, GL_TESS_EVALUATION_SHADER, GL_GEOMETRY_SHADER, GL_FRAGMENT_SHADER,
	 * GL_COMPUTE_SHADER \param name nom du shader (facultatif, uniquement à des fins de débogage)
	 */
	Opt<ShaderObject> addSource(ShaderType type, std::string const &name, std::string_view const &source);

	/* \brief Ajoute un shader au pipeline
	 * \param shader Shader a ajouter
	 */
	bool addShader(std::shared_ptr<ShaderObject> const &shader);

	void clear();

	[[nodiscard]] std::optional<ShaderProgram> createProgram(bool separable = false, bool retrievable = false);

	[[nodiscard]] std::optional<TransformFeedbackShaderProgram> createTransformFeedbackProgram(
		PrimitiveType recordedPrimitive, std::vector<std::string> const &recordedAttributesNames, bool separable = false, bool retrievable = false
	);

	[[nodiscard]] static Opt<ShaderProgram> createStandaloneShaderFromFile(
		ShaderType type, std::filesystem::path const &filename, bool retrievable = false
	);

	[[nodiscard]] std::optional<ComputeShader> createComputeShaderFromFile(std::filesystem::path const &filename, bool retrievable = false);

	/* [[nodiscard]] static std::optional<ShaderObject> createShaderFromFile(ShaderObject::Type shaderType, std::filesystem::path const &path);

	[[nodiscard]] static std::optional<ShaderObject> createShaderFromMemory(
		ShaderObject::Type shaderType, std::string const &name, std::string_view const &source
	); */

	[[nodiscard]] static bool loadModule(std::string const &virtual_filename, std::filesystem::path const &path);

	static std::shared_ptr<ShaderObject> getCommonShader(CommonShaderSource num);

	static std::shared_ptr<ShaderProgram> getCommonProgram(CommonProgram num);

	static std::shared_ptr<TransformFeedbackShaderProgram> getCommonTransformFeedbackProgram(CommonTransformFeedbackProgram num);

private:

	[[nodiscard]] static bool loadCommonModule(CommonModuleSource source);

	bool create(ShaderProgram &program, bool separable, bool retrievable);

	Collection<AbstractShaderObject> objects;

	GLuint types{};

	static std::array<std::shared_ptr<ShaderObject>, NUM_SHADER_SOURCES> common_shaders;

	static std::array<std::shared_ptr<ShaderProgram>, NUM_PROGRAMS> common_programs;

	static std::array<std::shared_ptr<TransformFeedbackShaderProgram>, NUM_TRANSFORMFEEDBACK_PROGRAMS> common_tf_programs;

	static std::array<bool, NUM_MODULE_SOURCES> common_modules;

	static std::map<std::string, std::filesystem::path> modulesLoaded;
};

} // namespace plp

#endif
