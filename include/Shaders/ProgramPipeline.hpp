#ifndef PROGRAMPIPELINE_HPP
#define PROGRAMPIPELINE_HPP

#include "Shaders/ShaderProgram.hpp"

#include <functional>
#include <unordered_map>

namespace plp
{

class ProgramPipeline : public AbstractShaderProgram
{

public:

	ProgramPipeline();

	~ProgramPipeline();

	bool useProgram(std::shared_ptr<ShaderProgram> const& program);

	bool useProgramStages(std::shared_ptr<ShaderProgram> const& program, GLbitfield stages);

	void removeProgram(std::shared_ptr<ShaderProgram> const& program);

	template<typename T>
	GLbitfield setUniformForStage(GLbitfield stages, const std::string &name, T value) {
		GLbitfield stages_set{};
		foreachStage(stages, [this, name, value, &stages_set](unsigned i) {
			m_stages[i] && m_stages[i]->setUniform(name, value) && (stages_set |= (1 << i)); 
		});
		return stages_set;
	}

    bool setUniform(const std::string &name, bool value) override;  
    bool setUniform(const std::string &name, int value) override;
	bool setUniform(const std::string &name, unsigned int value) override;
    bool setUniform(const std::string &name, float value) override;
	bool setUniform(const std::string &name, glm::vec2 vec) override;
    bool setUniform(const std::string &name, glm::vec3 vec) override;
	bool setUniform(const std::string &name, glm::vec4 vec) override;
    bool setUniform(const std::string &name, glm::mat3 mat) override;
    bool setUniform(const std::string &name, glm::mat4 mat) override;

	void bind(std::function<void()> const& action, bool save) const override;

private:

	void foreachStage(GLbitfield stages, std::function<void(unsigned)> action);

	std::array<std::shared_ptr<ShaderProgram>, 5> m_stages;

};

} // namespace plp


#endif