
#ifndef SHADEROBJECT_HPP
#define SHADEROBJECT_HPP

#include "ObjectGL.hpp"

#include <array>
#include <filesystem>
#include <optional>

namespace plp {

enum ShaderType {
	VERTEX_SHADER = GL_VERTEX_SHADER,
	TESS_CONTROL_SHADER = GL_TESS_CONTROL_SHADER,
	TESS_EVALUATION_SHADER = GL_TESS_EVALUATION_SHADER,
	GEOMETRY_SHADER = GL_GEOMETRY_SHADER,
	FRAGMENT_SHADER = GL_FRAGMENT_SHADER,
	COMPUTE_SHADER = GL_COMPUTE_SHADER
};

template<ShaderType T>
constexpr bool is_pipeline_shader = T != COMPUTE_SHADER;

class ShaderObject;
class ComputeShader;
class ShaderBuilder;

class AbstractShaderObject : public ObjectGL {

	friend ShaderBuilder;

protected:

	AbstractShaderObject(ShaderType type);

	AbstractShaderObject(AbstractShaderObject& other) = delete;
	AbstractShaderObject& operator=(AbstractShaderObject& other) = delete;

public:

	AbstractShaderObject(AbstractShaderObject&& other) = default;
	AbstractShaderObject& operator=(AbstractShaderObject&& other) = default;

	~AbstractShaderObject();

	[[nodiscard]] ShaderType getType() const;

	/** @brief Output formatted properties of the shader object to out
	 * @see https://registry.khronos.org/OpenGL-Refpages/es3.0/html/glGetShaderiv.xhtml
	 */
	void outputProperties(std::ostream& out) const;

	void outputInfoLog(std::ostream& out) const;

	static std::uint8_t getTypeBit(ShaderType type);

protected:

	[[nodiscard]] bool compile(std::string const& name, std::string_view const& source);

	[[nodiscard]] bool isCompiled() const;

protected:

	ShaderType type;
	std::filesystem::path filename;

};

class ShaderObject : public AbstractShaderObject
{

private:

	ShaderObject(ShaderType type) : AbstractShaderObject(type) {}

public:

	std::optional<ShaderObject>
	static createFromFile(ShaderType type, std::filesystem::path const &path);

	std::optional<ShaderObject>
	static createFromMemory(ShaderType type, std::string const &name, std::string_view const &source);
};

class ComputeShaderObject : public AbstractShaderObject
{

private:

	ComputeShaderObject() : AbstractShaderObject(COMPUTE_SHADER) {}

public:

	std::optional<ComputeShaderObject>
	static createFromFile(std::filesystem::path const &path);

	std::optional<ComputeShaderObject>
	static createFromMemory(std::string const &name, std::string_view const &source);

};

}

#endif