#ifndef RAWMODELBUILDER_HPP
#define RAWMODELBUILDER_HPP

#include "InstancedModel.hpp"

namespace plp
{

class ModelBuilder
{

public:

	ModelBuilder(std::size_t nbVerticesToDraw, PrimitiveType type = TRIANGLES, std::size_t patchSize = 0);

	bool addBuffer(StaticBufferStorage&& buffer, Attribute const& attrib, std::size_t offset = 0, unsigned int attribDivisor = 0);

	bool addBuffer(SharedBuffer const& buffer, Attribute const& attrib, std::size_t offset = 0, unsigned int attribDivisor = 0);

	bool addBuffer(StaticBufferStorage&& buffer, std::vector<Attribute> const& attribs, std::size_t offset = 0, unsigned int attribDivisor = 0);

	bool addBuffer(SharedBuffer const& buffer, std::vector<Attribute> const& attribs, std::size_t offset = 0, unsigned int attribDivisor = 0);

	void setElementBuffer(StaticBufferStorage&& buffer);

	void setElementBuffer(SharedBuffer buffer);

	void setBoundingBox(glm::vec3 center, glm::vec3 extent);

	void setName(std::string const& name);

	std::string getName() const;

	bool addTexture(std::shared_ptr<Texture> const& texture);

	std::optional<RawModel> create() const;

	std::optional<Model> create(std::shared_ptr<ShaderProgram> const& program, std::uint32_t instanceCount = 1) const;

	// std::optional<InstancedModel> create(std::shared_ptr<ShaderProgram> const& program, unsigned int instanceCount, unsigned swapBufferCount = 0, bool enableFrustumCulling = true);

	static std::optional<RawModel> create(
		std::size_t nbVerticesToDraw,
		RawModel::BindingPoint bindingPoints,
		SharedBuffer elements = nullptr,
		PrimitiveType type = TRIANGLES,
		std::size_t patchSize = 0
	);

	static std::optional<Model> create(
		std::shared_ptr<ShaderProgram> program,
		std::size_t nbVerticesToDraw,
		RawModel::BindingPoint bindingPoints,
		SharedBuffer elements = nullptr,
		PrimitiveType type = TRIANGLES,
		std::size_t patchSize = 0
	);

	void clear();
	
private:

	[[nodiscard]] bool checkAttribute(Attribute const& attr, unsigned int relativeOffset = 0);

	[[nodiscard]] bool checkBuffer(SharedBuffer const& buffer, GLuint bindingIndex, unsigned int stride);
	
	std::size_t nbVerticesToDraw;
	PrimitiveType type;
	std::size_t patchSize;

	std::vector<RawModel::BindingPoint> boundBuffers;
	std::set<unsigned int> usedAttributeIndices;
	SharedBuffer elements;
	Collection<Texture> textures;

	std::string name;
	
	glm::vec3 center;
	glm::vec3 extent;

};


} // namespace plp


#endif