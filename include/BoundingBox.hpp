#ifndef BOUNDINGBOX_HPP
#define BOUNDINGBOX_HPP

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

namespace plp
{

struct BoundingBox
{

	BoundingBox transform(glm::mat4 const& transform);

	glm::vec3 center;
	glm::vec3 extent;

};


} // namespace plp


#endif