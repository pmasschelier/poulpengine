#pragma once

// Project
#include "TransformFeedbackParticleSystem.hpp"

namespace plp {

class FireParticleSystem : public TransformFeedbackParticleSystem
{
public:
    FireParticleSystem(std::size_t maxParticleCount) : maxParticleCount(maxParticleCount) {}

    /**
     * Sets position boundaries of generated particles.
     * 
     * @param generatedPositionMin  Minimal possible position of generated particles
     * @param generatedPositionMax  Maximal possible position of generated particles
     */
    void setGeneratedPositionMinMax(const glm::vec3& generatedPositionMin, const glm::vec3& generatedPositionMax);

    /**
     * Sets velocity boundaries of generated particles.
     * 
     * @param generatedVelocityMin  Minimal possible velocity of generated particles
     * @param generatedVelocityMax  Maximal possible velocity of generated particles
     */
    void setGeneratedVelocityMinMax(const glm::vec3& generatedVelocityMin, const glm::vec3& generatedVelocityMax);
    
    /**
     * Sets lifetime boundaries of generated particles.
     * 
     * @param generatedLifetimeMin  Minimal possible lifetime of generated particles
     * @param generatedLifetimeMax  Maximal possible lifetime of generated particles
     */    
    void setGeneratedLifetimeMinMax(const float generatedLifetimeMin, const float generatedLifetimeMax);

    /**
     * Sets size boundaries of generated particles.
     * 
     * @param generatedSizeMin  Minimal possible size of generated particles
     * @param generatedSizeMax  Maximal possible size of generated particles
     */
    void setGeneratedSizeMinMax(const float generatedSizeMin, const float generatedSizeMax);

    /**
     * Gets generated particles color.
     */
    glm::vec3 getParticlesColor() const;

    virtual bool initialize();

protected:

    virtual void prepareRenderParticles();

private:

	const std::size_t maxParticleCount;

    glm::vec3 particlesColor_{ glm::vec3(0.83f, 0.435f, 0.0f) }; // Color of generated particles

    float remainingTimeToGenerateSeconds_{ 0.0f }; // How many seconds remain until next generation batch
    float generateEverySeconds_{ 0.05f }; // How often is one batch of particles generated (in seconds)
    int numParticlesToGenerate_{ 40 }; // Number of generated particles in one batch

    glm::vec3 generatedPositionMin_{ glm::vec3(-5.0f, 0.0f, -5.0f) }; // Minimal position of generated particles
    glm::vec3 generatedPositionRange_{ glm::vec3(10.0f, 0.0f, 10.0f) }; // Maximal position of generated particles

    glm::vec3 generatedVelocityMin_{ glm::vec3(-3, 0, -3) }; // Minimal velocity of generated particles
    glm::vec3 generatedVelocityRange_{ glm::vec3(6, 10, 6) }; // Maximal velocity of generated particles

    float generatedLifetimeMin_{ 1.5f }; // Minimal lifetime of generated particles
    float generatedLifetimeRange_{ 2.5f }; // Maximal lifetime of generated particles

    float generatedSizeMin_{ 0.4f }; // Minimal size of generated particles
    float generatedSizeRange_{ 0.75f }; // Maximal size of generated particles
};

} // namespace plp