#ifndef SKYBOX_HPP
#define SKYBOX_HPP

#include "Textures/CubemapTexture.hpp"
#include "Shaders/ShaderProgram.hpp"
#include "Cube.hpp"

namespace plp
{

class Skybox
{
public:
    /** @brief Constructeur par défaut
     * Crée une skybox sans texture (setCubemapTexture ou loadFromFiles doivent être appelées avant render)
     * @warning Ne doit pas être appelé avant la création du context opengl
     **/
    Skybox();

    void setCubemapTexture(std::shared_ptr<CubemapTexture> cubemapTex);

    bool loadFromFiles(std::array<std::filesystem::path, 6> const& filenames);

    void render() const;

private:
    std::shared_ptr<CubemapTexture> m_cubemapTex;
	
};
	
} // namespace plp


#endif

