#ifndef NAME_GENERATOR_HPP
#define NAME_GENERATOR_HPP

#include <cstdint>
#include <deque>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

namespace plp
{

class NameGenerator
{

	struct NameInfos
	{
		//NameInfos() : max_suffix(0) {}
		bool root_used{ true };
		std::uint32_t max_suffix{};
		std::deque<std::string> free_alt;
	};
	

public:

	std::vector<std::string> getUsedNames() const;

	std::uint32_t getMaxSuffix(std::string const& name) const;

	[[nodiscard]] bool addName(std::string const& name);

	std::string addNameWithSuffix(std::string const& name);

	bool free_name(std::string const& name);

private:

	std::string addNameWithSuffix_helper(std::string const& name, std::shared_ptr<NameInfos> infos);

	std::unordered_map<std::string, std::shared_ptr<NameInfos>> names;

};


} // namespace plp


#endif