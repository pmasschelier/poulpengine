#ifndef SHADOWMAP_HPP
#define SHADOWMAP_HPP

#include "Framebuffer.hpp"

#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>

namespace plp
{

class ShadowMap
{

public:

	ShadowMap(glm::uvec2 resolution);

private:

	std::optional<Framebuffer> fb;

};

	
} // namespace plp


#endif