#ifndef DYNAMICSTORAGE_HPP
#define DYNAMICSTORAGE_HPP

#include "MappedBufferStorage.hpp"

namespace plp
{

template<typename T, std::uint16_t F = BUFFER_WRITE_COHERENT>
class DynamicStorage : public MappedBufferStorage<T[], F>
{
	
public:

	using StaticBufferStorage::bind;
	using MappedBufferStorage<T[], F>::map;

	DynamicStorage(std::uint32_t initialCapacity) :
		MappedBufferStorage<T[], F>::MappedBufferStorage(initialCapacity),
		capacity(initialCapacity)
	{}

	DynamicStorage(DynamicStorage&& other) :
		MappedBufferStorage<T[], F>(std::forward<DynamicStorage>(other)),
		size(std::exchange(other.size, 0)),
		capacity(std::exchange(other.capacity, 0))
	{
	}

	DynamicStorage& operator=(DynamicStorage&& other)
	{
		MappedBufferStorage<T[], F>::operator=(std::forward<DynamicStorage>(other));
		size = std::exchange(other.size, 0);
		capacity = std::exchange(other.capacity, 0);
		return *this;
	}

	std::uint32_t create(std::uint32_t count)
	{
		if (size + count > capacity)
		{
			while (size + count > capacity)
				capacity <<= 1;
			reallocate();
		}
		std::uint32_t first = size;
		size += count;
		return first;
	}

	std::uint32_t getCapacity() const
	{
		return capacity;
	}

	std::uint32_t getSize() const
	{
		return size;
	}

protected:

	void reallocate()
	{
		DynamicStorage storage(capacity);
		storage.size = size;
		storage.copy(*this);
		*this = std::move(storage);
	}

private:

	std::uint32_t size{};
	std::uint32_t capacity;

};

} // namespace plp


#endif