#ifndef RENDERABLE_HPP
#define RENDERABLE_HPP

#include <cstdint>
#include <variant>

#include <glm/mat4x4.hpp>
#include <glm/mat3x3.hpp>

#include "DrawIndirectCommand.hpp"
#include "QueryObject.hpp"
#include "Shaders/AbstractShaderProgram.hpp"
#include "RawModel.hpp"
#include "MappedBufferStorage.hpp"

namespace plp
{

using DrawIndirectCommand = std::variant<DrawArraysIndirectCommand, DrawElementsIndirectCommand>;

class Renderable
{
	
public:

	static constexpr std::uint32_t PRIMITIVES_GENERATED = 1 << 0;
	static constexpr std::uint32_t TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN = 1 << 1;
	static constexpr std::uint32_t TRANSFORM_FEEDBACK_OVERFLOW = 1 << 2;
	static constexpr std::uint32_t TRANSFORM_FEEDBACK_STREAM_OVERFLOW = 1 << 3;
	static constexpr std::uint32_t SAMPLES_PASSED = 1 << 4;
	static constexpr std::uint32_t ANY_SAMPLES_PASSED = 1 << 5;
	static constexpr std::uint32_t ANY_SAMPLES_PASSED_CONSERVATIVE = 1 << 6;
	static constexpr std::uint32_t TIME_ELAPSED = 1 << 7;
	static constexpr std::uint32_t VERTICES_SUBMITTED = 1 << 8;
	static constexpr std::uint32_t PRIMITIVES_SUBMITTED = 1 << 9;
	static constexpr std::uint32_t VERTEX_SHADER_INVOCATIONS = 1 << 10;
	static constexpr std::uint32_t TESS_CONTROL_SHADER_PATCHES = 1 << 11;
	static constexpr std::uint32_t TESS_EVALUATION_SHADER_INVOCATIONS = 1 << 12;
	static constexpr std::uint32_t GEOMETRY_SHADER_INVOCATIONS = 1 << 13;
	static constexpr std::uint32_t GEOMETRY_SHADER_PRIMITIVES_EMITTED = 1 << 14;
	static constexpr std::uint32_t CLIPPING_INPUT_PRIMITIVES = 1 << 15;
	static constexpr std::uint32_t CLIPPING_OUTPUT_PRIMITIVES = 1 << 16;
	static constexpr std::uint32_t FRAGMENT_SHADER_INVOCATIONS = 1 << 17;
	static constexpr std::uint32_t COMPUTE_SHADER_INVOCATIONS = 1 << 18;

	struct QueryData {
		std::uint32_t primitives_generated;
		std::uint32_t transform_feedback_primitives_written;
		std::uint32_t transform_feedback_overflow;
		std::uint32_t transform_feedback_stream_overflow;
		std::uint32_t samples_passed;
		std::uint32_t any_samples_passed;
		std::uint32_t any_samples_passed_conservative;
		std::uint32_t time_elapsed;
		std::uint32_t vertices_submitted;
		std::uint32_t primitives_submitted;
		std::uint32_t vertex_shader_invocations;
		std::uint32_t tess_control_shader_patches;
		std::uint32_t tess_evaluation_shader_invocations;
		std::uint32_t geometry_shader_invocations;
		std::uint32_t geometry_shader_primitives_emitted;
		std::uint32_t clipping_input_primitives;
		std::uint32_t clipping_output_primitives;
		std::uint32_t fragment_shader_invocations;
		std::uint32_t compute_shader_invocations;
	};

	unsigned int getInstanceCount() const;

	const RawModel& getRawModel() const;

	virtual void render(const std::uint32_t queries, QueryData* data) const;

	DrawIndirectCommand createIndirectRenderingCommand() const;

protected:

	Renderable(RawModel&& rawmodel, std::shared_ptr<AbstractShaderProgram> program);

	Renderable(std::shared_ptr<const RawModel> rawmodel, std::shared_ptr<AbstractShaderProgram> program);

	virtual ~Renderable() = default;

	void setBaseInstance(unsigned int baseInstance);

	void setInstanceCount(unsigned int instanceCount);

	void render() const;

	void render(StaticBufferStorage& command) const;

protected:

	std::shared_ptr<AbstractShaderProgram> program;
	std::shared_ptr<const RawModel> rawmodel;

private:

	unsigned int instanceCount = 1;
	unsigned int baseInstance = 0;

};

} // namespace plp


#endif