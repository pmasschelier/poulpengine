#ifndef FRAMEBUFFER_HPP
#define FRAMEBUFFER_HPP

#include "ObjectGL.hpp"
#include "Textures/Texture.hpp"
#include "Utils.hpp"

#include <glm/vec4.hpp>

namespace plp
{

class FramebufferBuilder;

class Framebuffer : public ObjectGL
{

	friend FramebufferBuilder;

public:

	enum Attachment {
		COLOR = GL_COLOR,
		STENCIL = GL_STENCIL,
		DEPTH = GL_DEPTH
	};

public:

	Framebuffer();

public:

	~Framebuffer();

	Framebuffer(Framebuffer&&) = default;
	Framebuffer& operator=(Framebuffer&&) = default;

	bool clearColor(std::uint16_t attachmentIndex, glm::vec4 const& rgba);

	bool clearDepth(GLfloat value);

	bool clearStencil(GLint value);

	bool clearStencilAndDepth(GLfloat depthValue, GLuint stencilValue);

	static void bindReadBuffer(Framebuffer const& fb);

	static void bindDrawBuffer(Framebuffer const& fb);

	static GLuint getMaxColorAttachments();

	static GLuint getMaxFramebufferWith();

	static GLuint getMaxFramebufferHeight();

	static GLuint getMaxFramebufferLayers();

	static GLuint getMaxFramebufferSamples();

private:

	Collection<Texture> colorAttachments;
	std::shared_ptr<Texture> depthAttachment, stencilAttachment;

};
	
} // namespace plp


#endif