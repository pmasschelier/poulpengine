consteval std::array<glm::vec3, 32 * 16> getPatches() {
	constexpr std::size_t N = 4; // patch size
	glm::dvec3 p1[N*N], p2[N*N], p3[N*N], p4[N*N]; // patches
	std::array<glm::vec3, 32 * 16> patches;
	auto it = patches.begin();

	for (unsigned i = 0; i < INDICES.size(); i++) {
		for (unsigned j = 0; j < N; j++) {
			for (unsigned k = 0; k < N; k++) {
				glm::dvec3 const& cp1 = PATCH_DATA[INDICES[i][j * N + k]];
				glm::dvec3 const& cp2 = PATCH_DATA[INDICES[i][j * N + (3 - k)]];
				p1[j * N + k] = cp1;
				p2[j * N + k] = glm::dvec3(cp2.x, cp2.y, -cp2.z);
				if(i < 6) {
					p3[j * N + k] = glm::dvec3(-cp2.x, cp2.y, cp2.z);
					p4[j * N + k] = glm::dvec3(-cp1.x, cp1.y, -cp1.z);
				}
			}
		}
		it = std::copy(std::cbegin(p1), std::cend(p1), it);
		it = std::copy(std::cbegin(p2), std::cend(p2), it);
		if(i < 6) {
			it = std::copy(std::cbegin(p3), std::cend(p3), it);
			it = std::copy(std::cbegin(p4), std::cend(p4), it);
		}
	}
	
	return patches;
}

RawModel create() {
	auto teapot = getPatches();
	return ModelBuilder::create(teapot.size(), RawModel::BindingPoint { std::span(teapot), { POSITIONS_3D } }, nullptr, PATCHES, 16).value();
}