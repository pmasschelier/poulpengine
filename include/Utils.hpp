#pragma once

#include <iterator>
#include <filesystem>
#include <memory>
#include <numeric>
#include <optional>
#include <vector>

#include <GL/glew.h>

#define CASE_VALUE(a) case a: return #a

namespace plp
{

template <typename T>
using Opt = std::optional<std::shared_ptr<T>>;

template <typename T>
using Collection = std::vector<std::shared_ptr<T>>;

std::optional<std::string> readFile(std::filesystem::path const &path);

} // namespace plp

