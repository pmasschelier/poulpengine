#ifndef STATIC_BUFFER_STORAGE_HPP
#define STATIC_BUFFER_STORAGE_HPP

#include "ObjectGL.hpp"

#include <cassert>
#include <functional>
#include <iostream>
#include <memory>
#include <optional>
#include <span>
#include <stdexcept>
#include <utility>
#include <vector>

namespace plp
{

enum BufferObjectType
{
	ARRAY_BUFFER = GL_ARRAY_BUFFER,
	ELEMENT_ARRAY_BUFFER = GL_ELEMENT_ARRAY_BUFFER,
	COPY_READ_BUFFER = GL_COPY_READ_BUFFER,
	COPY_WRITE_BUFFER = GL_COPY_WRITE_BUFFER,
	PIXEL_UNPACK_BUFFER = GL_PIXEL_UNPACK_BUFFER,
	PIXEL_PACK_BUFFER = GL_PIXEL_PACK_BUFFER,
	QUERY_BUFFER = GL_QUERY_BUFFER,
	TEXTURE_BUFFER = GL_TEXTURE_BUFFER,
	TRANSFORM_FEEDBACK_BUFFER = GL_TRANSFORM_FEEDBACK_BUFFER,
	UNIFORM_BUFFER = GL_UNIFORM_BUFFER,
	DRAW_INDIRECT_BUFFER = GL_DRAW_INDIRECT_BUFFER,
	ATOMIC_COUNTER_BUFFER = GL_ATOMIC_COUNTER_BUFFER,
	DISPATCH_INDIRECT_BUFFER = GL_DISPATCH_INDIRECT_BUFFER,
	SHADER_STORAGE_BUFFER = GL_SHADER_STORAGE_BUFFER
};

template <BufferObjectType K>
constexpr bool is_indexed_target = K == ATOMIC_COUNTER_BUFFER || K == TRANSFORM_FEEDBACK_BUFFER || K == UNIFORM_BUFFER || K == SHADER_STORAGE_BUFFER;

// -------------- STORAGE SPECIFIC FLAGS --------------
constexpr std::uint16_t DYNAMIC_STORAGE_BIT = GL_DYNAMIC_STORAGE_BIT;
constexpr std::uint16_t CLIENT_STORAGE_BIT = GL_CLIENT_STORAGE_BIT;

// -------------- MAPPING SPECIFIC FLAGS --------------
constexpr std::uint16_t MAP_INVALIDATE_RANGE_BIT = GL_MAP_INVALIDATE_RANGE_BIT;
constexpr std::uint16_t MAP_INVALIDATE_BUFFER_BIT = GL_MAP_INVALIDATE_BUFFER_BIT;
constexpr std::uint16_t MAP_FLUSH_EXPLICIT_BIT = GL_MAP_FLUSH_EXPLICIT_BIT;
constexpr std::uint16_t MAP_UNSYNCHRONIZED_BIT = GL_MAP_UNSYNCHRONIZED_BIT;

// -------------- COMMON FLAGS --------------
constexpr std::uint16_t MAP_READ_BIT = GL_MAP_READ_BIT;
constexpr std::uint16_t MAP_WRITE_BIT = GL_MAP_WRITE_BIT;
constexpr std::uint16_t MAP_PERSISTENT_BIT = GL_MAP_PERSISTENT_BIT;
constexpr std::uint16_t MAP_COHERENT_BIT = GL_MAP_COHERENT_BIT;

constexpr std::uint16_t STORAGE_FLAGS_MASK =
	MAP_READ_BIT | MAP_WRITE_BIT | MAP_PERSISTENT_BIT | MAP_COHERENT_BIT | DYNAMIC_STORAGE_BIT | CLIENT_STORAGE_BIT;
constexpr std::uint16_t MAPPING_FLAGS_MASK = MAP_READ_BIT | MAP_WRITE_BIT | MAP_PERSISTENT_BIT | MAP_COHERENT_BIT | MAP_INVALIDATE_RANGE_BIT |
											 MAP_INVALIDATE_BUFFER_BIT | MAP_FLUSH_EXPLICIT_BIT | MAP_UNSYNCHRONIZED_BIT;
constexpr std::uint16_t BUFFER_WRITE_COHERENT = MAP_WRITE_BIT | MAP_PERSISTENT_BIT | MAP_COHERENT_BIT;
constexpr std::uint16_t BUFFER_READ_COHERENT = MAP_READ_BIT | MAP_PERSISTENT_BIT | MAP_COHERENT_BIT;

template <std::uint16_t FLAGS> constexpr bool dynamic_flags = (FLAGS & DYNAMIC_STORAGE_BIT);

template <std::uint16_t FLAGS> constexpr bool persistent_flags = (FLAGS & MAP_PERSISTENT_BIT);

template <std::uint16_t FLAGS>
constexpr bool coherent_storage_flags =
	((FLAGS & (MAP_READ_BIT | MAP_WRITE_BIT) || !(FLAGS & MAP_PERSISTENT_BIT))) && // On the one hand we need R/W flags to have persistence
	(FLAGS & MAP_PERSISTENT_BIT || !(FLAGS & MAP_COHERENT_BIT));				   // On the other hand we need persistence to have coherence

template <std::uint16_t FLAGS>
constexpr bool flags_allow_mapping =
	(FLAGS & (MAP_READ_BIT | MAP_WRITE_BIT)) && // MAP_READ_BIT or MAP_WRITE_BIT should be set
	!(FLAGS & MAP_READ_BIT && (FLAGS & (MAP_INVALIDATE_RANGE_BIT | MAP_INVALIDATE_BUFFER_BIT | MAP_UNSYNCHRONIZED_BIT))) && // if M
	(FLAGS & MAP_WRITE_BIT || !(FLAGS & MAP_FLUSH_EXPLICIT_BIT));

template <std::uint16_t FLAGS> constexpr bool flags_allow_writing = FLAGS & MAP_WRITE_BIT;
template <std::uint16_t FLAGS> constexpr bool flags_allow_reading = FLAGS & MAP_READ_BIT;

class RawModel;

class StaticBufferStorage : public ObjectGL
{

	friend RawModel;

protected:

	StaticBufferStorage() = default;

public:

	template <typename T, std::size_t N>
	StaticBufferStorage(std::span<T, N> const &data)
	{
		create(data.size_bytes(), data.data());
	}

	StaticBufferStorage(std::size_t sizeBytes)
	{
		create(sizeBytes, nullptr);
	}

	template <typename T, std::size_t N>
	StaticBufferStorage(std::size_t sizeBytes, std::span<T, N> const &data)
	{
		auto data_bytes = std::as_bytes(data);
		std::vector<std::byte> bytes(data_bytes.begin(), data_bytes.end());
		bytes.resize(sizeBytes);
		create(bytes.size(), bytes.data());
	}

protected:

	void create(GLsizeiptr sizeBytes, const void *data, std::uint16_t flags = 0)
	{
		glCreateBuffers(1, &id);
		glNamedBufferStorage(id, sizeBytes, data, flags & STORAGE_FLAGS_MASK);
		this->sizeBytes = sizeBytes;
	}

public:

	StaticBufferStorage(StaticBufferStorage &) = delete;
	StaticBufferStorage &operator=(StaticBufferStorage &) = delete;

	StaticBufferStorage(StaticBufferStorage &&other) = default;
	StaticBufferStorage &operator=(StaticBufferStorage &&other) = default;

	~StaticBufferStorage()
	{
		glDeleteBuffers(1, &id);
	}

	template <BufferObjectType K>
	void bind() const
	{
		glBindBuffer(K, id);
	}

	template <BufferObjectType K>
	void bind(GLuint index) const
		requires is_indexed_target<K>
	{
		glBindBufferBase(K, index, id);
	}

	template <BufferObjectType K>
	void bind(GLuint index, GLintptr offset, GLsizeiptr size) const
		requires is_indexed_target<K>
	{
		glBindBufferRange(K, index, id, offset, size);
	}

	void copy(StaticBufferStorage const& other) {
		glCopyNamedBufferSubData(other.id, id, 0, 0, std::min(sizeBytes, other.sizeBytes));
	}

	GLuint getID() const
	{
		return id;
	}

	std::size_t getSizeBytes() const
	{
		return sizeBytes;
	}

protected:

	std::size_t sizeBytes{};
};

using SharedBuffer = std::shared_ptr<const StaticBufferStorage>;

} // namespace plp

#endif
