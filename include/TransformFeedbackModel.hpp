#ifndef TRANSFORMFEEDBACKMODEL_HPP
#define TRANSFORMFEEDBACKMODEL_HPP

#include "Model.hpp"
#include "Shaders/TransformFeedbackShaderProgram.hpp"

namespace plp
{

class TransformFeedbackModel : public Renderable
{

public:
	
	TransformFeedbackModel(RawModel&& rawmodel, std::shared_ptr<TransformFeedbackShaderProgram> program, std::shared_ptr<StaticBufferStorage> writeBuffer = nullptr);

	const TransformFeedbackObject& getTransformFeedbackObject() const;

	void setWriteBuffer(std::shared_ptr<StaticBufferStorage> writeBuffer);

	void render(const std::uint32_t queries = 0, QueryData* data = nullptr) const override;

	TransformFeedbackObject swapTransformFeedbackObject();

private:

	TransformFeedbackShaderProgram* program;

	std::shared_ptr<StaticBufferStorage> writeBuffer;

	TransformFeedbackObject tf;
	
};

} // namespace plp


#endif