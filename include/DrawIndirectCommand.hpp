#ifndef DRAWINDIRECTCOMMAND_HPP
#define DRAWINDIRECTCOMMAND_HPP

#include <cstdint>

namespace plp
{

struct DrawArraysIndirectCommand
{
	std::uint32_t count = 0;
	std::uint32_t instanceCount = 1;
	std::uint32_t first = 0;
	std::uint32_t baseInstance = 0;
};

struct DrawElementsIndirectCommand
{
	std::uint32_t count = 0;
	std::uint32_t instanceCount = 1;
	std::uint32_t firstIndex = 0;
	std::uint32_t baseVertex = 0;
	std::uint32_t baseInstance = 0;
};

} // namespace plp

#endif