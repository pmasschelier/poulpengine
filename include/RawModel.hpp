#ifndef RAWMODEL_HPP
#define RAWMODEL_HPP

#include "Attribute.hpp"
#include "StaticBufferStorage.hpp"
#include "StreamingBufferObject.hpp"
#include "TransformFeedbackObject.hpp"
#include "Utils.hpp"

#include <cassert>
#include <iostream>
#include <map>
#include <set>

#include <glm/vec3.hpp>

namespace plp {

enum PrimitiveType {
	POINTS =  GL_POINTS,
	LINES =  GL_LINES,
	LINE_LOOP =  GL_LINE_LOOP,
	LINE_STRIP =  GL_LINE_STRIP,
	TRIANGLES =  GL_TRIANGLES,
	TRIANGLE_STRIP =  GL_TRIANGLE_STRIP,
	TRIANGLE_FAN =  GL_TRIANGLE_FAN,
	LINES_ADJACENCY =  GL_LINES_ADJACENCY,
	LINE_STRIP_ADJACENCY =  GL_LINE_STRIP_ADJACENCY,
	TRIANGLES_ADJACENCY =  GL_TRIANGLES_ADJACENCY,
	TRIANGLE_STRIP_ADJACENCY =  GL_TRIANGLE_STRIP_ADJACENCY,
	PATCHES = GL_PATCHES
};

class ModelBuilder;

class RawModel : public ObjectGL
{

	friend ModelBuilder;

	RawModel(std::uint32_t nbVerticesToDraw, PrimitiveType type = TRIANGLES, std::uint32_t patchSize = 0);

public:

	struct BindingPoint {

		BindingPoint(SharedBuffer buffer, std::vector<Attribute> const& attributes, std::size_t offset = 0, unsigned int attribDivisor = 0) :
			buffer(buffer),
			attributes(attributes),
			offset(offset),
			attribDivisor(attribDivisor) {}

		BindingPoint(StaticBufferStorage&& buffer, std::vector<Attribute> const& attributes, unsigned int attribDivisor = 0) : 
			BindingPoint(std::make_shared<StaticBufferStorage>(std::forward<StaticBufferStorage>(buffer)), attributes, attribDivisor) {}

		SharedBuffer buffer;
		std::vector<Attribute> attributes;
		std::uint32_t offset;
		unsigned int attribDivisor;
	};

	RawModel() = default;

	RawModel(RawModel&) = delete;
	RawModel& operator=(RawModel&) = delete;

	RawModel(RawModel&&) = default;
	RawModel& operator=(RawModel&&) = default;

	~RawModel();

	void setNbVerticesToDraw(std::uint32_t);

	std::uint32_t getNbVerticesToDraw() const;

	void setFirstVertex(std::uint32_t);

	std::uint32_t getFirstVertex() const;

	PrimitiveType getType() const;

	glm::vec3 getCenterAABB() const;

	glm::vec3 getExtentAABB() const;

	bool hasElementBuffer() const;

	void draw(StaticBufferStorage& command) const;

	void draw(std::uint32_t instanceCount = 1, unsigned int baseInstance = 0) const;

	void draw(TransformFeedbackObject const& tf) const;

	/**
	 * @brief Returns the maximum value for a vertex atribute's index
	 * The returned value will typically be 16
	 * @return Value of GL_MAX_VERTEX_ATTRIBS
	 */
	static GLuint getMaxVertexAttribs();

	static GLuint getMaxVertexAttribBindings();

	static GLuint getMaxVertexAttribStride();

	static GLuint getMaxVertexAttribRelativeOffset();

private:

	std::uint32_t nbVerticesToDraw;
	std::uint32_t first = 0;
	PrimitiveType type;
	std::uint32_t patchSize;

	glm::vec3 center;
	glm::vec3 extent;

	bool elementBuffer = false;
};

using SharedRawModel = std::shared_ptr<RawModel>;

}


#endif
