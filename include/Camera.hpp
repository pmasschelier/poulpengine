#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "MappedBufferStorage.hpp"

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include <numbers>

namespace plp {

struct CameraUniformData {
	glm::mat4 projection;
	glm::mat4 view;
	glm::mat4 projectionView;
	glm::mat4 inversedTransposedView;
	glm::vec3 position;
	float pad1;
	glm::vec3 billboardHorizontalVector;
	float pad2;
	glm::vec3 billboardVerticalVector;
	float pad3;
	
	struct Frustum {
		glm::vec4 topPlane;
		glm::vec4 bottomPlane;

		glm::vec4 rightPlane;
		glm::vec4 leftPlane;
		
		glm::vec4 farPlane;
		glm::vec4 nearPlane;
	} frustum;
};

class Camera : public CameraUniformData
{
public:

	enum Movement {
		LEFT = 1 << 0,
		RIGHT = 1 << 1,
		UP = 1 << 2,
		DOWN = 1 << 3,
		FORWARD = 1 << 4,
		BACKWARD = 1 << 5
	};

    Camera(
		glm::vec3 const& position = glm::vec3(0, 0, 0), 
        float horizontalAngle = 0.f,
        float verticalAngle = 0.f,
        float ratio = 4.0f / 3.0f,
        float FoV = 45.f,
        float speed = 3.f,
        float mouseSpeed = 0.002f,
        float nearClipping = 0.1f, 
        float farClipping = 100.0f
	);

    // glm::mat4 update(Input const& in, sf::Time const& elapsedTime);

    glm::mat4 setPosition(glm::vec3 const& position);
    glm::vec3 getPosition() const;

	glm::vec3 getDirection() const;
	glm::quat getRotation() const;

    glm::mat4 move(glm::vec3 const& displacement);
	glm::mat4 move(Movement movement, float elaspedTimeSec);

	glm::mat4 moveView(glm::vec2 const& euler);
	// glm::mat4 moveView(glm::vec2 const mouseDisplacement, float elaspedTimeSec);

    glm::mat4 setTarget(glm::vec3 const& target);

    glm::mat4 setFov(float FoV);
    float getFov() const;

    void setSpeed(float speed);
    float getSpeed() const;

    void setMouseSpeed(float mouseSpeed);
    float getMouseSpeed() const;

    glm::mat4 setRatio(float ratio);
    float getRatio() const;

    glm::mat4 setNearClipping(float near);
    float getNearClipping() const;

    glm::mat4 setFarClipping(float far);
    float getFarClipping() const;

    glm::mat4 getViewMatrix() const;
    glm::mat4 getProjectionMatrix() const;
    glm::mat4 getProjectionViewMatrix() const;

	std::pair<glm::vec3, glm::vec3> computeBillboardingVectors() const;

	void copyFrustum(Camera const& other);

	void activate() const;

private:

	CameraUniformData::Frustum computeFrustum() const;

    glm::mat4 updateProjection();
    glm::mat4 updateView();
    void updateDirectionRightUp();

    glm::vec3 m_direction;
    glm::vec3 m_right;
    glm::vec3 m_up;
    // angle horizontal = 0.f -> regard vers +z
    float m_horizontalAngle = 3.14f;
    // angle vertical = 0.f -> regard vers l'horizon
    float m_verticalAngle;
    float m_FOV;

    float m_speed; // unités/s
    float m_mouseSpeed; // = 0.005f

    float m_ratio;
    float m_nearClipping, m_farClipping;

	using CameraUniformBlock = MappedBufferStorage<CameraUniformData, BUFFER_WRITE_COHERENT>;
	CameraUniformBlock ubo;
	const CameraUniformBlock::MappedPtr& mappedUBO;
};

}

#endif

