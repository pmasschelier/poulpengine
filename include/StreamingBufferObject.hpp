#ifndef STREAMING_BUFFER_OBJECT_HPP
#define STREAMING_BUFFER_OBJECT_HPP

#include "StaticBufferStorage.hpp"

namespace plp
{

class StreamingBufferObject : public StaticBufferStorage
{

public:
	
	StreamingBufferObject()
	{
		glCreateBuffers(1, &id);
	}

	~StreamingBufferObject()
	{
		glDeleteBuffers(1, &id);
	}

	StreamingBufferObject(StreamingBufferObject&) = delete;
	StreamingBufferObject& operator=(StreamingBufferObject&) = delete;

	StreamingBufferObject(StreamingBufferObject&&) = default;
	StreamingBufferObject& operator=(StreamingBufferObject&&) = default; 

	template<typename T, std::size_t N>
	void update(std::span<T, N> const& data)
	{
		glNamedBufferData(id, data.size_bytes(), nullptr, GL_STREAM_DRAW);
		glNamedBufferSubData(id, 0, data.size_bytes(), data.data());
		sizeBytes = data.size_bytes();
	}

};

} // namespace plp




#endif