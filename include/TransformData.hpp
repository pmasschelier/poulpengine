#ifndef TRANSFORMDATA_HPP
#define TRANSFORMDATA_HPP

namespace plp
{

struct LocalTransformData
{
	std::uint32_t parentIndex;
	std::uint32_t boundingBoxIndex;
	std::uint32_t shouldUpdate;
	std::uint32_t shouldRender;
	glm::mat4 model;
};

struct GlobalTransformData
{
	glm::mat4 model;
	glm::mat4 inversedTransposedModel;
};

} // namespace plp


#endif