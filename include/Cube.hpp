#ifndef CUBE_HPP
#define CUBE_HPP

#include <array>
#include <memory>

#include "PoulpEngine.hpp"
#include "RawModel.hpp"
#include "ModelBuilder.hpp"

namespace plp {

namespace Cube {

/* ordre : +x, -x, +y, -y, +z, -z, le premier triangle est toujours celui situé le plus vers -x, -y 
 *  les triangles sont tracés sans passer par l'hypothénus et dans le sens trigo.
 *  on a touours un triangle en bas à droite et un triangle en haut à gauche
 */

constexpr std::array<float, 108> CUBE = 
{
	1.0, -1.0, 1.0,     1.0, -1.0, -1.0,     1.0, 1.0, -1.0,		// Face gauche (+x)
	1.0, 1.0, -1.0,		1.0, 1.0, 1.0,       1.0, -1.0, 1.0,		// Face gauche (+x)

	-1.0, -1.0, -1.0,	-1.0, -1.0, 1.0,	-1.0, 1.0, 1.0,			// Face droite (-x)
	-1.0, 1.0, 1.0,		-1.0, 1.0, -1.0,    -1.0, -1.0, -1.0, 		// Face droite (-x)

	1.0, 1.0, -1.0,     -1.0, 1.0, -1.0,     -1.0, 1.0, 1.0, 		// Face dessus (+y)
	-1.0, 1.0, 1.0,		1.0, 1.0, 1.0,		1.0, 1.0, -1.0,			// Face dessus (+y)

	-1.0, -1.0, 1.0,   -1.0, -1.0, -1.0,	1.0, -1.0, -1.0,		// Face dessous (-y)
	1.0, -1.0, -1.0,	1.0, -1.0, 1.0,     -1.0, -1.0, 1.0,	    // Face dessous (-y)

	-1.0, -1.0, 1.0,	1.0, -1.0, 1.0,		1.0, 1.0, 1.0,			// Face arrière (+z)
	1.0, 1.0, 1.0,		-1.0, 1.0, 1.0,     -1.0, -1.0, 1.0,  		// Face arrière (+z)

	1.0, -1.0, -1.0,	-1.0, -1.0, -1.0,   -1.0, 1.0, -1.0,        // Face avant (-z)
	-1.0, 1.0, -1.0,    1.0, 1.0, -1.0,     1.0, -1.0, -1.0         // Face avant (-z)
};


constexpr std::array<float, 72> VERTICES =
{
	1.0, -1.0, 1.0,     1.0, -1.0, -1.0,     1.0, 1.0, -1.0,	1.0, 1.0, 1.0,		// Face gauche (+x)
	-1.0, -1.0, -1.0,	-1.0, -1.0, 1.0,	-1.0, 1.0, 1.0,		-1.0, 1.0, -1.0,	// Face droite (-x)
	1.0, 1.0, -1.0,     -1.0, 1.0, -1.0,     -1.0, 1.0, 1.0,	1.0, 1.0, 1.0, 		// Face dessus (+y)
	-1.0, -1.0, 1.0,   -1.0, -1.0, -1.0,	1.0, -1.0, -1.0,	1.0, -1.0, 1.0,		// Face dessous (-y)
	-1.0, -1.0, 1.0,	1.0, -1.0, 1.0,		1.0, 1.0, 1.0,		-1.0, 1.0, 1.0,		// Face arrière (+z)
	1.0, -1.0, -1.0,	-1.0, -1.0, -1.0,   -1.0, 1.0, -1.0,	1.0, 1.0, -1.0		// Face avant (-z)
};

constexpr std::array<float, 72> NORMALS =
{
	1., 0., 0., 	1., 0., 0., 	1., 0., 0., 	1., 0., 0., 	// Face gauche (+x)
	-1., 0., 0.,	-1., 0., 0.,	-1., 0., 0.,	-1., 0., 0.,	// Face droite (-x)
	0., 1., 0.,		0., 1., 0.,		0., 1., 0.,		0., 1., 0.,		// Face dessus (+y)
	0., -1., 0.,	0., -1., 0.,	0., -1., 0.,	0., -1., 0.,	// Face dessous (-y)
	0., 0., 1.,		0., 0., 1.,		0., 0., 1.,		0., 0., 1.,		// Face arrière (+z)
	0., 0., -1.,	0., 0., -1.,	0., 0., -1.,	0., 0., -1.		// Face avant (-z)
};

constexpr std::array<unsigned int, 36> INDICES =
{
	0, 1, 2,		2, 3, 0,
	4, 5, 6,		6, 7, 4,
	8, 9, 10,		10, 11, 8,
	12, 13, 14,		14, 15, 12,
	16, 17, 18,		18, 19, 16,
	20, 21, 22,		22, 23, 20
};

SharedBuffer getCubeVBO();
SharedBuffer getVerticesBO();
SharedBuffer getNormalsBO();
SharedBuffer getElementsBO();

std::array<float, 72> computeColorsByFace(std::array<glm::vec3, 6> const& colors);

/*
	*	Ex: Cube::computeTexCoords(3, 1, {1, 1, 2, 2, 0, 0});
	*/
std::array<float, 48> computeTexCoords(unsigned width, unsigned height, std::array<unsigned, 6> const& index);

ModelBuilder createColoredCube(std::array<glm::vec3, 6> const& colors = {{
	FloatColor::Red,
	FloatColor::Green,
	FloatColor::Blue,
	FloatColor::Red,
	FloatColor::Green,
	FloatColor::Blue
}});

ModelBuilder createTexturedCube(unsigned widthTex, unsigned heightTex, std::array<unsigned, 6> const& index);

SharedRawModel createNudeCube();

};

}

#endif

