#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>

#include "SFML-based/Image.hpp"

namespace plp {

bool init();

void clear(glm::vec4 color);

void setViewPort(glm::uvec4 const& viewport);

void setWireframeMode(bool activate = true);

bool screenshot(glm::uvec2 wsize, Image& screen);

}
