#ifndef CUBEMAPTEXTURE_HPP
#define CUBEMAPTEXTURE_HPP

#include "Textures/Texture2DStorage.hpp"

#include <array>
#include <filesystem>
#include <optional>

namespace plp
{

class CubemapTexture : public Texture2DStorage
{

protected:

	CubemapTexture();

public:

	CubemapTexture(CubemapTexture&&) = default;
	CubemapTexture& operator=(CubemapTexture&&) = default;

    /// @brief Charge la cubemap texture depuis 6 fichiers image
    /// @param filenames chemin vers les images dans l'ordre : +x, -x, +y, -y, +z, -z
    /// @return true ssi tout s'est bien passé
    std::optional<CubemapTexture> 
	static createFromFiles(std::array<std::filesystem::path, 6> const& filenames);

    /// @brief Charge la cubemap texture depuis un tileset
    /// @param widthTex nombre de tiles en largeur
    /// @param heightTex nombre de tiles en hauteur
    /// @param index index dans le tileset (dans le sens de lecture) des images dans l'ordre : +x, -x, +y, -y, +z, -z
    /// @param filename chemin vers le tileset
    /// @return true ssi tout s'est bien passé
    std::optional<CubemapTexture>
	static createFromTileMap(
		unsigned widthTex,
		unsigned heightTex,
		std::array<unsigned, 6> const& index,
		std::filesystem::path const& filename
	);

};


} // namespace plp


#endif
