#ifndef TEXTURE_BINDER_HPP
#define TEXTURE_BINDER_HPP

#include "Texture.hpp"

namespace plp
{

class TextureBinder
{
private:
	Texture const& tex;
	GLuint unit;
	GLuint resetValue;
public:
	TextureBinder(Texture const& tex, GLuint unit = 0, bool save = false) : tex(tex), unit(unit)
	{
		resetValue = tex.bind(unit, save);
	}
	
	~TextureBinder()
	{
		tex.unbind(unit, resetValue);
	}

	TextureBinder(const TextureBinder&) = delete;
	TextureBinder& operator=(const TextureBinder&) = delete;
};
	
} // namespace plp



#endif