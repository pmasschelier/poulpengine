#ifndef TEXTURE2DSTORAGE_HPP
#define TEXTURE2DSTORAGE_HPP

#include "Textures/Texture.hpp"

#include <glm/vec2.hpp>

#include <iostream>

namespace plp
{

class Texture2DStorage : public Texture
{

protected:

	////////////////////////////////////////////////////////////
    /// \brief Default constructor
    ///
	/// Does nothing and can't be called from outside
    ///
    ////////////////////////////////////////////////////////////
    Texture2DStorage(Texture::Target target, Texture::SizedInternalFormat format = RGB8)
		: Texture(target, format) {}

    ////////////////////////////////////////////////////////////
    /// \brief Create the texture
    ///
    /// If this function fails, the texture is left unchanged.
    ///
    /// \param size Width and height of the texture
    ///
    /// \return True if creation was successful
    ///
    ////////////////////////////////////////////////////////////
    [[nodiscard]] bool create(const glm::uvec2& size);

public:

	////////////////////////////////////////////////////////////
    /// \brief Return the size of the texture
    ///
    /// \return Size in pixels
    ///
    ////////////////////////////////////////////////////////////
    [[nodiscard]] glm::uvec2 getSize() const;

	////////////////////////////////////////////////////////////
    /// \brief Swap the contents of this texture with those of another
    ///
    /// \param right Instance to swap with
    ///
    ////////////////////////////////////////////////////////////
    void swap(Texture2DStorage& right) noexcept;

protected:

	void updateWrapProperty() override;

protected:

    glm::uvec2 m_size; //!< Public texture size

};

} // namespace plp


#endif
