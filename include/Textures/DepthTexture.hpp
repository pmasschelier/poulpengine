#ifndef DEPTHTEXTURE_HPP
#define DEPTHTEXTURE_HPP

#include "Textures/Texture2DStorage.hpp"

#include <optional>

namespace plp
{

class DepthTexture : public Texture2DStorage
{

protected:

	DepthTexture(Target target, SizedInternalFormat format)
		: Texture2DStorage(target, format) {}

public:
	
	[[nodiscard]] std::optional<DepthTexture>
	static createEmpty(glm::uvec2 size);

};

} // namespace plp


#endif