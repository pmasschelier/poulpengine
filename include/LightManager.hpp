#ifndef LIGHTMANAGER_HPP
#define LIGHTMANAGER_HPP

#include <glm/glm.hpp>
#include <GL/glew.h>

#include <vector>

#include "MappedBufferStorage.hpp"

namespace plp
{

class LightManager
{
	
public:

	static const std::size_t MAX_LIGHT_NUMBER = 32;

	enum LightType : std::uint32_t {
		POINT,
		DIRECTIONAL,
		SPOT
	};

	struct Light {
		LightType type;
		// ATTENUATION
		float constant = 1.f;
		float linear = 0.f;
		float quadratic = 0.f;
		glm::vec4 position{};
		glm::vec4 direction{};
		glm::vec4 color{1.f};
	};

	struct LightsUniformData {
		uint32_t nb_lights = 0;
		uint32_t active = 0;
		uint32_t padding[2];
		Light lights[MAX_LIGHT_NUMBER];
	};

	LightManager();

	bool addLight(Light const& light);

	void setActive(bool active = true);

private:

	std::vector<Light> lights;

	using LightManagerUniformBlock = MappedBufferStorage<LightsUniformData, BUFFER_WRITE_COHERENT>;
	LightManagerUniformBlock ubo;
	const LightManagerUniformBlock::MappedPtr& mappedUBO;
};

} // namespace plp


#endif