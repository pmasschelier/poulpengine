#ifndef OBJECTGL_HPP
#define OBJECTGL_HPP

#include <GL/glew.h>

#include <utility>

namespace plp
{

class ObjectGL {

public:

	ObjectGL(ObjectGL&) = delete;
	ObjectGL& operator=(ObjectGL&) = delete;

	ObjectGL(ObjectGL&& other) : id(std::exchange(other.id, 0))
	{}

	ObjectGL& operator=(ObjectGL&& other)
	{
		id = std::exchange(other.id, 0);
		return *this;
	}

	virtual ~ObjectGL() = default;

	GLuint getID() const {
		return id;
	}

protected:

	ObjectGL() = default;

	GLuint id{};

};

} // namespace plp


#endif