#ifndef INSTANCEDMODEL_HPP
#define INSTANCEDMODEL_HPP

#include "DrawIndirectCommand.hpp"
#include "Model.hpp"
#include "TransformFeedbackModel.hpp"
#include "MappedBufferStorage.hpp"

namespace plp
{

class InstancedModel : public Model
{

public:

	struct TransformData {
		glm::mat4 model;
		glm::mat3 inversedTransposedModel;
	};

	using TranformationBufferType = MappedBufferStorage<TransformData[]>;
	
	InstancedModel(
		Model &&model,
		std::shared_ptr<TranformationBufferType> const& instancedBuffer,
		unsigned int instanceCount,
		unsigned int swapBufferCount = 0
	);

	InstancedModel(
		Model &&model,
		std::shared_ptr<TranformationBufferType> const& instancedBuffer,
		TransformFeedbackModel &&cullingModel,
		unsigned int instanceCount,
		unsigned int swapBufferCount = 0
	);

	void update();

	void render(const std::uint32_t queries = 0, QueryData* data = nullptr) const override;

private:

	unsigned int getWriteBufferIndex() const;
	
	unsigned int getReadBufferIndex() const;

	// DrawElementsIndirectCommand drawCommand;

	// StaticBufferStorage drawBuffer;

	std::unique_ptr<TransformFeedbackModel> cullingModel;

	std::shared_ptr<TranformationBufferType> instancedBuffer;
	const TranformationBufferType::MappedPtr& transformations;

	std::shared_ptr<StaticBufferStorage> tfBuffer;

	unsigned int swapBufferCount;

	long long swapCount = 0;

};

	
} // namespace plp


#endif
