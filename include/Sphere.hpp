#ifndef SPHERE_HPP
#define SPHERE_HPP

#include <RawModel.hpp>
#include "ModelBuilder.hpp"

#include <vector>

namespace plp
{

namespace Sphere
{

void computeVertices(
    unsigned short meridians,
    unsigned short parallels,
    std::vector<float>& vertices,
    std::vector<unsigned int>& elements,
    std::vector<float>& uvs);

ModelBuilder createTexturedSphere(unsigned short meridians, unsigned short parallels);

} // namespace Sphere


} // namespace plp


#endif