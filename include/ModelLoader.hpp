#ifndef MODELLOADER_HPP
#define MODELLOADER_HPP

#include "SceneNode.hpp"
#include "Utils.hpp"
#include "Model.hpp"

namespace plp
{

class SceneManager;

class ModelLoader
{

private:

	static std::shared_ptr<ShaderProgram> basicModelShader;

public:

	ModelLoader() = default;

	[[nodiscard]] Opt<SceneNode> 
	static loadSceneFromFile(SceneManager &scene, std::filesystem::path const &path);

	[[nodiscard]] std::optional<std::vector<ModelBuilder>>
	static loadModelsFromFile(SceneManager &scene, std::filesystem::path const &path);

	[[nodiscard]] std::optional<ModelBuilder>
	static loadNthModelFromFile(SceneManager &scene, std::filesystem::path const &path, unsigned int index);

	static std::shared_ptr<ShaderProgram> getBasicModelShader();

};

} // namespace plp


#endif