#ifndef PLANE_HPP
#define PLANE_HPP

#include <array>

#include "ModelBuilder.hpp"

namespace plp
{

namespace Plane
{

constexpr std::array<float, 12> VERTICES = {
	0.0, 0.0, 0.0, 	0.0, 0.0, 1.0,	1.0, 0.0, 1.0, 	1.0, 0.0, 0.0
};

constexpr std::array<float, 12> NORMALS = {
	0.0, 1.0, 0.0,	0.0, 1.0, 0.0,	0.0, 1.0, 0.0,	0.0, 1.0, 0.0,
};

constexpr std::array<float, 8> UVS = {
	1.0, 0.0,	1.0, 1.0, 	0.0, 1.0,	0.0, 0.0
};

constexpr std::array<unsigned int, 6> INDICES = {
	0, 1, 3,	1, 2, 3
};

SharedBuffer getVerticesBO();
SharedBuffer getNormalsBO();
SharedBuffer getElementsBO();
SharedBuffer getTexcoordsBO();

SharedRawModel getTexturedPlane();

} // namespace Plane



} // namespace plp


#endif