#ifndef MAPPEDBUFFERSTORAGE_HPP
#define MAPPEDBUFFERSTORAGE_HPP

#include "StaticBufferStorage.hpp"
#include <iostream>

namespace plp
{

template<typename S = std::byte[], std::uint16_t F = BUFFER_WRITE_COHERENT>
requires coherent_storage_flags<F> && flags_allow_mapping<F>
class MappedBufferStorage : public StaticBufferStorage
{

public:

	MappedBufferStorage() requires(!std::is_array_v<S>)
	{
		std::cout << "CREATE SCALAR MappedBufferStorage of size " << sizeof(S) << std::endl;
		create(sizeof(S), nullptr, F);
	}

	MappedBufferStorage(std::size_t size) requires(std::is_array_v<S>)
	{
		std::cout << "CREATE ARRAY MappedBufferStorage of size " << size * sizeof(std::remove_extent_t<S>) << "\t( " << size << " elements)" << std::endl;
		create(size * sizeof(std::remove_extent_t<S>), nullptr, F);
	}

	MappedBufferStorage(MappedBufferStorage&& other) :
		StaticBufferStorage(std::forward<MappedBufferStorage>(other)),
		data(std::exchange(other.data, nullptr))
	{}

	MappedBufferStorage& operator=(MappedBufferStorage&& other)
	{
		StaticBufferStorage::operator=(std::forward<MappedBufferStorage>(other));
		data = std::exchange(other.data, nullptr);
		return *this;
	}

	~MappedBufferStorage() = default;

	class UnMapper {
	
		GLuint id;

	public:

		UnMapper(GLuint id = 0) : id(id) {}

		UnMapper(UnMapper&) = delete;
		UnMapper& operator=(UnMapper&) = delete;

		UnMapper(UnMapper&& other) = default;
		UnMapper& operator=(UnMapper&&) = default;

		void swap(UnMapper& other)
		{
			std::swap(id, other.id);
		}
	
		void operator()(std::remove_extent_t<S>*) {
			if(glIsBuffer(id))
				glUnmapNamedBuffer(id);
		}
	};

	using ConstMappedPtr = std::unique_ptr<const S, UnMapper>;

	const ConstMappedPtr& map() requires (!flags_allow_writing<F>) {
		if(!data)
			mapBuffer();
		assert(data);
		return data;
	}

	using MappedPtr = std::unique_ptr<S, UnMapper>;
	
	const MappedPtr& map() requires flags_allow_writing<F> {
		if(!data)
			mapBuffer();
		assert(data);
		return data;
	}

protected:

	void mapBuffer() {
		void* void_ptr = glMapNamedBufferRange(id, 0, sizeBytes, F & MAPPING_FLAGS_MASK);
		MappedPtr ptr (
			reinterpret_cast<std::remove_extent_t<S>*>(void_ptr),
			UnMapper(id)
		);
		data.swap(ptr);
	}

	MappedPtr data = nullptr;

};




} // namespace plp

/* 
template<typename S>
class Mapper : public ObjectGL {

public:

	template<std::uint16_t F>
	Mapper(MappedBufferStorage<F>& buffer)
	{
		void* void_ptr = glMapNamedBufferRange(buffer.id, 0, buffer.getSizeBytes(), F & MAPPING_FLAGS_MASK);
		ptr = reinterpret_cast<std::remove_extent<S>*>(void_ptr);
		id = buffer.id;
	}

	~Mapper()
	{
		if(valid)
			glUnmapNamedBuffer(id);
	}

	S& operator*()
	requires flags_allow_writing<F>
	{
		return *ptr;
	}

	const S& operator*() const
	requires (!flags_allow_writing<F>) && flags_allow_reading<F>
	{
		return *ptr;
	}

	S& operator[](std::uint32_t index)
	requires flags_allow_writing<F> && std::is_array<S>
	{
		return ptr[index];
	}

	const S& operator[](std::uint32_t index) const
	requires (!flags_allow_writing<F>) && flags_allow_reading<F> && std::is_array<S>
	{
		return ptr[index];
	}

private:

	bool valid = false;
	S* ptr;

}; */

#endif
