#ifndef TRANSFORMBUFFER_HPP
#define TRANSFORMBUFFER_HPP

#include "DynamicStorage.hpp"
#include "SceneNode.hpp"
#include "TransformData.hpp"

#include <list>

namespace plp
{

class TransformBuffer
{

	struct Command {
		DrawIndirectCommand command;
		glm::vec3 center;
		glm::vec3 extent;
	};

	struct Instance {
		static constexpr std::uint32_t 
		CASTER = 0x1, ACTIVE_CASTER = 0x2, RECEIVER = 0x4, ACTIVE_RECEIVER = 0x8;

		std::uint32_t transformIndex;
		std::uint32_t commandIndex;
		std::uint32_t status;
	};

public:

	TransformBuffer();

	std::uint32_t addCommand(
		Command const& command,
		bool caster = false,
		bool receiver = true
	);

	std::uint32_t addTransform(std::uint32_t count);
	bool updateTransform(SceneNode& node);

	void notifyUpdate(std::uint32_t transformId);

	const StaticBufferStorage& getBuffer() const;

	void bind(std::uint32_t baseInstance, std::uint32_t instanceCount) const;

	void computeCulling();

	void cullTransforms(
		std::shared_ptr<StaticBufferStorage>& culledTransforms,
		StaticBufferStorage& commandBuffer,
		GLintptr commandOffset,
		glm::vec3 center,
		glm::vec3 extent,
		std::uint32_t baseInstance,
		std::uint32_t instanceCount
	);

protected:

	// void createNewBuffers();

private:

	static constexpr unsigned int INITIAL_NODE_CAPACITY = 128;
	DynamicStorage<GlobalTransformData> models;
	DynamicStorage<Command> commands;
	DynamicStorage<Instance> instances;

	std::vector<bool> shouldUpdateTransform;
};

} // namespace plp

#endif
