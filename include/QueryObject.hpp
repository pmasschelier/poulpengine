#ifndef QUERY_OBJECT_HPP
#define QUERY_OBJECT_HPP

#include <functional>

#include "StaticBufferStorage.hpp"

namespace plp
{

enum QueryType {
	SAMPLES_PASSED = GL_SAMPLES_PASSED,
	ANY_SAMPLES_PASSED = GL_ANY_SAMPLES_PASSED,
	ANY_SAMPLES_PASSED_CONSERVATIVE = GL_ANY_SAMPLES_PASSED_CONSERVATIVE,
	TIME_ELAPSED = GL_TIME_ELAPSED,
	TIMESTAMP = GL_TIMESTAMP,
	PRIMITIVES_GENERATED = GL_PRIMITIVES_GENERATED,
	TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN = GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN
};

template<QueryType T>
class QueryObject : public ObjectGL
{

public:

	QueryObject()
	{
		glCreateQueries(T, 1, &id);
	}

	~QueryObject()
	{
		glDeleteQueries(1, &id);
	}

	QueryObject(QueryObject&&) = default;
	QueryObject& operator=(QueryObject&&) = default;

	void query(StaticBufferStorage& buffer, GLintptr offset, std::function<void()> action) const
	{
		glBeginQuery(T, id);
		action();
		glEndQuery(T);
		glGetQueryBufferObjectiv(id, buffer.getID(), GL_QUERY_RESULT, offset);
	}

	GLint querySync(std::function<void()> action) const
	{
		glBeginQuery(T, id);
		action();
		glEndQuery(T);
		GLint result{};
		glGetQueryObjectiv(id, GL_QUERY_RESULT, &result);
		return result;
	}

	GLint querySync(std::function<void()> action, GLuint index) const
	{
		static_assert(
			T == PRIMITIVES_GENERATED ||
			T == TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN,
			"Only PRIMITIVES_GENERATED and TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN queries can be indexed."
		);
		glBeginQueryIndexed(T, index, id);
		action();
		glEndQueryIndexed(T, index);
		GLint result{};
		glGetQueryObjectiv(id, GL_QUERY_RESULT, &result);
		return result;
	}
	
};

} // namespace plp

#endif