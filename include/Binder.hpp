#ifndef BINDER_HPP
#define BINDER_HPP

#include <functional>

namespace plp
{

class Bindable
{

public:

	virtual ~Bindable() = default;

	virtual void bind(std::function<void()> const& action, bool save = false) const = 0;

};

} // namespace plp


#endif