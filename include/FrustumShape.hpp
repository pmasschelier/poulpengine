#ifndef FRUSTUMSHAPE_HPP
#define FRUSTUMSHAPE_HPP

#include "ModelBuilder.hpp"

namespace plp
{

namespace FrustumShape
{

ModelBuilder create(float FoV, float ratio, float nearClipping, float farClipping);

} // namespace FrustumShape


} // namespace plp


#endif