#pragma once

// GLM
#include <glm/glm.hpp>

// Project
#include "Shaders/TransformFeedbackShaderProgram.hpp"
#include "Shaders/ShaderBuilder.hpp"
#include "Textures/Texture2D.hpp"
#include "TransformFeedbackObject.hpp"
#include "QueryObject.hpp"
#include "Camera.hpp"
#include "Attribute.hpp"
#include "RawModel.hpp"
#include "ModelBuilder.hpp"
#include "Renderable.hpp"

namespace plp
{

/**
 * Base class containing common methods / components of
 * a transform feedback based particle system.
 */
class TransformFeedbackParticleSystem
{
public:

	enum ParticleType
	{
		PARTICLE_TYPE_GENERATOR, // Represents generator particle
		PARTICLE_TYPE_NORMAL	 // Represents normal (non-generator) particle
	};

	struct RecordedAttribute : public Attribute
	{
		std::string name;
		bool neededForRendering;
	};

	struct Param
	{
		std::string name;
		Attribute::Type type;
		Attribute::Width width;
		std::size_t offset{};

		Attribute toAttribute() const {
			return { type, width, Attribute::DUMMY };
		}
	};

	struct TransformFeedbackParticleSystemInitialParameters
	{
		std::size_t maxParticleCount;
		std::shared_ptr<Texture2D> texture;
		std::vector<std::pair<ShaderType, std::filesystem::path>> updateShaderPaths;
		std::vector<std::pair<ShaderType, std::filesystem::path>> renderShaderPaths;
		std::vector<RecordedAttribute> recordedVariables;
		std::vector<Param> params;
	};

	/**
	 * Creates and initializes transform feedback particle system instance.
	 *
	 * @param numMaxParticlesInBuffer  Maximal number of particles to be stored in the buffer
	 */
	TransformFeedbackParticleSystem();

	/**
	 * Initializes particle system. This function must be called after constructing the object
	 * (it's deliberately not called in constructor to prevent virtual method invocation there).
	 *
	 * @return True, if initialization was successful or false otherwise.
	 */
	bool init(TransformFeedbackParticleSystemInitialParameters params);

	/**
	 * Renders all particles from the current read buffer that were
	 * recorded during transform feedback.
	 */
	void render(glm::mat4 modelMatrix, glm::mat3 inversedTransposedModelMatrix, const std::uint32_t queries = 0, Renderable::QueryData* data = nullptr) const;

	/**
	 * Updates all particles in the current read buffer using render
	 * process with transform feedback and records the updated particles
	 * into the current write buffer.
	 *
	 * @param deltaTime  Time passed since last frame (in seconds)
	 */
	void updateParticles(const float deltaTime);

	/**
	 * Gets maximal number of particles that can be stored in the buffer.
	 * This number is chosen when constructing the particle system.
	 */
	std::size_t getMaxParticleCount() const;

	/**
	 * Gets number of particles currently stored in the buffer.
	 */
	std::size_t getNbParticles() const;

	template<typename T>
	bool setParameter(std::string const& name, T value)
	{
		auto& params_ptr = params_ubo->map();
		auto it = offset_map.find(name);
		if(it == offset_map.end())
			return false;
		std::memcpy(&params_ptr[it->second], &value, sizeof(T));
		return true;
	}

	void setGeneratedAmount(int numParticlesToGenerate, float generateEverySeconds);

	template<typename T>
	bool setRenderUniform(std::string const& name, T value) {
		return renderShader->setUniform(name, value);
	}

protected:
	void swapBuffers();

	/**
	 * Generates random number generator seed. This seed is a vec3 with random
	 * float values that can be passed to the shaders for random number generation.
	 */
	static glm::vec3 generateRandomNumberGeneratorSeed();

protected:
	enum Status
	{
		UNINITIALIZED,
		INITIALIZED,
		UPDATED
	};

	Status status{ UNINITIALIZED }; // Flag telling if the particle system has been initialized

    float remainingTimeToGenerateSeconds{ 0.0f }; // How many seconds remain until next generation batch
    float generateEverySeconds{ 0.05f }; // How often is one batch of particles generated (in seconds)
    unsigned int numParticlesToGenerate{ 40 }; // Number of generated particles in one batch

	using DoubleBuffer = std::pair<std::shared_ptr<StaticBufferStorage>, std::shared_ptr<StaticBufferStorage>>;
	DoubleBuffer buffers;
	std::pair<RawModel, RawModel> updateVAO;
	std::pair<RawModel, RawModel> renderVAO;

	std::map<std::string, std::size_t> offset_map;
	std::span<std::byte>::iterator params_ptr;

	std::unique_ptr<MappedBufferStorage<>> params_ubo = nullptr;

	std::size_t maxParticleCount{}; // Holds maximal number of particles stored in the VBOs
	std::size_t nbParticles{1};		// Holds current number of particles stored

	std::shared_ptr<TransformFeedbackShaderProgram> updateShader;
	std::shared_ptr<ShaderProgram> renderShader;

	std::vector<Attribute> updateAttribute, renderAttribute;

	std::shared_ptr<Texture2D> texture;

	/**
	 * Generates all OpenGL objects required for the transform feedback particle system.
	 * This includes particles VBOs, all VAOs, transform feedback and query.
	 */
	void generateOpenGLObjects();
};

}; // namespace plp