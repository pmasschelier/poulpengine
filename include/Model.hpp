#ifndef MODEL_HPP
#define MODEL_HPP

#include "RawModel.hpp"
#include "Renderable.hpp"
#include "Shaders/ProgramPipeline.hpp"
#include "Textures/Texture2D.hpp"

#include <array>

namespace plp {

class ModelBuilder;

class Model : public Renderable {

	friend ModelBuilder;

public:

	using Renderable::render;

	static constexpr std::size_t MAX_TEXTURE_UNITS = 16;

	struct Material {
		glm::vec4 ambient;
		glm::vec4 diffuse;
		glm::vec3 specular;
		float shininess;
	};

	Model  (RawModel&& model,
			std::shared_ptr<AbstractShaderProgram> program);

	Model  (std::shared_ptr<const RawModel> model,
			std::shared_ptr<AbstractShaderProgram> program);
	
	Model(Model&&) = default;
	Model& operator=(Model&&) = default;

	void setProgram(std::shared_ptr<AbstractShaderProgram> program);

	void setMaterial(Material const& material);

	void addTexture(std::shared_ptr<Texture> const& texture, std::string const& samplerName = "tex", std::size_t unit = 0);

	void setCullFace(bool activate = true);

	bool getCullFace() const;

	virtual void render(StaticBufferStorage& command) const;

	void render(const std::uint32_t queries = 0, QueryData* data = nullptr) const override;

	virtual void render(glm::mat4 modelMatrix, glm::mat3 inversedTransposedModelMatrix, const std::uint32_t queries, QueryData* data) const;

protected:

	static void checkTextureUnit(std::size_t unit);

	Material material;
	GLuint m_materialUBO{};

	std::array<std::optional<std::pair<std::string, std::shared_ptr<Texture>>>, MAX_TEXTURE_UNITS> textures;
	std::array<GLuint, 16> texturesId;

	bool cullFace = true;
};

}

#endif