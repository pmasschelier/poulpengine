#ifndef SCENEMANAGER_HPP
#define SCENEMANAGER_HPP

#include "Camera.hpp"
#include "InstancedModel.hpp"
#include "LightManager.hpp"
#include "ModelLoader.hpp"
#include "NameGenerator.hpp"
#include "Skybox.hpp"
#include "TransformBuffer.hpp"
#include "TransformFeedbackParticleSystem.hpp"
#include "Utils.hpp"

#include <map>
#include <memory>
#include <mutex>
#include <thread>

namespace plp
{

class SceneNode;

class SceneManager
{

public:

	struct SceneNodeInfos
	{
		std::shared_ptr<SceneNode> node;
		bool shouldRender = true;
	};

	struct ModelInfos
	{
		std::shared_ptr<Model> model;
	};

	struct InstancedModelInfos
	{
		std::shared_ptr<Model> model;
		std::uint32_t baseInstance = 0;
		std::uint32_t instanceCount = 0;
		std::shared_ptr<StaticBufferStorage> transforms;
	};

	SceneManager();

	SceneManager(SceneManager &) = delete;
	SceneManager &operator=(SceneManager &) = delete;

	SceneManager(SceneManager &&) = default;
	SceneManager &operator=(SceneManager &&) = default;

	[[nodiscard]] Opt<Texture> getTexture(std::string const &name) const;

	[[nodiscard]] Opt<SceneNode> loadSceneFromFile(std::filesystem::path const &path);
	[[nodiscard]] Opt<Texture2D> loadTextureFromFile(std::string const &name, std::filesystem::path const &filename);
	[[nodiscard]] bool loadSkyboxFromFiles(std::array<std::filesystem::path, 6> const &filenames);

	Opt<Model> addModel(std::string name, Model &&model, bool suffix = false);

	bool addLight(LightManager::Light const &light);

	[[nodiscard]] Opt<ShaderProgram> addShaderProgram(std::string const &name, ShaderProgram &&program);

	[[nodiscard]] Opt<ShaderProgram> addShaderProgram(std::string const &name, std::shared_ptr<ShaderProgram> const &program);

	[[nodiscard]] std::shared_ptr<SceneNode> createSceneNode(std::string const &name, std::shared_ptr<SceneNode> parent = nullptr);

	[[nodiscard]] std::shared_ptr<SceneNode> createSceneNode(std::string const &name, std::shared_ptr<Model> renderable, std::shared_ptr<SceneNode> parent = nullptr);

	[[nodiscard]] std::optional<std::pair<std::shared_ptr<SceneNode>, std::shared_ptr<Model>>> createSceneNode(
		std::string const &name, std::shared_ptr<RawModel> const &model, std::shared_ptr<ShaderProgram> const &shader, std::shared_ptr<SceneNode> parent = nullptr
	);

	[[nodiscard]] std::shared_ptr<SceneNode> createSceneNode(std::string const &name, std::shared_ptr<TransformFeedbackParticleSystem> const &system, std::shared_ptr<SceneNode> parent = nullptr);

	[[nodiscard]] std::optional<Collection<SceneNode>> createInstancedSceneNodes(
		std::string const &name, std::shared_ptr<Model> model, std::shared_ptr<SceneNode> parent = nullptr
	);

	[[nodiscard]] std::shared_ptr<SceneNode> duplicateSceneNode(std::shared_ptr<SceneNode> const &node, std::shared_ptr<SceneNode> parent);

	[[nodiscard]] std::shared_ptr<SceneNode> duplicateSceneNode(std::shared_ptr<SceneNode> const &node);

	[[nodiscard]] Opt<SceneNode> duplicateSceneNode(std::string const &name);

	bool enableRender(std::string const &name, bool enable = true);

	void render(bool frustumCulling = false);

private:

	void updateNodesTransforms();

	SceneNodeInfos emplaceSceneNode(std::string name, std::uint32_t transformId, std::shared_ptr<SceneNode> parent);

protected:

	std::unique_ptr<Camera> camera;
	std::unique_ptr<Skybox> skybox;
	std::unique_ptr<LightManager> light_manager;

	template <typename T> using SharedMap = std::unordered_map<std::string, std::shared_ptr<T>>;

	SharedMap<Texture2D> textures;
	/* SharedMap<ShaderObject> objects; */
	SharedMap<ShaderProgram> programs;
	std::unordered_map<std::string, SceneNodeInfos> nodes;
	std::shared_ptr<SceneNode> rootNode;
	std::unordered_map<std::string, ModelInfos> models;

	std::vector<InstancedModelInfos> instanced_models;

	NameGenerator nodeNameDomain, meshNameDomain;
	TransformBuffer transforms;
};

} // namespace plp

#endif
