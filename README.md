# PoulpEngine

## Description

PoulpEngine is a modern C++ 3D graphics library based on OpenGL.
It was developped using [C++23](https://en.cppreference.com/w/cpp/23) and [OpenGL 4.6](https://registry.khronos.org/OpenGL/specs/gl/glspec46.core.pdf) with [full DSA](https://github.com/fendevel/Guide-to-Modern-OpenGL-Functions).

The goal of the library is to provide a wrapper around OpenGL objects and more high-level concepts as HeightMaps or Camera to ease the developement of OpenGL based games.
It is agnostic of the windowing API although GLFW was used for the examples.

## Dependencies

PoulpEngine was build upon :

- OpenGL Mathematics (GLM) : [https://github.com/g-truc/glm](https://github.com/g-truc/glm)
- GLEW - The OpenGL Extension Wrangler Library : [https://github.com/nigels-com/glew](https://github.com/nigels-com/glew)

You better have these libraries installed, alongside with OpenGL 4.6 installed on your computer to be able to compile and use PoulpEngine.
To check if OpenGL 4.6 is installed on your machine :
- On Linux : `glxinfo | grep -i "opengl core profile version"`
- On Windows : IDK, you can try to use the glewinfo tool from [https://glew.sourceforge.net/](https://glew.sourceforge.net/)

Moreover some headers and source file of the following libraries have been completely or partially reused :
- stb : [https://github.com/nothings/stb](https://github.com/nothings/stb)
- SFML : [https://github.com/SFML/SFML](https://github.com/SFML/SFML)

## Build process

For the first build you have to install the submodules:
```sh
cd poulpengine
git submodule init
git submodule update
```

Then you can build the whole project with cmake:
```sh
cmake -B build
cd build
make
```

You can then found the library at build/libPoulpEngine.a and the examples in build/examples.

**Warning: In Release mode a lot of checks are disabled to not impact performance, to have proper debugging output build in Debug mode**

To build the library in debug mode do instead:
```sh
cmake -B build -DCMAKE_BUILD_TYPE=Debug
```
