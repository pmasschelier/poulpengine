cmake_minimum_required(VERSION 3.12)
project(Skybox)

# Set C++ standard to C++23
set(CMAKE_CXX_STANDARD 23)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# Find required packages (glfw and glew)
find_package(glfw3 REQUIRED)
# find_package(GLEW REQUIRED)

# Add the include directories
include_directories(include)
include_directories(../../externals/imgui ../../externals/imgui/backends)

# Add the source files for the TexturedCube example
file(GLOB EXAMPLE_SOURCES src/*.cpp ../common/common.cpp)
file(GLOB IMGUI_SOURCES
	../../externals/imgui/*.cpp
	../../externals/imgui/backends/imgui_impl_glfw.cpp
	../../externals/imgui/backends/imgui_impl_opengl3.cpp
)

# Create an executable from the source files
add_executable(${PROJECT_NAME} ${EXAMPLE_SOURCES} ${IMGUI_SOURCES})

add_custom_command(
	TARGET ${PROJECT_NAME}
	POST_BUILD
	COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/resources ./resources
)

# Link the required libraries to the example executable
target_link_libraries(${PROJECT_NAME} PRIVATE PoulpEngine glfw) # GLEW::GLEW
