#include "../../common/common.hpp"

#include <fstream>

#include <Shaders/ShaderBuilder.hpp>
#include <Skybox.hpp>
#include <Teapot.hpp>
#include <Model.hpp>
#include <LightManager.hpp>
#include <SceneManager.hpp>
#include <SFML-based/Font.hpp>
#include <SFML-based/Text.hpp>
#include <NameGenerator.hpp>

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include "glm/glm.hpp"

std::vector<const char*> names;
std::vector<plp::Model::Material> materials;
plp::Model::Material currentMaterialCopy;
int currentMaterial{};

void loadMaterial(std::filesystem::path const& filename) {
	std::ifstream file(filename);
    std::stringstream sstr;
    std::string str;

    if(file) {
#ifndef NDEBUG
		std::cout << std::endl;
		std::cout << std::setfill('-') << std::setw(20) << "";
		std::cout << " Materials ";
		std::cout << std::setfill('-') << std::setw(20) << "" << std::endl;
#endif
        std::getline(file, str);
        while(!file.eof()) {
            plp::Model::Material m;
            char c;
            std::stringstream name;
            while(file.get(c) && c != '\t') {
				if(c == '\n') continue;
                name << c;
			}
            file >> m.ambient.x >> m.ambient.y >> m.ambient.z;
            file >> m.diffuse.x >> m.diffuse.y >> m.diffuse.z;
            file >> m.specular.x >> m.specular.y >> m.specular.z;
            file >> m.shininess;
            m.shininess *= 128.f;
#ifndef NDEBUG
            std::cout << name.str() << " : " << std::endl;
            std::cout << "\tambient:\t" << m.ambient.x << " - " << m.ambient.y << " - " << m.ambient.z << std::endl;
            std::cout << "\tdiffuse:\t" << m.diffuse.x << " - " << m.diffuse.y << " - " << m.diffuse.z << std::endl;
            std::cout << "\tspecular:\t" << m.specular.x << " - " << m.specular.y << " - " << m.specular.z << std::endl;
            std::cout << "\tshininess:\t" << m.shininess << std::endl;
#endif
			names.push_back(strdup(name.str().c_str()));
            materials.push_back(m);
        }

		currentMaterialCopy = materials[currentMaterial];
    }
}

bool showNormals = false;
extern bool wireframeMode;
extern bool captureInput;

void update(GLFWwindow* window, plp::Model& teapot) {
	static int lastMaterial = currentMaterial;
	
	commonKeyboardUpdate(window);

	if(keyboard.key[GLFW_KEY_N]) {
		keyboard.key[GLFW_KEY_N] = false;
		showNormals = !showNormals;
	}
	if(keyboard.key[GLFW_KEY_SPACE]) {
		keyboard.key[GLFW_KEY_SPACE] = false;
		currentMaterial = (currentMaterial + 1) % materials.size();
	}
	if(currentMaterial != lastMaterial) {
		currentMaterialCopy = materials[currentMaterial];
		lastMaterial = currentMaterial;
	}
	teapot.setMaterial(currentMaterialCopy);

	keyboard.ack = true;
}

float teapotAngle = 0.f;

int main(void)
{
	constexpr glm::ivec2 WSIZE(800, 800);
	GLFWwindow* window = glfw_init("Teapot and skybox", WSIZE);
	glfwMaximizeWindow(window);

	if(!window)
		return EXIT_FAILURE;

	if(!plp::init())
        return EXIT_FAILURE;
	
	camera = std::make_unique<plp::Camera>();
	camera->setRatio(WSIZE.x / WSIZE.y);
	camera->setPosition({ 0.f, 4.f, -10.f });
	camera->setTarget({ 0.f, 0.f, 0.f });

	plp::SceneManager scene;

	bool success = scene.loadSkyboxFromFiles({
		"resources/skybox/skybox_px.jpg",
		"resources/skybox/skybox_nx.jpg",
		"resources/skybox/skybox_py.jpg",
		"resources/skybox/skybox_ny.jpg",
		"resources/skybox/skybox_pz.jpg",
		"resources/skybox/skybox_nz.jpg",
	});

	if(!success) {
		std::cerr << "Error while loading the Skybox." << std::endl;
		return EXIT_FAILURE;
	}

	auto rawteapot = std::make_shared<plp::RawModel>(plp::Teapot::create());

	plp::ShaderBuilder builder;

	builder.addShader(plp::ShaderBuilder::getCommonShader(plp::VERTEX_PASSTHROUGH_SOURCE));
	auto control_shader = builder.addFile(plp::TESS_CONTROL_SHADER, "resources/shaders/teapot.cs").value();
	auto evaluation_shader = builder.addFile(plp::TESS_EVALUATION_SHADER, "resources/shaders/teapot.es").value();
	builder.addShader(plp::ShaderBuilder::getCommonShader(plp::FRAGMENT_MATERIAL_SOURCE));
	auto shaderCommon = scene.addShaderProgram("teapot", builder.createProgram(true).value()).value();

	builder.addShader(plp::ShaderBuilder::getCommonShader(plp::VERTEX_PASSTHROUGH_SOURCE));
	builder.addShader(control_shader);
	builder.addShader(evaluation_shader);
	builder.addShader(plp::ShaderBuilder::getCommonShader(plp::GEOMETRY_NORMALS_SOURCE));
	builder.addShader(plp::ShaderBuilder::getCommonShader(plp::FRAGMENT_UNICOLOR_SOURCE));
	auto shaderNormals = scene.addShaderProgram("normals", builder.createProgram(true).value()).value();

	shaderNormals->setUniform("color", glm::vec3(1.0, 1.0, 0.0));
	
	scene.addLight({
		.type = plp::LightManager::DIRECTIONAL,
		.direction = { 0.f, -2.f, 5.f, 1.f }
	});

	loadMaterial("resources/materials.csv");

	auto [teapot, teapotModel] = scene.createSceneNode("teapot", rawteapot, shaderCommon).value();
	teapotModel->setMaterial(currentMaterialCopy);
	teapotModel->setCullFace(false);

	plp::Model::QueryData data;
	//teapot.addModel(teapotModel, plp::Model::TESS_CONTROL_SHADER_PATCHES | plp::Model::PRIMITIVES_GENERATED, &data);

	auto [normals_teapot, normalsTeapotModel] = scene.createSceneNode("teapotNormals", rawteapot, shaderNormals).value();
	teapot->addChild(normals_teapot);
	normalsTeapotModel->setCullFace(false);

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();

	// Setup Platform/Renderer backends
	ImGui_ImplGlfw_InitForOpenGL(window, true);          // Second param install_callback=true will install GLFW callbacks and chain to existing ones.
	ImGui_ImplOpenGL3_Init();

	double currentTime = glfwGetTime();
	double lastTime = currentTime;
	
	while(!glfwWindowShouldClose(window)) {

		currentTime = glfwGetTime();
		elapsedTime = currentTime - lastTime;
		lastTime = currentTime;

		glfwPollEvents();
		update(window, *teapotModel);

		if(captureInput) {
			io.ConfigFlags &= ~ImGuiConfigFlags_NavEnableKeyboard;	// Disable Keyboard Controls
			io.ConfigFlags &= ~ImGuiConfigFlags_NavEnableGamepad;	// Disable Gamepad Controls
			io.ConfigFlags |= ImGuiConfigFlags_NoMouse;				// Disable Mouse Controls
		}
		else {
			io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;	// Enable Keyboard Controls
			io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;	// Enable Gamepad Controls
			io.ConfigFlags &= ~ImGuiConfigFlags_NoMouse;			// Disable Mouse Controls
		}

		// Start the Dear ImGui frame
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		{
			ImGui::Begin("Teapot options");

			ImGui::Checkbox("Capture events (Press P)", &captureInput);
			ImGui::Checkbox("Show normals (Press N)", &showNormals);
			ImGui::Checkbox("Wireframe mode (Press W)", &wireframeMode);

			ImGui::SeparatorText("Teapot rotation");
			ImGui::SliderAngle("Y-axis angle", &teapotAngle);
			ImGui::SeparatorText("Teapot material (Press Space)");
			ImGui::Combo("##Teapot material", &currentMaterial, names.data(), names.size());
			ImGui::Text("Ambient color");
			ImGui::ColorEdit3("##AmbientEdit", &currentMaterialCopy.ambient.r, ImGuiColorEditFlags_Float);
			ImGui::Text("Diffuse color");
			ImGui::ColorEdit3("##DiffuseEdit", &currentMaterialCopy.diffuse.r, ImGuiColorEditFlags_Float);
			ImGui::Text("Specular color");
			ImGui::ColorEdit3("##SpecularEdit", &currentMaterialCopy.specular.r, ImGuiColorEditFlags_Float);
			ImGui::Text("Shininess");
			ImGui::SliderFloat("##ShininessEdit", &currentMaterialCopy.shininess, 0.f, 128.f, "%.f");

			ImGui::SeparatorText("Statistics");
			ImGui::Text("Patches passed : %d", data.tess_control_shader_patches);
			ImGui::Text("Primitives generated : %d", data.primitives_generated);
			ImGui::Text("ImGui average %.3f ms/frame (%.1f FPS)", 1000.0f / io.Framerate, io.Framerate);

			ImGui::End();
		}

		teapot->setRotation(glm::angleAxis(teapotAngle, glm::vec3(0.0, 1.0, 0.0)));
		scene.enableRender("teapotNormals", showNormals);

		plp::clear({ 0.2f, 0.3f, 0.3f, 1.0f });

		scene.render();

		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		glfwSwapBuffers(window);
	}

	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
	
	return EXIT_SUCCESS;
}
