// vertex shader
#version 460

layout (location = 0) in vec3 aPos;

out gl_PerVertex
{
	vec4 gl_Position;
	float gl_PointSize;
	float gl_ClipDistance[];
};

void main()
{
    gl_Position = vec4(aPos, 1.0);
}