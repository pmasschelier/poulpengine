#version 460 core
#extension GL_ARB_shading_language_include : require

layout (quads, fractional_odd_spacing, ccw) in;

#include "/common/camera.glsl"

in gl_PerVertex
{
	vec4 gl_Position;
	float gl_PointSize;
	float gl_ClipDistance[];
} gl_in[gl_MaxPatchVertices];

out gl_PerVertex {
	vec4 gl_Position;
	float gl_PointSize;
	float gl_ClipDistance[];
};

out VS_OUT {
	vec3 wPos;
	vec3 wNormal;
};

uniform mat4 model;
uniform mat3 inversedTransposedModel;

vec4 bezierSurface(const float Bu[4], const float Bv[4]) {
	return Bu[0] * (
		Bv[0] * gl_in[0].gl_Position
	+	Bv[1] * gl_in[1].gl_Position
	+	Bv[2] * gl_in[2].gl_Position
	+	Bv[3] * gl_in[3].gl_Position)
	+	Bu[1] * (
		Bv[0] * gl_in[4].gl_Position
	+	Bv[1] * gl_in[5].gl_Position
	+	Bv[2] * gl_in[6].gl_Position
	+	Bv[3] * gl_in[7].gl_Position)
	+	Bu[2] * (
		Bv[0] * gl_in[8].gl_Position
	+	Bv[1] * gl_in[9].gl_Position
	+	Bv[2] * gl_in[10].gl_Position
	+	Bv[3] * gl_in[11].gl_Position)
	+	Bu[3] * (
	+	Bv[0] * gl_in[12].gl_Position
	+	Bv[1] * gl_in[13].gl_Position
	+	Bv[2] * gl_in[14].gl_Position
	+	Bv[3] * gl_in[15].gl_Position);
}

void main()
{
    // get patch coordinate
    float u = gl_TessCoord.x;
    float v = gl_TessCoord.y;

	const float Bv[4] = {(1-v)*(1-v)*(1-v), 3*v*(1-v)*(1-v), 3*v*v*(1-v), v*v*v};
	const float Bdv[4] = {-3*(1-v)*(1-v), 3*(1-v)*(1-v) - 6*v*(1-v), 6*v*(1-v) - 3*v*v, 3*v*v};
	const float Bu[4] = {(1-u)*(1-u)*(1-u), 3*u*(1-u)*(1-u), 3*u*u*(1-u), u*u*u};
	const float Bdu[4] = {-3*(1-u)*(1-u), 3*(1-u)*(1-u) - 6*u*(1-u), 6*u*(1-u) - 3*u*u, 3*u*u};

	vec4 p = bezierSurface(Bu, Bv);
	vec3 du = vec3(bezierSurface(Bdu, Bv));
	vec3 dv = vec3(bezierSurface(Bu, Bdv));

    // ----------------------------------------------------------------------
    // output patch point position in clip space

    gl_Position = projectionView * model * p;

	wPos = vec3(model * p);
	wNormal = normalize(inversedTransposedModel * cross(du, dv));
}