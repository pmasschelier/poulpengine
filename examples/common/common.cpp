
#include "common.hpp"

bool wireframeMode = false;
bool captureInput = true;

std::unique_ptr<plp::Camera> camera;
double elapsedTime;

Keyboard keyboard;
glm::ivec2 windowSize;

void glfw_error_callback(int error, const char *description);
void glfw_key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void glfw_mouse_callback(GLFWwindow* window, double xpos, double ypos);
void glfw_framebuffer_size_callback(GLFWwindow* window, int width, int height);

GLFWwindow* glfw_init(std::string_view const& title, glm::ivec2 wsize) {
	const char *vs = glfwGetVersionString();
	std::cout << "GLFW version : " << vs << std::endl;
	glfwSetErrorCallback(glfw_error_callback);

	if (!glfwInit())
	{
		std::cerr << "Initialization of GLFW failed" << std::endl;
		return nullptr;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, true);
	glfwWindowHint(GLFW_SAMPLES, 4);

	GLFWwindow* window = glfwCreateWindow(wsize.x, wsize.y, title.data(), nullptr, nullptr);
	
	if (!window)
	{
		// Window or OpenGL context creation failed
		std::cerr << "Window or OpenGL context creation failed" << std::endl;
		return nullptr;
	}

	glfwMakeContextCurrent(window);
	glfwSwapInterval(0);
	//glfwSetInputMode(window, GLFW_STICKY_KEYS, GLFW_TRUE);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetKeyCallback(window, glfw_key_callback);
	glfwSetCursorPosCallback(window, glfw_mouse_callback); 
	glfwSetFramebufferSizeCallback(window, glfw_framebuffer_size_callback);

	return window;
}

void glfw_error_callback([[maybe_unused]] int error, const char *description)
{
	std::cerr << "Error (GLFW) : " << description << std::endl;
}

void glfw_mouse_callback([[maybe_unused]]GLFWwindow* window, double xpos, double ypos)
{
	static bool firstMouse = true;
	static double lastX, lastY;

	glfwSetInputMode(window, GLFW_CURSOR, captureInput ? GLFW_CURSOR_DISABLED : GLFW_CURSOR_NORMAL);

	if(!captureInput)
		return;

    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }
  
    float xoffset = std::clamp(xpos - lastX, -200., 200.);
    float yoffset = std::clamp(lastY - ypos, -200., 200.); 
    lastX = xpos;
    lastY = ypos;
    camera->moveView({ xoffset, yoffset });
}

void glfw_key_callback([[maybe_unused]] GLFWwindow* window, int key, [[maybe_unused]] int scancode, int action, [[maybe_unused]] int mods)
{
    if (action == GLFW_PRESS) {
		keyboard.key[key] = true;
		keyboard.ack = false;
	}
	if(action == GLFW_RELEASE && keyboard.ack) {
		keyboard.key[key] = false;
	}
}

void glfw_framebuffer_size_callback([[maybe_unused]] GLFWwindow* window, int width, int height)
{
	windowSize = glm::uvec2(width, height);
    plp::setViewPort({0, 0, width, height});
	camera->setRatio((float) width/height);
}

void commonKeyboardUpdate(GLFWwindow* window) {

	if(keyboard.key[GLFW_KEY_P]) {
		keyboard.key[GLFW_KEY_P] = false;
		captureInput = !captureInput;
	}
	
	if(keyboard.key[GLFW_KEY_W] || keyboard.key[GLFW_KEY_Z]) {
		keyboard.key[GLFW_KEY_W] = keyboard.key[GLFW_KEY_Z] = false;
		wireframeMode = !wireframeMode;
	}
	plp::setWireframeMode(wireframeMode); // Need to be here to be triggered by ImGUI too

	if(keyboard.key[GLFW_KEY_S]) {
		keyboard.key[GLFW_KEY_S] = false;
		plp::Image screenshot;
		int width, height;
		glfwGetWindowSize(window, &width, &height);
		plp::screenshot(glm::uvec2(width, height), screenshot);
		if(!screenshot.saveToFile("screenshot.png"))
			std::cerr << "Screenshot couldn't be saved as screenshot.png" << std::endl;
		else
			std::cout << "Screenshot saved as screenshot.png" << std::endl;
	}

	if(!captureInput)
		return;

	unsigned m{};

	if(keyboard.key[GLFW_KEY_UP])
		m |= plp::Camera::FORWARD;
	if(keyboard.key[GLFW_KEY_DOWN])
		m |= plp::Camera::BACKWARD;
	if(keyboard.key[GLFW_KEY_RIGHT])
		m |= plp::Camera::RIGHT;
	if(keyboard.key[GLFW_KEY_LEFT])
		m |= plp::Camera::LEFT;
	/* if(keyboard.key[GLFW_KEY_SPACE])
		m |= plp::Camera::UP;
	if(keyboard.key[GLFW_KEY_LEFT_SHIFT])
		m |= plp::Camera::DOWN; */
	
	camera->move((plp::Camera::Movement) m, elapsedTime);
}