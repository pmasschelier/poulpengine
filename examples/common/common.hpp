#ifndef COMMON_HPP
#define COMMON_HPP

// To make sure there will be no header conflicts
// (should be fine even without that)
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <GL/glew.h>

#include <algorithm>
#include <iostream>
#include <memory>

#include <PoulpEngine.hpp>
#include <Camera.hpp>

struct Keyboard {
	bool key[GLFW_KEY_LAST + 1];
	bool ack = false;
};

extern Keyboard keyboard;
extern glm::ivec2 windowSize;

extern std::unique_ptr<plp::Camera> camera;
extern double elapsedTime;

GLFWwindow* glfw_init(std::string_view const& title, glm::ivec2 wsize);
void commonKeyboardUpdate(GLFWwindow* window);

#endif