#version 330 core
out vec4 FragColor;

in vec2 texCoord;
in vec3 wPos;
in vec3 wNormal;

uniform float earthDistance;
uniform float earthRadius;

uniform sampler2D dayTexture;
uniform sampler2D nightTexture;

void main()
{
    float ratio = clamp((length(wPos) - earthDistance) / earthRadius + 0.25f, 0.f, 1.f);
    vec4 day = texture(dayTexture, texCoord);
    vec4 night = texture(nightTexture, texCoord);
    FragColor = mix(day, night, ratio);
}