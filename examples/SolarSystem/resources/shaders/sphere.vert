#version 330 core
layout(location = 0) in vec3 aPos;
layout(location = 2) in vec2 aTex;

layout (std140) uniform Camera
{
	mat4 projection; // 4 * 4 * 4 bytes = align 16
	mat4 view; // 4 * 4 * 4 bytes = align 16
	mat4 projectionView; // 4 * 4 * 4 bytes = align 16
	mat4 inversedTransposedView; // 4 * 4 * 4 bytes = align 12
	vec3 position; // 3 * 3 bytes = align 12
};

uniform mat4 model;
uniform mat3 inversedTransposedModel;

out vec2 texCoord;
out vec3 wPos;
out vec3 wNormal;

void main() {
    wPos = vec3(model * vec4(aPos, 1.0));
	wNormal = normalize(inversedTransposedModel * aPos);
    gl_Position = projectionView * vec4(wPos, 1.0); // w = 1 -> position, w = 0 -> direction
    texCoord = aTex;
}