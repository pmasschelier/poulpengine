#version 330 core
out vec4 FragColor;

in vec2 texCoord;
in vec3 wPos;
in vec3 wNormal;

uniform vec3 moonPos;
uniform float moonRadius;
uniform sampler2D moonTexture;

void main()
{  
	float ratio = max(-dot(normalize(wPos - moonPos), wNormal), 0.0);
	FragColor = texture(moonTexture, texCoord) * ratio;
}