#include "../../common/common.hpp"

#include <fstream>

#include <FrustumShape.hpp>
#include <SceneManager.hpp>
#include <Shaders/ShaderBuilder.hpp>
#include <Skybox.hpp>
#include <Sphere.hpp>
#include <Plane.hpp>
#include <Model.hpp>

#include <random>

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

using namespace std::numbers;

extern bool wireframeMode;
extern bool captureInput;

bool moveAsteroids = false;
bool frustumCullingEnabled = false;
bool externalView = false;

void update(GLFWwindow* window) {
	if(keyboard.key[GLFW_KEY_A]) {
		keyboard.key[GLFW_KEY_A] = false;
		moveAsteroids = !moveAsteroids;
	}
	if(keyboard.key[GLFW_KEY_F]) {
		keyboard.key[GLFW_KEY_F] = false;
		frustumCullingEnabled = !frustumCullingEnabled;
	}
	if(keyboard.key[GLFW_KEY_E]) {
		keyboard.key[GLFW_KEY_E] = false;
		externalView = !externalView;
	}
	commonKeyboardUpdate(window);
	keyboard.ack = true;
}

const static float kSizeSun = 1;
const static float kSizeEarth = 0.5;
const static float kSizeMoon = 0.25;
const static float kRadOrbitEarth = 10;
const static float kRadOrbitMoon = 2;
const static float kRadOrbitAsteroid = 200;

#include <variant>
#include "DrawIndirectCommand.hpp"

int main(void)
{

	std::cout << "Sizeof DrawElementsIndirectCommand: " << sizeof(plp::DrawElementsIndirectCommand) << std::endl;
	std::cout << "Sizeof variant: " << sizeof(std::variant<plp::DrawElementsIndirectCommand, plp::DrawArraysIndirectCommand>) << std::endl;
	constexpr glm::ivec2 WSIZE(800, 800);
	GLFWwindow* window = glfw_init("Teapot and skybox", WSIZE);
	glfwMaximizeWindow(window);
	glfwGetWindowSize(window, &windowSize.x, &windowSize.y);

	if(!window)
		return EXIT_FAILURE;

	if(!plp::init())
        return EXIT_FAILURE;
	
	plp::ShaderBuilder builder;
	plp::SceneManager scene;
	
	camera = std::make_unique<plp::Camera>();
	camera->setSpeed(6.f);
	camera->setRatio(windowSize.x / windowSize.y);
	camera->setPosition({ 0.f, 4.f, -10.f });
	camera->setTarget({ 0.f, 0.f, 0.f });
	camera->setFarClipping(600.f);

	plp::Camera fixedCamera;
	fixedCamera.setPosition({0.f, 50.f, -250.f});
	fixedCamera.setTarget({ 0.f, 0.f, 0.f });
	fixedCamera.setFarClipping(600.f);

	bool success = scene.loadSkyboxFromFiles({
		"resources/skybox/right.png",
		"resources/skybox/left.png",
		"resources/skybox/top.png",
		"resources/skybox/bottom.png",
		"resources/skybox/front.png",
		"resources/skybox/back.png",
	});

	if(!success) {
		std::cerr << "Error while loading the Skybox." << std::endl;
		return EXIT_FAILURE;
	}

	scene.addLight({
		.type = plp::LightManager::POINT,
	});

	auto sphere = std::make_shared<plp::RawModel>(plp::Sphere::createTexturedSphere(100, 100).create().value());

	auto sphere_vert = std::make_shared<plp::ShaderObject>(plp::ShaderObject::createFromFile(
		plp::VERTEX_SHADER,
		"resources/shaders/sphere.vert"
	).value());

	////////////////////	SUN		////////////////////

	builder.addShader(sphere_vert);
	builder.addFile(plp::FRAGMENT_SHADER, "resources/shaders/sun.frag");
	auto sun_shader = scene.addShaderProgram("sun", builder.createProgram().value()).value();
	auto [sun, sunModel] = scene.createSceneNode("sun", sphere, sun_shader).value();
	sun->setScale(glm::vec3(kSizeSun));

	auto sun_tex = scene.loadTextureFromFile("sun", "resources/textures/2k_sun.jpg").value();
	sunModel->addTexture(sun_tex, "sunTexture");

	////////////////////	EARTH		////////////////////

	builder.addShader(sphere_vert);
	builder.addFile(plp::FRAGMENT_SHADER, "resources/shaders/earth.frag");
	auto earth_shader = scene.addShaderProgram("earth", builder.createProgram().value()).value();
	earth_shader->setUniform("earthDistance", kRadOrbitEarth);
	earth_shader->setUniform("earthRadius", kSizeEarth);
	auto [earth, earthModel] = scene.createSceneNode("earth", sphere, earth_shader).value();
	earth->setScale(glm::vec3(kSizeEarth));

	auto earth_day = scene.loadTextureFromFile("earthday", "resources/textures/2k_earth_daymap.jpg").value();
	earthModel->addTexture(earth_day, "dayTexture", 0);

	auto earth_night = scene.loadTextureFromFile("earthnight", "resources/textures/2k_earth_nightmap.jpg").value();
	earthModel->addTexture(earth_night, "nightTexture", 1);

	constexpr double earth_angle = glm::radians(23.5);
	glm::vec3 earth_rotation_axis(std::sin(earth_angle), std::cos(earth_angle), 0.f);
	earth->setRotation(glm::rotation(glm::vec3(0.0, 1.0, 0.0), earth_rotation_axis));

	////////////////////	MOON		////////////////////

	builder.addShader(sphere_vert);
	builder.addFile(plp::FRAGMENT_SHADER, "resources/shaders/moon.frag");
	auto moon_shader = scene.addShaderProgram("moon", builder.createProgram().value()).value();
	moon_shader->setUniform("moonRadius", kSizeMoon);
	auto [moon, moonModel] = scene.createSceneNode("moon", sphere, moon_shader).value();
	moon->setScale(glm::vec3(kSizeMoon));

	auto moon_tex = scene.loadTextureFromFile("moon", "resources/textures/2k_moon.jpg").value();
	moonModel->addTexture(moon_tex);

	////////////////////	EARTH CENTER	////////////////////
	
	auto earthCenter = scene.createSceneNode("earthCenter");
	earthCenter->addChild(earth);
	earthCenter->addChild(moon);

	////////////////////	ASTEROID	////////////////////
	auto asteroid_center = scene.createSceneNode("asteroid center");

	plp::ModelLoader loader;
	builder.addShader(plp::ShaderBuilder::getCommonShader(plp::VERTEX_TEXTURE_INSTANCED_SOURCE));
	builder.addShader(plp::ShaderBuilder::getCommonShader(plp::FRAGMENT_TEXTURE_SOURCE));
	auto asteroid_shader = scene.addShaderProgram("asteroid", builder.createProgram().value()).value();

	auto asteroid = loader.loadNthModelFromFile(scene, "resources/models/rock/rock.obj", 0).value();
	auto instanced_asteroid = std::make_shared<plp::Model>(asteroid.create(asteroid_shader, 20000).value());
	auto asteroids = scene.createInstancedSceneNodes("rock", instanced_asteroid, asteroid_center).value();

	////////////////////	FRUSTUM 	////////////////////
	builder.addShader(plp::ShaderBuilder::getCommonShader(plp::VERTEX_POSITIONS_SOURCE));
	builder.addShader(plp::ShaderBuilder::getCommonShader(plp::FRAGMENT_UNICOLOR_SOURCE));
	auto frustumShader = scene.addShaderProgram("frustumShader", builder.createProgram().value()).value();
	frustumShader->setUniform("color", glm::vec3(1.f, 0.f, 0.f));
	plp::ModelBuilder frustumModel = plp::FrustumShape::create(camera->getFov(), camera->getRatio(), camera->getNearClipping(), camera->getFarClipping());
	auto frustumSceneNode = scene.createSceneNode("frustum", std::make_shared<plp::RawModel>(frustumModel.create().value()), frustumShader).value().first;

	std::default_random_engine gen;
    std::uniform_real_distribution<double> uniform_d(-1.0, 1.0);
	std::normal_distribution normal_d(0.0, 1.0);
	
	int i = 1;
	for(auto const& asteroid: asteroids) {
		float angleY = uniform_d(gen) * pi;
		float ecartX = normal_d(gen) * 0.2;
		float ecartY = normal_d(gen) * .02;
		float ecartZ = normal_d(gen) * 0.2;
		float scale = 0.2f + normal_d(gen) * 0.2;
		auto eulerAngles = glm::vec3(uniform_d(gen) * pi, uniform_d(gen) * pi, uniform_d(gen) * pi);
		asteroid->setPosition(kRadOrbitAsteroid * glm::vec3(std::cos(angleY) + ecartX, ecartY, std::sin(angleY) + ecartZ));
		asteroid->setScale(glm::vec3(std::max(scale, 0.f)));
		asteroid->setRotation(glm::quat(eulerAngles));
	}

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();

	// Setup Platform/Renderer backends
	ImGui_ImplGlfw_InitForOpenGL(window, true);          // Second param install_callback=true will install GLFW callbacks and chain to existing ones.
	ImGui_ImplOpenGL3_Init();

	double currentTime = glfwGetTime();
	double lastTime = currentTime;

	double angle{}; // rotation angle of the earth

	while(!glfwWindowShouldClose(window)) {

		currentTime = glfwGetTime();
		elapsedTime = currentTime - lastTime;
		lastTime = currentTime;

		glfwPollEvents();
		update(window);

		if(captureInput) {
			io.ConfigFlags &= ~ImGuiConfigFlags_NavEnableKeyboard;	// Disable Keyboard Controls
			io.ConfigFlags &= ~ImGuiConfigFlags_NavEnableGamepad;	// Disable Gamepad Controls
			io.ConfigFlags |= ImGuiConfigFlags_NoMouse;				// Disable Mouse Controls
		}
		else {
			io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;	// Enable Keyboard Controls
			io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;	// Enable Gamepad Controls
			io.ConfigFlags &= ~ImGuiConfigFlags_NoMouse;			// Disable Mouse Controls
		}

		// Start the Dear ImGui frame
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		{
			ImGui::Begin("Teapot options");

			ImGui::Checkbox("Capture events (Press P)", &captureInput);
			ImGui::Checkbox("Wireframe mode (Press W)", &wireframeMode);
			ImGui::Checkbox("Move asteroids (Press A)", &moveAsteroids);
			ImGui::Checkbox("Enable frustum culling (Press F)", &frustumCullingEnabled);
			ImGui::Checkbox("Enable external view (Press E)", &externalView);

			ImGui::SeparatorText("Statistics");
			ImGui::Text("ImGui average %.3f ms/frame (%.1f FPS)", 1000.0f / io.Framerate, io.Framerate);

			ImGui::End();
		}

		double angle_disp = elapsedTime / 120.f * 2 * pi;
		angle += angle_disp;

		earthCenter->setPosition(glm::vec3(kRadOrbitEarth * std::cos(angle * 0.5), 0.f, kRadOrbitEarth * std::sin(angle * 0.5)));

		earth->rotate(glm::angleAxis((float)angle_disp, earth_rotation_axis));
		
		moon->setPosition(glm::vec3(kRadOrbitMoon * std::cos(angle * 2.f), 0.0, kRadOrbitMoon * std::sin(angle * 2.f)));
		moon->rotate(glm::angleAxis(- (float)angle_disp, glm::vec3(0.0, 1.0, 0.0)));

		if(moveAsteroids) {
			const float freq = glm::radians(1.f);
			const float asteroidsAngleDisplacement = freq * elapsedTime;
			asteroid_center->rotate(glm::angleAxis(0.1f * asteroidsAngleDisplacement, glm::vec3(0.f, 1.f, 0.f)));
			const float phase = 2.f * pi / static_cast<float>(asteroids.size());
			const float amplitude = 0.02f * kRadOrbitAsteroid;
			for(unsigned int i = 0; i < asteroids.size(); i++) {
				asteroids[i]->move(glm::vec3(0.f, amplitude * asteroidsAngleDisplacement * glm::sin(freq * currentTime + static_cast<float>(i) * phase), 0.f));
			}
		}

		moon_shader->setUniform("moonPos", moon->getPosition());

		plp::clear({ 0.2f, 0.3f, 0.3f, 1.0f });

		camera->activate();
		scene.enableRender(frustumSceneNode->getName(), false);
		
		float windowSizeX = externalView ? windowSize.x * 0.5f : windowSize.x;
		camera->setRatio(windowSizeX / windowSize.y);
		plp::setViewPort(glm::uvec4(0, 0, windowSizeX, windowSize.y));
		scene.render(frustumCullingEnabled);

		if(externalView) {
			frustumSceneNode->setPosition(camera->getPosition());
			frustumSceneNode->setRotation(camera->getRotation());
			scene.enableRender(frustumSceneNode->getName());
			fixedCamera.copyFrustum(*camera);
			fixedCamera.activate();
			fixedCamera.setRatio(windowSizeX / windowSize.y);
			plp::setViewPort(glm::uvec4(windowSizeX, 0, windowSizeX, windowSize.y));
			scene.render(frustumCullingEnabled);
		}

		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		glfwSwapBuffers(window);
	}

	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
	
	return EXIT_SUCCESS;
}