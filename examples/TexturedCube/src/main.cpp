#include "../../common/common.hpp"

#include <Cube.hpp>
#include <Model.hpp>
#include <SceneManager.hpp>
#include <Shaders/ShaderBuilder.hpp>
#include <Skybox.hpp>
#include <SFML-based/Font.hpp>
#include <SFML-based/Text.hpp>

void update(GLFWwindow* window) {
	commonKeyboardUpdate(window);
	keyboard.ack = true;
}

int main(void)
{
	constexpr glm::ivec2 WSIZE(800, 800);
	GLFWwindow* window = glfw_init("A Textured Cube", WSIZE);

	if(!window)
		return EXIT_FAILURE;

	if(!plp::init())
        return EXIT_FAILURE;
	
	camera = std::make_unique<plp::Camera>();
	camera->setRatio(WSIZE.x / WSIZE.y);
	camera->setPosition({ 0.f, 2.f, -5.f });
	camera->setTarget({ 0.f, 0.f, 0.f });

	plp::SceneManager scene;
	plp::ShaderBuilder builder;
	plp::ShaderProgram::implementationLimits(std::cout);

	auto vert = plp::ShaderBuilder::getCommonShader(plp::VERTEX_TEXTURE_SOURCE);
	vert->outputProperties(std::cout);
	builder.addShader(vert);

	auto frag = plp::ShaderBuilder::getCommonShader(plp::FRAGMENT_TEXTURE_SOURCE);
	frag->outputProperties(std::cout);
	builder.addShader(frag);

	auto program = std::make_shared<plp::ShaderProgram>(builder.createProgram().value());
	
	program->outputProperties(std::cout);
	program->outputInterface(std::cout);

	auto craftingTex = scene.loadTextureFromFile("table", "resources/crafting_table.png").value();
	// craftingTex->setSmooth(true);

	plp::RawModel cube = plp::Cube::createTexturedCube(3, 1, {0, 0, 2, 2, 1, 1}).create().value();
	auto cubeModel = std::make_shared<plp::Model>(std::move(cube), program);
	cubeModel->addTexture(craftingTex);
	
	scene.createSceneNode("crafting table", cubeModel);

	double currentTime = glfwGetTime();
	double lastTime = currentTime;

	while(!glfwWindowShouldClose(window)) {

		currentTime = glfwGetTime();
		elapsedTime = currentTime - lastTime;
		lastTime = currentTime;

		update(window);

		plp::clear({ 0.2f, 0.3f, 0.3f, 1.0f });

		scene.render();

		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	
	return EXIT_SUCCESS;
}