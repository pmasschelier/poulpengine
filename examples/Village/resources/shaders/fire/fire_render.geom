#version 440 core

layout (std140, binding = 0) uniform Camera
{
	mat4 projection; // 4 * 4 * 4 bytes = align 16
	mat4 view; // 4 * 4 * 4 bytes = align 16
	mat4 projectionView; // 4 * 4 * 4 bytes = align 16
	mat4 inversedTransposedView; // 4 * 4 * 4 bytes = align 12
	vec3 position; // 3 * 3 bytes = align 12
	// padding 4
	vec3 billboardHorizontalVector; // 3 * 3 bytes = align 12
	// padding 4
	vec3 billboardVerticalVector; // 3 * 3 bytes = align 12
};

layout(points) in;
layout(triangle_strip) out;
layout(max_vertices = 4) out;

// All that we get from vertex shader
in float ioLifetime[];
in float ioSize[];

// Color of particles
uniform vec3 particlesColor;

// Output variables
smooth out vec2 ioTexCoord;
smooth out vec4 ioEyeSpacePosition;
flat out vec4 ioColor;

void main()
{
    vec3 particlePosition = gl_in[0].gl_Position.xyz;
    float size = ioSize[0];

    // Mix particles color with alpha calculated from particle lifetime
    ioColor = vec4(particlesColor, min(1.0, ioLifetime[0]));

    // Emit bottom left vertex
    vec4 bottomLeft = vec4(particlePosition+(-billboardHorizontalVector-billboardVerticalVector)*size, 1.0);
    ioTexCoord = vec2(0.0, 0.0);
    gl_Position = projectionView * bottomLeft;
    ioEyeSpacePosition = view * bottomLeft;
    EmitVertex();

    // Emit bottom right vertex
    vec4 bottomRight = vec4(particlePosition+(billboardHorizontalVector-billboardVerticalVector)*size, 1.0);
    ioTexCoord = vec2(1.0, 0.0);
    gl_Position = projectionView * bottomRight;
    ioEyeSpacePosition = view * bottomRight;
    EmitVertex();

    // Emit top left vertex
    vec4 topLeft = vec4(particlePosition+(-billboardHorizontalVector+billboardVerticalVector)*size, 1.0);
    ioTexCoord = vec2(0.0, 1.0);
    gl_Position = projectionView * topLeft;
    ioEyeSpacePosition = view * topLeft;
    EmitVertex();

    // Emit top right vertex
    vec4 topRight = vec4(particlePosition+(billboardHorizontalVector+billboardVerticalVector)*size, 1.0);
    ioTexCoord = vec2(1.0, 1.0);
    gl_Position = projectionView * topRight;
    ioEyeSpacePosition = view * topRight;
    EmitVertex();

    // And finally end the primitive
    EndPrimitive();
}