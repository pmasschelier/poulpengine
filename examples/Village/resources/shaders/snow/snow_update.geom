#version 460 core
#extension GL_ARB_shading_language_include : require

#include "/common/random.glsl"

const int PARTICLE_TYPE_GENERATOR = 0;
const int PARTICLE_TYPE_NORMAL = 1;

layout(points) in;
layout(points) out;
layout(max_vertices = 64) out;

// All that we get from vertex shader
in int ioType[];
in vec3 ioPosition[];
in float ioSize[];
in float ioAlpha[];
in uint ioSnowflakeIndex[];

// All that we send further and that is recorded by transform feedback
out int outType;
out vec3 outPosition;
out float outSize;
out float outAlpha;
flat out uint outSnowflakeIndex;

layout (std140) uniform Parameters {
	// Position where new particles are generated
	vec3 generatedPositionMin;
	vec3 generatedPositionRange;

	// Size of newly generated particles
	float generatedSizeMin;
	float generatedSizeRange;

	// Alpha of newly generated particles
	float generatedAlphaMin;
	float generatedAlphaRange;
};

// Time passed since last frame (in seconds)
uniform float elapsedTime;

// How many particles should be generated during this pass - if greater than zero, then particles are generated
uniform uint numParticlesToGenerate;

void main()
{
    initializeRandomNumberGeneratorSeed();

    // First, check if the incoming type of particle is generator
    outType = ioType[0];
    if(outType == PARTICLE_TYPE_GENERATOR)
    {
        // If it's the case, always emit generator particle further
        EmitVertex();
        EndPrimitive();

        // And now generate random particles, if numParticlesToGenerate is greater than zero
        for(int i = 0; i < numParticlesToGenerate; i++)
        {
            outType = PARTICLE_TYPE_NORMAL;
            outPosition = randomVectorMinRange(generatedPositionMin, generatedPositionRange);
            outSize = randomFloatMinRange(generatedSizeMin, generatedSizeRange);
            outAlpha = randomFloatMinRange(generatedAlphaMin, generatedAlphaRange);
            outSnowflakeIndex = randomIntMinRange(0, 4);
            EmitVertex();
            EndPrimitive();
        }

        return;
    }

    // If we get here, this means we deal with normal snow particle
    // Update its Y position and emit the particle only if it's still above zero (ground)
    outPosition = ioPosition[0];
    outPosition.y -= 40.0f * ioSize[0] * elapsedTime;
    if(outPosition.y > 0.0)
    {
        outSize = ioSize[0];
        outAlpha = ioAlpha[0];
        outSnowflakeIndex = ioSnowflakeIndex[0];
        EmitVertex();
        EndPrimitive();
	}
}