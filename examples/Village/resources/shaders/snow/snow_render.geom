#version 440 core

layout (std140, binding = 0) uniform Camera
{
	mat4 projection; // 4 * 4 * 4 bytes = align 16
	mat4 view; // 4 * 4 * 4 bytes = align 16
	mat4 projectionView; // 4 * 4 * 4 bytes = align 16
	mat4 inversedTransposedView; // 4 * 4 * 4 bytes = align 12
	vec3 position; // 3 * 3 bytes = align 12
	// padding 4
	vec3 billboardHorizontalVector; // 3 * 3 bytes = align 12
	// padding 4
	vec3 billboardVerticalVector; // 3 * 3 bytes = align 12
};

layout(points) in;
layout(triangle_strip) out;
layout(max_vertices = 4) out;

// Color of particles
uniform vec3 particlesColor;

// All that we get from vertex shader
in float ioSize[];
in float ioAlpha[];
in uint ioSnowflakeIndex[];

// Output variables
smooth out vec2 ioTexCoord;
smooth out vec4 ioEyeSpacePosition;
flat out vec4 ioColor;

void main()
{
    vec3 particlePosition = gl_in[0].gl_Position.xyz;
    float size = ioSize[0];

    // Mix particles color with alpha property from particle
    ioColor = vec4(particlesColor, min(1.0, ioAlpha[0]));

    // Offset particle position a bit on X axis depending on Y position
    // This makes particles wave a bit in the air
    particlePosition.x += 0.5 * sin(particlePosition.y * 0.3);

    // Calculate which snowflake texture we should take from our collection of 4 snowflakes
    uint snowflakeIndex = ioSnowflakeIndex[0];
    uint row = snowflakeIndex / 2;
    uint col = snowflakeIndex % 2;
    vec2 texCoordBase = vec2(0.5 * col, 0.5 - row * 0.5);

    // Emit bottom left vertex
    vec4 bottomLeft = vec4(particlePosition+(-billboardHorizontalVector-billboardVerticalVector)*size, 1.0);
    ioTexCoord = texCoordBase;
    gl_Position = projectionView * bottomLeft;
    ioEyeSpacePosition = view * bottomLeft;
    EmitVertex();

    // Emit bottom right vertex
    vec4 bottomRight = vec4(particlePosition+(billboardHorizontalVector-billboardVerticalVector)*size, 1.0);
    ioTexCoord = texCoordBase + vec2(0.5, 0.0);
    gl_Position = projectionView * bottomRight;
    ioEyeSpacePosition = view * bottomRight;
    EmitVertex();

    // Emit top left vertex
    vec4 topLeft = vec4(particlePosition+(-billboardHorizontalVector+billboardVerticalVector)*size, 1.0);
    ioTexCoord = texCoordBase + vec2(0.0, 0.5);
    gl_Position = projectionView * topLeft;
    ioEyeSpacePosition = view * topLeft;
    EmitVertex();

    // Emit top right vertex
    vec4 topRight = vec4(particlePosition+(billboardHorizontalVector+billboardVerticalVector)*size, 1.0);
    ioTexCoord = texCoordBase + vec2(0.5, 0.5);
    gl_Position = projectionView * topRight;
    ioEyeSpacePosition = view * topRight;
    EmitVertex();

    // And finally end the primitive
    EndPrimitive();
}