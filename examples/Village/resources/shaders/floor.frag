#version 460 core
#extension GL_ARB_shading_language_include : require

in VS_OUT {
	vec3 wPos;
    vec3 wNormal;
	vec2 texCoords;
};

#include "/common/light_casters.glsl"
#include "/common/camera.glsl"

uniform sampler2D snow;
uniform sampler2D pavement;
uniform sampler2D path;

out vec4 FragColor;

void main() {
    vec3 snowTexel = texture(snow, texCoords * 20.f).xyz;
    vec3 pathTexel = texture(path, texCoords).xyz;
    vec3 pavementTexel = texture(pavement, texCoords * 20.f).xyz;

    float pathWeight = pathTexel.r;
    float snowWeight = 1.0 - pathWeight;
	
    vec3 diffuse = pavementTexel*pathWeight + snowTexel*snowWeight;

	vec3 viewDir = normalize(wPos - position);
	vec3 color = additivePhongLighting(viewDir, wPos, wNormal, diffuse, diffuse, vec3(0.0), 0.0);
	FragColor = vec4(color, 1.0);
}