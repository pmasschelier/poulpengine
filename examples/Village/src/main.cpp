#include "../../common/common.hpp"

#include <fstream>

#include <Skybox.hpp>
#include <TransformFeedbackParticleSystem.hpp>
#include <Plane.hpp>
#include <ModelLoader.hpp>
#include <Model.hpp>
#include <SceneManager.hpp>
#include <Shaders/ShaderBuilder.hpp>

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

extern bool wireframeMode;
extern bool captureInput;

void update(GLFWwindow* window) {
	commonKeyboardUpdate(window);
	keyboard.ack = true;
}

std::shared_ptr<plp::Model> createFloor(plp::SceneManager& scene);
std::shared_ptr<plp::TransformFeedbackParticleSystem> createFire(plp::SceneManager& scene);
std::shared_ptr<plp::TransformFeedbackParticleSystem> createSnow(plp::SceneManager& scene);

int main(void)
{
	constexpr glm::ivec2 WSIZE(800, 800);
	GLFWwindow* window = glfw_init("Village", WSIZE);
	glfwMaximizeWindow(window);

	if(!window)
		return EXIT_FAILURE;

	if(!plp::init())
        return EXIT_FAILURE;
	
	camera = std::make_unique<plp::Camera>();
	camera->setSpeed(30.f);
	camera->setRatio(WSIZE.x / WSIZE.y);
	camera->setPosition({ 0.f, 4.f, -10.f });
	camera->setTarget({ 0.f, 0.f, 0.f });
	camera->setFarClipping(300.f);

	plp::ShaderBuilder builder;
	plp::SceneManager scene;

	bool success = scene.loadSkyboxFromFiles({
		"resources/skyboxes/jajsnow1/right.jpg",
		"resources/skyboxes/jajsnow1/left.jpg",
		"resources/skyboxes/jajsnow1/top.jpg",
		"resources/skyboxes/jajsnow1/bottom.jpg",
		"resources/skyboxes/jajsnow1/front.jpg",
		"resources/skyboxes/jajsnow1/back.jpg",
	});

	if(!success) {
		std::cerr << "Error while loading the Skybox." << std::endl;
		return EXIT_FAILURE;
	}
	
	constexpr auto FIRE_POS = glm::vec4(90.0f, 0.0f, 70.0f, 1.f);
	scene.addLight({
		.type = plp::LightManager::POINT,
		.constant = 1.f,
		.linear = 0.0014,
		.quadratic =  0.000007,
		.position = FIRE_POS + glm::vec4(0.f, 5.f, 0.f, 0.f),
	});

	scene.addLight({
		.type = plp::LightManager::DIRECTIONAL,
		.constant = 2.f,
		.direction = glm::vec4(-1.f, -.7f, -0.5f, 0.f),
	});

	auto floor = scene.createSceneNode("plane", createFloor(scene));
	floor->setScale({ 200.0, 1.0, 200.0 });

	auto fire = createFire(scene);
	scene.createSceneNode("fire", fire)->setPosition(FIRE_POS);

	auto snow = createSnow(scene);
	scene.createSceneNode("snow", snow);

	auto scheune = scene.loadSceneFromFile("resources/models/scheune_3ds/scheune.3ds").value();
	scheune->setScale(glm::vec3(0.03f, 0.03f, 0.03f));
	scheune->rotate(glm::angleAxis(glm::radians(-90.f), glm::vec3(1.f, 0.f, 0.f)));

	auto scheune1 = scene.duplicateSceneNode(scheune);
	auto scheune2 = scene.duplicateSceneNode(scheune);

	scheune->rotate(glm::angleAxis(glm::radians(45.f), glm::vec3(0.f, 1.f, 0.f)));
	scheune->setPosition(glm::vec3(30.f, 0.f, 100.f));

	scheune1->rotate(glm::angleAxis(glm::radians(90.f), glm::vec3(0.f, 1.f, 0.f)));
	scheune1->setPosition(glm::vec3(80.f, 0.f, 160.f));

	scheune2->rotate(glm::angleAxis(glm::radians(130.f), glm::vec3(0.f, 1.f, 0.f)));
	scheune2->setPosition(glm::vec3(150.f, 0.f, 160.f));

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();

	// Setup Platform/Renderer backends
	ImGui_ImplGlfw_InitForOpenGL(window, true);          // Second param install_callback=true will install GLFW callbacks and chain to existing ones.
	ImGui_ImplOpenGL3_Init();

	double currentTime = glfwGetTime();
	double lastTime = currentTime;

	while(!glfwWindowShouldClose(window)) {

		currentTime = glfwGetTime();
		elapsedTime = currentTime - lastTime;
		lastTime = currentTime;

		glfwPollEvents();
		update(window);

		fire->updateParticles(elapsedTime);
		snow->updateParticles(elapsedTime);

		plp::clear({ 0.2f, 0.3f, 0.3f, 1.0f });
		
		scene.render();

		/* ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData()); */

		glfwSwapBuffers(window);
	}

	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
	
	return EXIT_SUCCESS;
}

std::shared_ptr<plp::Model> createFloor(plp::SceneManager& scene) {
	plp::ShaderBuilder builder;
	builder.addShader(plp::ShaderBuilder::getCommonShader(plp::VERTEX_TEXTURE_SOURCE));
	builder.addFile(plp::FRAGMENT_SHADER, "resources/shaders/floor.frag");
	auto program = std::make_shared<plp::ShaderProgram>(std::move(builder.createProgram().value()));

	auto floor = std::make_shared<plp::Model>(plp::Plane::getTexturedPlane(), program);
	
	auto snow_tex = scene.loadTextureFromFile("snow", "resources/textures/snow.png");
	if(!snow_tex)
		throw new std::runtime_error("snow.png couldn't be loaded.");
	
	auto pavement_tex = scene.loadTextureFromFile("pavement", "resources/textures/pavement.jpg");
	if(!pavement_tex)
		throw new std::runtime_error("pavement.png couldn't be loaded.");
	
	auto path_tex = scene.loadTextureFromFile("path", "resources/textures/path.png");
	if(!path_tex)
		throw new std::runtime_error("path.png couldn't be loaded.");

	floor->addTexture(snow_tex.value(), "snow", 0);
	floor->addTexture(pavement_tex.value(), "pavement", 1);
	floor->addTexture(path_tex.value(), "path", 2);

	return floor;
}

std::shared_ptr<plp::TransformFeedbackParticleSystem> createFire(plp::SceneManager& scene) {
	using namespace plp;

	auto fire_tex = scene.loadTextureFromFile("fire", "resources/textures/particles/fire.bmp");
	if(!fire_tex)
		throw new std::runtime_error("fire.png couldn't be loaded.");

	auto fire = std::make_shared<plp::TransformFeedbackParticleSystem>();
	fire->init({
		1000,
		fire_tex.value(),
		{
			{ VERTEX_SHADER, "resources/shaders/fire/fire_update.vert" },
			{ GEOMETRY_SHADER, "resources/shaders/fire/fire_update.geom" }
		},
		{
			{ VERTEX_SHADER, "resources/shaders/fire/fire_render.vert" },
			{ GEOMETRY_SHADER, "resources/shaders/fire/fire_render.geom" },
			{ FRAGMENT_SHADER, "resources/shaders/fire/fire_render.frag" }
		},
		{
			{ Attribute::FLOAT, Attribute::VEC3, 2, "outVelocity", false },
			{ Attribute::FLOAT, Attribute::SCALAR, 3, "outLifetime", true },
			{ Attribute::FLOAT, Attribute::SCALAR, 4, "outSize", true },
		},
		{
			{ "generatedPositionMin", Attribute::FLOAT, Attribute::VEC3 },
			{ "generatedPositionRange", Attribute::FLOAT, Attribute::VEC3 },
			{ "generatedVelocityMin", Attribute::FLOAT, Attribute::VEC3 },
			{ "generatedVelocityRange", Attribute::FLOAT, Attribute::VEC3 },
			{ "generatedLifetimeMin", Attribute::FLOAT, Attribute::SCALAR },
			{ "generatedLifetimeRange", Attribute::FLOAT, Attribute::SCALAR },
			{ "generatedSizeMin", Attribute::FLOAT, Attribute::SCALAR },
			{ "generatedSizeRange", Attribute::FLOAT, Attribute::SCALAR },
		}
	});

	fire->setGeneratedAmount(40, 0.01f);
	fire->setParameter("generatedPositionMin", glm::vec3());
	fire->setParameter("generatedPositionRange", glm::vec3(4.0f, 0.0f, 4.0f));
	fire->setParameter("generatedVelocityMin", glm::vec3(-2.5f, 0.0f, -2.5f));
	fire->setParameter("generatedVelocityRange", glm::vec3(5.0f, 32.0f, 5.0f));

	fire->setParameter("generatedLifetimeMin", 2.5f);
	fire->setParameter("generatedLifetimeRange", 3.5f);
	fire->setParameter("generatedSizeMin", 0.1f);
	fire->setParameter("generatedSizeRange", 0.3f);

	fire->setRenderUniform("particlesColor", glm::vec3(0.83f, 0.435f, 0.0f));
	return fire;
}

std::shared_ptr<plp::TransformFeedbackParticleSystem> createSnow(plp::SceneManager& scene) {
	using namespace plp;

	auto snow_tex = scene.loadTextureFromFile("snowflakes", "resources/textures/particles/snowflakes.bmp");
	if(!snow_tex)
		throw new std::runtime_error("snowflakes.bmp couldn't be loaded.");

	auto snow = std::make_shared<plp::TransformFeedbackParticleSystem>();
	snow->init({
		50'000,
		snow_tex.value(),
		{
			{ VERTEX_SHADER, "resources/shaders/snow/snow_update.vert" },
			{ GEOMETRY_SHADER, "resources/shaders/snow/snow_update.geom" }
		},
		{
			{ VERTEX_SHADER, "resources/shaders/snow/snow_render.vert" },
			{ GEOMETRY_SHADER, "resources/shaders/snow/snow_render.geom" },
			{ FRAGMENT_SHADER, "resources/shaders/snow/snow_render.frag" }
		},
		{
			{ Attribute::FLOAT, Attribute::SCALAR, 2, "outSize", true },
			{ Attribute::FLOAT, Attribute::SCALAR, 3, "outAlpha", true },
			{ Attribute::UNSIGNED_INT, Attribute::SCALAR, 4, "outSnowflakeIndex", true },
		},
		{
			{ "generatedPositionMin", Attribute::FLOAT, Attribute::VEC3 },
			{ "generatedPositionRange", Attribute::FLOAT, Attribute::VEC3 },
			{ "generatedSizeMin", Attribute::FLOAT, Attribute::SCALAR },
			{ "generatedSizeRange", Attribute::FLOAT, Attribute::SCALAR },
			{ "generatedAlphaMin", Attribute::FLOAT, Attribute::SCALAR },
			{ "generatedAlphaRange", Attribute::FLOAT, Attribute::SCALAR },
		}
	});

	snow->setGeneratedAmount(40, 0.005f);
	snow->setParameter("generatedPositionMin", glm::vec3(0.f, 50.f, 0.f));
	snow->setParameter("generatedPositionRange", glm::vec3(200.f, 30.0, 200.f));

	snow->setParameter("generatedSizeMin", .05f);
	snow->setParameter("generatedSizeRange", .15f);

	snow->setParameter("generatedAlphaMin", .5f);
	snow->setParameter("generatedAlphaRange", .3f);

	snow->setRenderUniform("particlesColor", glm::vec3(0.85f, 0.85f, 0.85f));

	return snow;
}
